# core.sdk #
# @ephesoft/core.sdk

## Consuming this package

[API Documentation](https://bitbucket.org/ephesoft/semantik.api.documentation/src/master/http.api.sdk/modules.md)

Add the package to your repository:
```bash
npm i @ephesoft/core.sdk
```

__NOTE:__ *This package does not include an index file.  Your imports will look like this:*

```typescript
import { sleep } from '@ephesoft/core.sdk/concurrency/sleep';
```

API documentation may be generated with ```npm run docs``` (HTML) or ```npm run docs:bitbucket``` (MD).

## Development

### Files that must be updated
__IMPORTANT:__ The following files should be updated with every change:
* package.json ($.version property) - use semantic versioning
* package-lock.json ($.version property) - must match version in package.json
* changelog.md - Should include the version and a description of the changes
* readme.md - Review this file to see if changes are needed

### NPM run targets
Run NPM commands as
```bash
npm run command
```
#### Available Commands
|Command|Description|
|:-|:-|
| audit | Executes a dependency audit using the default NPM registry |
| audit:fix | Attempts an automatic fix of any audit failures. |
| build | Cleans the dist folder and transpiles/webpacks the code |
| clean | Deletes the working folders like dist and docs |
| clean:docs | Deletes the docs folder |
| create:beta | Auto versions the package to the next available beta version. |
| docs | Create API documentation for the package in a "docs" folder |
| docs:bitbucket | Creates API documentation in Bitbucket markdown format in a "docs" folder |
| lint | Performs static analysis on the TypeScript files. |
| lint:fix | Performs static analysis on the TypeScript files and attempts to automatically fix errors. |
| lint:report | Performs static analysis on the TypeScript files using the default validation rules and logs results to a JSON file. |
| pack | Creates a local tarball package |
| postbuild| Automatically ran during build. |
| postprepare  | Automatically ran during package install to setup tooling. |
| prebuild | Automatically ran during build. |
| start | Instructs the TypeScript compiler to compile the ts files every time a change is detected. |
| test | Executes all tests and generates coverage. |
| test:coverage | Generates code coverage reports from test data. |
| test:unit | Runs the unit tests. |
| validate:ga | Validates the package version is valid for GA release. |
| validate:beta | Validates the package version is valid for a Beta release. |
