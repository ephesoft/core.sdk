import { expect } from 'chai';
import { StringEnum } from '../../../src/conversion/StringEnum';
import { InvalidParameterError } from '../../../src/errors/InvalidParameterError';
import { executeAndExpectError } from '@ephesoft/test.assertions';

describe('StringEnum<TEnum> unit tests', function () {
    enum TestEnum {
        Key1 = 'Value1',
        Key2 = 'Value2',
        Key3 = 'Value3'
    }

    describe('constructor', function () {
        it('should throw error if enumType parameter is not an object', function () {
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    new StringEnum<TestEnum>('not an object');
                },
                InvalidParameterError,
                'Invalid enumType parameter - must be the type of an enum'
            );
        });

        it('should throw error if enumType contains an empty key', function () {
            executeAndExpectError(
                () => {
                    new StringEnum<TestEnum>({ ['']: 'value' });
                },
                InvalidParameterError,
                'Invalid enumType parameter - must not have any keys which are an empty string'
            );
        });

        it('should throw error if enumType contains a value which is an empty string', function () {
            executeAndExpectError(
                () => {
                    new StringEnum<TestEnum>({ key: '' });
                },
                InvalidParameterError,
                'Invalid enumType parameter - must not have any values that are undefined, null, or an empty string'
            );
        });

        it('should throw error if enumType contains a non-string value', function () {
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in a bad value
                    new StringEnum<TestEnum>({ key: 42 });
                },
                InvalidParameterError,
                'Invalid enumType parameter - all values must be strings'
            );
        });

        it('should accept valid enum type', function () {
            new StringEnum<TestEnum>(TestEnum);
            // If it didn't throw an error we're good
        });
    });

    describe('get type', function () {
        it('should return the enum type', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            expect(stringEnum.type).to.equal(TestEnum);
        });
    });

    describe('get keys(): string[]', function () {
        it('should return key names in original case', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            expect(stringEnum.keys).to.deep.equal(['Key1', 'Key2', 'Key3']);
        });

        it('should return a copy of the keys - not the working copy', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const keys = stringEnum.keys;
            keys.push('Key4');
            expect(stringEnum.keys).to.deep.equal(['Key1', 'Key2', 'Key3']);
        });
    });

    describe('get values(): string[]', function () {
        it('should return values in original case', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            expect(stringEnum.values).to.deep.equal(['Value1', 'Value2', 'Value3']);
        });

        it('should return a copy of the values - not the working copy', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const values = stringEnum.values;
            values.push('Key4' as TestEnum);
            expect(stringEnum.values).to.deep.equal(['Value1', 'Value2', 'Value3']);
        });
    });

    describe('isKey(text: string, caseSensitive: boolean = false): boolean', function () {
        it('should throw error if text parameter is undefined', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    stringEnum.isKey(undefined);
                },
                InvalidParameterError,
                'Invalid text parameter - must not be undefined or null'
            );
        });

        it('should throw error if text parameter is null', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    stringEnum.isKey(null);
                },
                InvalidParameterError,
                'Invalid text parameter - must not be undefined or null'
            );
        });

        it('should return false if key not present in enum', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            // We do not trim white space as part of the conversion
            const result = stringEnum.isKey(' Key1 ');
            expect(result).to.be.false;
        });

        it('should return false if case sensitive and case does not match', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const result = stringEnum.isKey('key1', true);
            expect(result).to.be.false;
        });

        it('should return true for direct hit when case sensitive', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const result = stringEnum.isKey('Key1', true);
            expect(result).to.be.true;
        });

        it('should return true if not case sensitive and case does not match', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const result = stringEnum.isKey('key1');
            expect(result).to.be.true;
        });
    });

    describe('isValue(text: string, caseSensitive: boolean = false): boolean', function () {
        it('should throw error if text parameter is undefined', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    stringEnum.isValue(undefined);
                },
                InvalidParameterError,
                'Invalid text parameter - must not be undefined or null'
            );
        });

        it('should throw error if text parameter is null', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    stringEnum.isValue(null);
                },
                InvalidParameterError,
                'Invalid text parameter - must not be undefined or null'
            );
        });

        it('should return false if value not present in enum', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            // We do not trim white space as part of the conversion
            const result = stringEnum.isValue(' Value1 ');
            expect(result).to.be.false;
        });

        it('should return false if case sensitive and case does not match', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const result = stringEnum.isValue('VALUE1', true);
            expect(result).to.be.false;
        });

        it('should return true for direct hit when case sensitive', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const result = stringEnum.isValue('Value1', true);
            expect(result).to.be.true;
        });

        it('should return true if not case sensitive and case does not match', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const result = stringEnum.isValue('VALUE1');
            expect(result).to.be.true;
        });
    });

    describe('convertToKey(text: string, caseSensitive: boolean = false): string', function () {
        it('should throw error if text parameter is undefined', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    stringEnum.convertToKey(undefined);
                },
                InvalidParameterError,
                'Invalid text parameter (undefined) - must be one of: Key1, Key2, Key3'
            );
        });

        it('should throw error if text is not an enum key', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    stringEnum.convertToKey('key4');
                },
                InvalidParameterError,
                'Invalid text parameter (key4) - must be one of: Key1, Key2, Key3'
            );
        });

        it('should throw error if case sensitive and case does not match', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    stringEnum.convertToKey('key1', true);
                },
                InvalidParameterError,
                'Invalid text parameter (key1) - must be one of: Key1, Key2, Key3'
            );
        });

        it('should return key if case sensitive and case matches', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const key = stringEnum.convertToKey('Key1', true);
            expect(key).to.equal('Key1');
        });

        it('should return key if not case sensitive and case does not match', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const key = stringEnum.convertToKey('key2');
            expect(key).to.equal('Key2');
        });
    });

    describe('convertToValue(text: string, caseSensitive: boolean = false): string', function () {
        it('should throw error if text parameter is undefined', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    stringEnum.convertToValue(undefined);
                },
                InvalidParameterError,
                'Invalid text parameter (undefined) - must be one of: Value1, Value2, Value3'
            );
        });

        it('should throw error if text is not an enum value', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    stringEnum.convertToValue('Value4');
                },
                InvalidParameterError,
                'Invalid text parameter (Value4) - must be one of: Value1, Value2, Value3'
            );
        });

        it('should throw error if case sensitive and case does not match', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    stringEnum.convertToValue('value1', true);
                },
                InvalidParameterError,
                'Invalid text parameter (value1) - must be one of: Value1, Value2, Value3'
            );
        });

        it('should return value if case sensitive and case matches', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const value = stringEnum.convertToValue('Value1', true);
            expect(value).to.equal('Value1');
        });

        it('should return value if not case sensitive and case does not match', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const value = stringEnum.convertToValue('value2');
            expect(value).to.equal('Value2');
        });
    });

    describe('getValueForKey(key: string, caseSensitive: boolean = false): TEnum', function () {
        it('should throw error if key parameter is undefined', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    stringEnum.getValueForKey(undefined);
                },
                InvalidParameterError,
                'Invalid key parameter (undefined) - must be one of: Key1, Key2, Key3'
            );
        });

        it('should throw error if key is not a key', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    stringEnum.getValueForKey('key4');
                },
                InvalidParameterError,
                'Invalid key parameter (key4) - must be one of: Key1, Key2, Key3'
            );
        });

        it('should throw error if case sensitive and case does not match', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    stringEnum.getValueForKey('key1', true);
                },
                InvalidParameterError,
                'Invalid key parameter (key1) - must be one of: Key1, Key2, Key3'
            );
        });

        it('should return value if case sensitive and case matches', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const value = stringEnum.getValueForKey('Key1', true);
            expect(value).to.equal('Value1');
        });

        it('should return value if not case sensitive and case does not match', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const value = stringEnum.getValueForKey('key2');
            expect(value).to.equal('Value2');
        });
    });

    describe('getKeyForValue(value: string, caseSensitive: boolean = false): TEnum', function () {
        it('should throw error if value parameter is undefined', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    stringEnum.getKeyForValue(undefined);
                },
                InvalidParameterError,
                'Invalid value parameter (undefined) - must be one of: Value1, Value2, Value3'
            );
        });

        it('should throw error if value is not an enum value', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    stringEnum.getKeyForValue('Value4');
                },
                InvalidParameterError,
                'Invalid value parameter (Value4) - must be one of: Value1, Value2, Value3'
            );
        });

        it('should throw error if case sensitive and case does not match', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    stringEnum.getKeyForValue('value1', true);
                },
                InvalidParameterError,
                'Invalid value parameter (value1) - must be one of: Value1, Value2, Value3'
            );
        });

        it('should return value if case sensitive and case matches', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const key = stringEnum.getKeyForValue('Value1', true);
            expect(key).to.equal('Key1');
        });

        it('should return value if not case sensitive and case does not match', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            const key = stringEnum.getKeyForValue('value2');
            expect(key).to.equal('Key2');
        });
    });

    describe('toJSON(): unknown', function () {
        it('should serialize nicely with JSON.stringify', function () {
            const stringEnum = new StringEnum<TestEnum>(TestEnum);
            expect(JSON.stringify(stringEnum)).to.equal('{"Key1":"Value1","Key2":"Value2","Key3":"Value3"}');
        });
    });
});
