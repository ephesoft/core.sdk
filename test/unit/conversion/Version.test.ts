import { executeAndExpectError } from '@ephesoft/test.assertions';
import { expect } from 'chai';
import { InvalidParameterError } from '../../../src/errors/InvalidParameterError';
import { Version, VersionType } from '../../../src/conversion/Version';

describe('Version unit tests', function () {
    describe('parse', function () {
        it('should parse two-segment version', function () {
            const version = Version.parse('1.2');

            expect(version.major).to.equal(1);
            expect(version.minor).to.equal(2);
            expect(version.bugfix).to.equal(0);
            expect(version.prerelease).to.equal(0);
            expect(version.type).to.equal(VersionType.GeneralAvailability);
        });

        it('should parse three-segment version', function () {
            const version = Version.parse('1.2.3');

            expect(version.major).to.equal(1);
            expect(version.minor).to.equal(2);
            expect(version.bugfix).to.equal(3);
            expect(version.prerelease).to.equal(0);
            expect(version.type).to.equal(VersionType.GeneralAvailability);
        });

        it('should parse alpha version', function () {
            const version = Version.parse('1.2.3-alpha.4');

            expect(version.major).to.equal(1);
            expect(version.minor).to.equal(2);
            expect(version.bugfix).to.equal(3);
            expect(version.prerelease).to.equal(4);
            expect(version.type).to.equal(VersionType.Alpha);
        });

        it('should parse beta version', function () {
            const version = Version.parse('1.2.3-beta.4');

            expect(version.major).to.equal(1);
            expect(version.minor).to.equal(2);
            expect(version.bugfix).to.equal(3);
            expect(version.prerelease).to.equal(4);
            expect(version.type).to.equal(VersionType.Beta);
        });

        it('should be case insensitive', function () {
            const version = Version.parse('11.22.33-bEtA.44');

            expect(version.major).to.equal(11);
            expect(version.minor).to.equal(22);
            expect(version.bugfix).to.equal(33);
            expect(version.prerelease).to.equal(44);
            expect(version.type).to.equal(VersionType.Beta);
        });

        it('should not parse malformed version', function () {
            for (const version of [
                '1',
                '1.',
                '1.x',
                'x.1',
                '1-alpha.1',
                '.1.1',
                ' 1.0',
                '1.0 ',
                '1.0-alpha.1',
                '1.0.',
                '1.0.x',
                '1.0.0-beta',
                '1.0.0-1',
                '1.0.0-.1'
            ]) {
                executeAndExpectError(
                    () => {
                        Version.parse(version);
                    },
                    InvalidParameterError,
                    /Invalid version/
                );
            }
        });
    });

    describe('constructor', function () {
        it('should populate object (minimum data)', function () {
            const version = new Version({ major: 4, minor: 2 });

            expect(version.major).to.equal(4);
            expect(version.minor).to.equal(2);
            expect(version.bugfix).to.equal(0);
            expect(version.prerelease).to.equal(0);
            expect(version.type).to.equal(VersionType.GeneralAvailability);
        });

        it('should populate object (all data)', function () {
            const version = new Version({ major: 1, minor: 2, bugfix: 3, prerelease: 4, type: VersionType.Beta });

            expect(version.major).to.equal(1);
            expect(version.minor).to.equal(2);
            expect(version.bugfix).to.equal(3);
            expect(version.prerelease).to.equal(4);
            expect(version.type).to.equal(VersionType.Beta);
        });

        it('should throw error if major version is negative', function () {
            executeAndExpectError(
                () => {
                    new Version({ major: -1, minor: 2, bugfix: 3, prerelease: 4, type: VersionType.Alpha });
                },
                InvalidParameterError,
                /Invalid version/
            );
        });

        it('should throw error if major version is float', function () {
            executeAndExpectError(
                () => {
                    new Version({ major: 1.1, minor: 2, bugfix: 3, prerelease: 4, type: VersionType.Alpha });
                },
                InvalidParameterError,
                /Invalid version/
            );
        });

        it('should throw error if minor version is negative', function () {
            executeAndExpectError(
                () => {
                    new Version({ major: 1, minor: -2, bugfix: 3, prerelease: 4, type: VersionType.Alpha });
                },
                InvalidParameterError,
                /Invalid version/
            );
        });

        it('should throw error if minor version is float', function () {
            executeAndExpectError(
                () => {
                    new Version({ major: 1, minor: 2.2, bugfix: 3, prerelease: 4, type: VersionType.Alpha });
                },
                InvalidParameterError,
                /Invalid version/
            );
        });

        it('should throw error if bugfix version is negative', function () {
            executeAndExpectError(
                () => {
                    new Version({ major: 1, minor: 2, bugfix: -3, prerelease: 4, type: VersionType.Alpha });
                },
                InvalidParameterError,
                /Invalid version/
            );
        });

        it('should throw error if bugfix version is float', function () {
            executeAndExpectError(
                () => {
                    new Version({ major: 1, minor: 2, bugfix: 3.3, prerelease: 4, type: VersionType.Alpha });
                },
                InvalidParameterError,
                /Invalid version/
            );
        });

        it('should throw error if prerelease version is negative', function () {
            executeAndExpectError(
                () => {
                    new Version({ major: 1, minor: 2, bugfix: 3, prerelease: -4, type: VersionType.Alpha });
                },
                InvalidParameterError,
                /Invalid version/
            );
        });

        it('should throw error if prerelease version is float', function () {
            executeAndExpectError(
                () => {
                    new Version({ major: 1, minor: 2, bugfix: 3, prerelease: 4.4, type: VersionType.Alpha });
                },
                InvalidParameterError,
                /Invalid version/
            );
        });

        it('should throw error if prerelease version is specified and bugfix version is not', function () {
            executeAndExpectError(
                () => {
                    new Version({ major: 1, minor: 2, prerelease: 4 });
                },
                InvalidParameterError,
                /Invalid version/
            );
        });

        it('should throw error if prerelease version is specified and type is not', function () {
            executeAndExpectError(
                () => {
                    new Version({ major: 1, minor: 2, bugfix: 3, prerelease: 4 });
                },
                InvalidParameterError,
                /Invalid version/
            );
        });

        it('should throw error if prerelease version is specified and type is not', function () {
            executeAndExpectError(
                () => {
                    new Version({ major: 1, minor: 2, bugfix: 3, prerelease: 4 });
                },
                InvalidParameterError,
                /Invalid version/
            );
        });

        it('should throw error if prerelease version is specified and type is GeneralAvailability', function () {
            executeAndExpectError(
                () => {
                    new Version({ major: 1, minor: 2, bugfix: 3, prerelease: 4, type: VersionType.GeneralAvailability });
                },
                InvalidParameterError,
                /Invalid version/
            );
        });

        it('should throw error if type is not GeneralAvailability and prerelease version is not specified', function () {
            executeAndExpectError(
                () => {
                    new Version({ major: 1, minor: 2, bugfix: 3, type: VersionType.Beta });
                },
                InvalidParameterError,
                /Invalid version/
            );
        });
    });

    describe('toJSON', function () {
        it('should properly represent two-segment version', function () {
            const version1 = new Version({ major: 1, minor: 2 });
            expect(version1.toJSON()).to.deep.equal({
                major: 1,
                minor: 2
            });

            const version2 = new Version({ major: 0, minor: 0 });
            expect(version2.toJSON()).to.deep.equal({
                major: 0,
                minor: 0
            });
        });

        it('should properly represent three-segment version', function () {
            const version1 = new Version({ major: 1, minor: 2, bugfix: 3 });
            expect(version1.toJSON()).to.deep.equal({
                major: 1,
                minor: 2,
                bugfix: 3
            });

            const version2 = new Version({ major: 0, minor: 0, bugfix: 0 });
            expect(version2.toJSON()).to.deep.equal({
                major: 0,
                minor: 0,
                bugfix: 0
            });
        });

        it('should properly represent alpha version', function () {
            const version1 = new Version({ major: 1, minor: 2, bugfix: 3, prerelease: 4, type: VersionType.Alpha });
            expect(version1.toJSON()).to.deep.equal({
                major: 1,
                minor: 2,
                bugfix: 3,
                prerelease: 4,
                type: VersionType.Alpha
            });

            const version2 = new Version({ major: 0, minor: 0, bugfix: 0, prerelease: 0, type: VersionType.Alpha });
            expect(version2.toJSON()).to.deep.equal({
                major: 0,
                minor: 0,
                bugfix: 0,
                prerelease: 0,
                type: VersionType.Alpha
            });
        });

        it('should properly represent beta version', function () {
            const version1 = new Version({ major: 11, minor: 22, bugfix: 33, prerelease: 44, type: VersionType.Beta });
            expect(version1.toJSON()).to.deep.equal({
                major: 11,
                minor: 22,
                bugfix: 33,
                prerelease: 44,
                type: VersionType.Beta
            });

            const version2 = new Version({ major: 0, minor: 0, bugfix: 0, prerelease: 0, type: VersionType.Beta });
            expect(version2.toJSON()).to.deep.equal({
                major: 0,
                minor: 0,
                bugfix: 0,
                prerelease: 0,
                type: VersionType.Beta
            });
        });
    });

    describe('toString', function () {
        it('should properly represent two-segment version', function () {
            const version1 = new Version({ major: 1, minor: 2 });
            expect(version1.toString()).to.equal('1.2');

            const version2 = new Version({ major: 0, minor: 0 });
            expect(version2.toString()).to.equal('0.0');
        });

        it('should properly represent three-segment version', function () {
            const version1 = new Version({ major: 1, minor: 2, bugfix: 3 });
            expect(version1.toString()).to.equal('1.2.3');

            const version2 = new Version({ major: 0, minor: 0, bugfix: 0 });
            expect(version2.toString()).to.equal('0.0.0');
        });

        it('should properly represent alpha version', function () {
            const version1 = new Version({ major: 1, minor: 2, bugfix: 3, prerelease: 4, type: VersionType.Alpha });
            expect(version1.toString()).to.equal('1.2.3-alpha.4');

            const version2 = new Version({ major: 0, minor: 0, bugfix: 0, prerelease: 0, type: VersionType.Alpha });
            expect(version2.toString()).to.equal('0.0.0-alpha.0');
        });

        it('should properly represent beta version', function () {
            const version1 = new Version({ major: 11, minor: 22, bugfix: 33, prerelease: 44, type: VersionType.Beta });
            expect(version1.toString()).to.equal('11.22.33-beta.44');

            const version2 = new Version({ major: 0, minor: 0, bugfix: 0, prerelease: 0, type: VersionType.Beta });
            expect(version2.toString()).to.equal('0.0.0-beta.0');
        });
    });
});
