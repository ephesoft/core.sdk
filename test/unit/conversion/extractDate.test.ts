import { assert } from 'chai';
import { extractDate } from '../../../src/conversion/extractDate';
import { DataTypeExtractionError } from '../../../src/errors/DataTypeExtractionError';
import { executeAndExpectError } from '@ephesoft/test.assertions';

describe('extractDate unit tests', function () {
    it('should parse date in long US format', function () {
        assert.strictEqual(extractDate('March 2, 2020'), '2020-03-02');
    });

    it('should parse date in abbreviated US format', function () {
        assert.strictEqual(extractDate('FEB 26, 2020'), '2020-02-26');
    });

    it('should parse date and time in long US format', function () {
        assert.strictEqual(extractDate('March 2, 2020 1:05:32 PM'), '2020-03-02');
    });

    it('should parse date and time in abbreviated US format', function () {
        assert.strictEqual(extractDate('Dec 13, 2020 1:05:32 PM'), '2020-12-13');
    });

    it('should parse MM/d/yyyy format', function () {
        assert.strictEqual(extractDate('10/1/2019'), '2019-10-01');
    });

    it('should parse MM/dd/yyyy format', function () {
        assert.strictEqual(extractDate('10/01/2019'), '2019-10-01');
    });

    it('should parse M/d/yyyy format', function () {
        assert.strictEqual(extractDate('1/9/19'), '2019-01-09');
    });

    it('should parse MM/d/yyyy HH:MM:SS format', function () {
        assert.strictEqual(extractDate('10/1/2019 18:23:52'), '2019-10-01');
    });

    it('should parse sortable date time format', function () {
        assert.strictEqual(extractDate('2009-06-15T13:45:30'), '2009-06-15');
    });

    it('should parse RFC1123 format', function () {
        assert.strictEqual(extractDate('Mon, 15 Jun 2009 20:45:30 GMT'), '2009-06-15');
    });

    it('should parse yyyy-MM-dd format', function () {
        assert.strictEqual(extractDate('2009-06-15'), '2009-06-15');
    });

    it('should parse yyyy-M-d format', function () {
        assert.strictEqual(extractDate('2009-6-3'), '2009-06-03');
    });

    it('should parse MM-dd-yy format', function () {
        assert.strictEqual(extractDate('06-15-19'), '2019-06-15');
    });

    it('should parse universal sortable date time format', function () {
        assert.strictEqual(extractDate('2009-06-15 13:45:30Z'), '2009-06-15');
    });

    it('should parse universal full date time format', function () {
        assert.strictEqual(extractDate('Monday, June 15, 2009 8:45:30 PM'), '2009-06-15');
    });

    it('should parse year month format', function () {
        assert.strictEqual(extractDate('Monday, June 15, 2009 8:45:30 PM'), '2009-06-15');
    });

    it('should allow February 29 on leap year', function () {
        assert.strictEqual(extractDate('02-29-2020'), '2020-02-29');
    });

    it('should convert iso8601 date format to MM-dd-yyyy', function () {
        assert.strictEqual(extractDate('2020-02-29', { dateFormat: 'MM-dd-yyyy' }), '02-29-2020');
    });

    it('should convert US date format to MM-dd-yyyy', function () {
        assert.strictEqual(extractDate('March 2, 2020', { dateFormat: 'MM-dd-yyyy' }), '03-02-2020');
    });

    it('should convert abbreviated US date format to MM-dd-yyyy', function () {
        assert.strictEqual(extractDate('FEB 26, 2020', { dateFormat: 'MM-dd-yyyy' }), '02-26-2020');
    });

    it('should convert MM/D/yyyy date format to MM-dd-yyyy', function () {
        assert.strictEqual(extractDate('10/1/2019', { dateFormat: 'MM-dd-yyyy' }), '10-01-2019');
    });

    it('should convert MM/D/yyyy date format to MMM Do yy', function () {
        assert.strictEqual(extractDate('10/1/2019', { dateFormat: 'MMM do yy' }), 'Oct 1st 19');
    });
    it('should return default value if invalid date in yyyy-MM-dd format when no format specified', function () {
        assert.strictEqual(extractDate('something', { defaultValue: '2020-12-25' }), '2020-12-25');
    });

    it('should return default value if invalid date with supplied dateFormat', function () {
        assert.strictEqual(extractDate('not a date', { dateFormat: 'MM-dd-yyyy', defaultValue: '2020-12-25' }), '12-25-2020');
    });

    it('should return null default value if invalid date with date and ignores dateFormat', function () {
        assert.strictEqual(extractDate('not a date', { defaultValue: null }), null);
    });

    it('should return default if value is invalid', function () {
        assert.strictEqual(extractDate('December 33, 2020', { defaultValue: 'December 25, 2020' }), '2020-12-25');
    });

    it('should throw exception if value is undefined and default value is invalid', function () {
        executeAndExpectError(
            () => {
                extractDate(undefined as never, { defaultValue: 'not a date' });
            },
            DataTypeExtractionError,
            'Failed to parse both value (undefined) and default value (not a date) as a date: Unsupported date format'
        );
    });

    it('should throw exception if value is undefined', function () {
        executeAndExpectError(
            () => {
                extractDate(undefined as never);
            },
            DataTypeExtractionError,
            'Failed to parse value (undefined) as a date: Unsupported date format'
        );
    });

    it('should throw exception if value is empty string', function () {
        executeAndExpectError(
            () => {
                extractDate('');
            },
            DataTypeExtractionError,
            'Failed to parse value () as a date: Unsupported date format'
        );
    });

    it('should throw exception if value is not a date', function () {
        executeAndExpectError(
            () => {
                extractDate('not a date');
            },
            DataTypeExtractionError,
            'Failed to parse value (not a date) as a date: Unsupported date format'
        );
    });

    it('should throw exception if date contains an out-of-bounds month', function () {
        for (const month of ['00', '13']) {
            const date = `2020-${month}-01`;
            executeAndExpectError(
                () => {
                    extractDate(date);
                },
                DataTypeExtractionError,
                `Failed to parse value (${date}) as a date: Date was found to be invalid after formatting (${date})`
            );
        }
    });

    it('should throw exception if date contains an out-of-bounds day', function () {
        for (const day of ['00', '29']) {
            const date = `2019-02-${day}`;
            executeAndExpectError(
                () => {
                    extractDate(date);
                },
                DataTypeExtractionError,
                `Failed to parse value (${date}) as a date: Date was found to be invalid after formatting (${date})`
            );
        }
    });
});
