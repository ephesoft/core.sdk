import { executeAndExpectError } from '@ephesoft/test.assertions';
import { expect } from 'chai';
import { extractNumber } from '../../../src/conversion/extractNumber';
import { DataTypeExtractionError } from '../../../src/errors/DataTypeExtractionError';

describe('extractNumber unit tests', function () {
    it('should parse 0 as a number', function () {
        expect(extractNumber(0)).to.equal(0);
    });

    it('should parse 0', function () {
        expect(extractNumber('0')).to.equal(0);
    });

    it('should parse 0.0', function () {
        expect(extractNumber('0.0')).to.equal(0);
    });

    it('should parse 0.01', function () {
        expect(extractNumber('0.01')).to.equal(0.01);
    });

    it('should parse 1', function () {
        expect(extractNumber('1')).to.equal(1);
    });

    it('should parse -1', function () {
        expect(extractNumber('-1')).to.equal(-1);
    });

    it('should parse --1', function () {
        expect(extractNumber('--1')).to.equal(-1);
    });

    it('should parse 1.1', function () {
        expect(extractNumber('1.1')).to.equal(1.1);
    });

    it('should parse 1..1', function () {
        expect(extractNumber('1..1')).to.equal(1.1);
    });

    it('should parse -1.1', function () {
        expect(extractNumber('-1.1')).to.equal(-1.1);
    });

    it('should parse .0', function () {
        expect(extractNumber('.0')).to.equal(0);
    });

    it('should parse .15', function () {
        expect(extractNumber('.15')).to.equal(0.15);
    });

    it('should parse -.15', function () {
        expect(extractNumber('-.15')).to.equal(-0.15);
    });

    it('should parse ,15', function () {
        expect(extractNumber(',15')).to.equal(0.15);
    });

    it('should parse -,15', function () {
        expect(extractNumber('-,15')).to.equal(-0.15);
    });

    it('should parse 1.', function () {
        expect(extractNumber('1.')).to.equal(1);
    });

    it('should parse 1,', function () {
        expect(extractNumber('1,')).to.equal(1);
    });

    it('should parse $1.30.', function () {
        expect(extractNumber('$1.30.')).to.equal(1.3);
    });

    it('should parse $100.00', function () {
        expect(extractNumber('$100.00')).to.equal(100);
    });

    it('should parse 1,00', function () {
        expect(extractNumber('1,00')).to.equal(1);
    });

    it('should parse -$100', function () {
        expect(extractNumber('-$100')).to.equal(-100);
    });

    it('should parse $-100', function () {
        expect(extractNumber('$-100')).to.equal(-100);
    });

    it('should parse 1,000,000.01', function () {
        expect(extractNumber('1,000,000.01')).to.equal(1000000.01);
    });

    it('should parse 1, 000, 000 .01', function () {
        expect(extractNumber('1, 000, 000 .01')).to.equal(1000000.01);
    });

    it('should parse 1,000,000,01', function () {
        expect(extractNumber('1,000,000,01')).to.equal(1000000.01);
    });

    it('should parse 1.000.000.01', function () {
        expect(extractNumber('1.000.000.01')).to.equal(1000000.01);
    });

    it('should parse 1. 000. 000 .01', function () {
        expect(extractNumber('1. 000. 000 .01')).to.equal(1000000.01);
    });

    it('should return default value when string not a number', function () {
        expect(extractNumber('avbncsans', 0)).to.equal(0);
    });

    it('should return default null value when string not a number', function () {
        expect(extractNumber('avbncsans', null)).to.equal(null);
    });

    it('should return default value when number is not a safe integer', function () {
        const unsafePositiveInteger = Number.MAX_SAFE_INTEGER + 1;
        const result = extractNumber(unsafePositiveInteger, 0);
        expect(result).to.equal(0);
    });

    it('should return default value when number is larger than Number.MAX_VALUE', function () {
        const unsafePositiveNumber = Number.MAX_VALUE + 1;
        const result = extractNumber(unsafePositiveNumber, 100);
        expect(result).to.equal(100);
    });

    it('should throw error if value has only spaces', function () {
        executeAndExpectError(
            () => {
                extractNumber('     ');
            },
            DataTypeExtractionError,
            'Failed to parse value (     ) as a number: Unsupported number format'
        );
    });

    it('should throw error if value is empty string', function () {
        executeAndExpectError(
            () => {
                extractNumber('');
            },
            DataTypeExtractionError,
            'Failed to parse value () as a number: Unsupported number format'
        );
    });

    it('should throw error if value gets trimmed to empty string', function () {
        executeAndExpectError(
            () => {
                extractNumber('$');
            },
            DataTypeExtractionError,
            'Failed to parse value ($) as a number: Unsupported number format'
        );
    });

    it('should throw error if value is dash', function () {
        executeAndExpectError(
            () => {
                extractNumber('-');
            },
            DataTypeExtractionError,
            'Failed to parse value (-) as a number: Unsupported number format'
        );
    });

    it('should throw error if not a number', function () {
        executeAndExpectError(
            () => {
                extractNumber('not a number');
            },
            DataTypeExtractionError,
            'Failed to parse value (not a number) as a number: Unsupported number format'
        );
    });

    it('should throw error if not a safe positive integer', function () {
        const unsafePositiveInteger: number = Number.MAX_SAFE_INTEGER + 1;
        executeAndExpectError(
            () => {
                extractNumber(unsafePositiveInteger.toString());
            },
            DataTypeExtractionError,
            `Failed to parse value (${unsafePositiveInteger}) as a number: Value must be a safe number`
        );
    });

    it('should throw error if not a safe negative integer', function () {
        const unsafeNegativeInteger: number = Number.MIN_SAFE_INTEGER - 1;
        executeAndExpectError(
            () => {
                extractNumber(unsafeNegativeInteger.toString());
            },
            DataTypeExtractionError,
            `Failed to parse value (${unsafeNegativeInteger}) as a number: Value must be a safe number`
        );
    });

    it('should throw error if number is greater than Number.MAX_VALUE', function () {
        const unsafePostivieNumber: number = Number.MAX_VALUE + 1;
        executeAndExpectError(
            () => {
                extractNumber(unsafePostivieNumber);
            },
            DataTypeExtractionError,
            `Failed to parse value (${unsafePostivieNumber}) as a number: Value must be a safe number`
        );
    });
});
