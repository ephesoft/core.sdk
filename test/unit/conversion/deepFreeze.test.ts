import { deepFreeze } from '../../../src/conversion/deepFreeze';
import { executeAndExpectError } from '@ephesoft/test.assertions';
import { expect } from 'chai';

describe('deepFreeze unit tests', function () {
    it('should do nothing for null', function () {
        // @ts-expect-error - force in null value
        expect(deepFreeze(null)).to.equal(null);
    });

    it('should do nothing for undefined', function () {
        // @ts-expect-error - force in undefined value
        expect(deepFreeze(undefined)).to.equal(undefined);
    });

    it('should freeze simple object', function () {
        const frozen = deepFreeze({ key: 'value' });
        executeAndExpectError(
            () => {
                frozen.key = 'someothervalue';
            },
            Error,
            /Cannot assign to read only property 'key'/s
        );
        executeAndExpectError(
            () => {
                (frozen as Record<string, unknown>).newKey = 'test';
            },
            Error,
            /Cannot add property newKey/s
        );
    });

    it('should freeze nested object', function () {
        const frozen = deepFreeze({ inner: { key: 'value' } });
        executeAndExpectError(
            () => {
                frozen.inner = { key: 'someothervalue' };
            },
            Error,
            /Cannot assign to read only property 'inner'/s
        );
        executeAndExpectError(
            () => {
                frozen.inner.key = 'someothervalue';
            },
            Error,
            /Cannot assign to read only property 'key'/s
        );
    });

    it('should freeze recursive object', function () {
        const outer = {
            inner: { outer: {} }
        };
        outer.inner = { outer };
        const frozen = deepFreeze(outer);
        executeAndExpectError(
            () => {
                frozen.inner = { outer: {} };
            },
            Error,
            /Cannot assign to read only property 'inner'/s
        );
        executeAndExpectError(
            () => {
                frozen.inner.outer = { inner: {} };
            },
            Error,
            /Cannot assign to read only property 'outer'/s
        );
    });

    it('should freeze primitive array', function () {
        const frozen = deepFreeze([1, 2, 3]);
        executeAndExpectError(
            () => {
                frozen[0] = 42;
            },
            Error,
            /Cannot assign to read only property '0'/s
        );
        executeAndExpectError(
            () => {
                frozen.push(42);
            },
            Error,
            /Cannot add property 3/s
        );
    });

    it('should freeze object array', function () {
        const frozen = deepFreeze([
            {
                key: 'value1'
            },
            {
                key: 'value2'
            },
            {
                key: 'value3'
            }
        ]);
        executeAndExpectError(
            () => {
                frozen[0] = { key: 'someothervalue' };
            },
            Error,
            /Cannot assign to read only property '0'/s
        );
        executeAndExpectError(
            () => {
                frozen[0].key = 'someothervalue';
            },
            Error,
            /Cannot assign to read only property 'key'/s
        );
    });

    it('should freeze deep structure', function () {
        const frozen = deepFreeze({
            array: [
                {
                    array2: [1, 2, 3]
                }
            ]
        });
        executeAndExpectError(
            () => {
                frozen.array = [];
            },
            Error,
            /Cannot assign to read only property 'array'/s
        );
        executeAndExpectError(
            () => {
                frozen.array[0] = { array2: [] };
            },
            Error,
            /Cannot assign to read only property '0'/s
        );
        executeAndExpectError(
            () => {
                frozen.array[0].array2 = [];
            },
            Error,
            /Cannot assign to read only property 'array2'/s
        );
        executeAndExpectError(
            () => {
                frozen.array[0].array2[1] = 42;
            },
            Error,
            /Cannot assign to read only property '1'/s
        );
    });
});
