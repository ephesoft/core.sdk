import { assert } from 'chai';
import * as Conversion from '../../../src/conversion/conversion';
import { ConversionError } from '../../../src/conversion/ConversionError';

describe('conversion unit tests', (): void => {
    describe('toBoolean(unknown, boolean?): boolean', (): void => {
        describe('No default value provided', (): void => {
            // nullish values

            it('should throw error if value is undefined', (): void => {
                try {
                    Conversion.toBoolean(undefined);
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.strictEqual(e.message, 'Value (undefined) could not be converted to a boolean');
                }
            });

            it('should throw error if value is null', (): void => {
                try {
                    Conversion.toBoolean(null);
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.strictEqual(e.message, 'Value (null) could not be converted to a boolean');
                }
            });

            // boolean argument

            it('should return true if value is boolean true', (): void => {
                assert.strictEqual(Conversion.toBoolean(true), true);
            });

            it('should return false if value is boolean false', (): void => {
                assert.strictEqual(Conversion.toBoolean(false), false);
            });

            // numeric values

            it('should throw error if value is not number', (): void => {
                try {
                    Conversion.toBoolean(1);
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.strictEqual(e.message, 'Value (1) could not be converted to a boolean');
                }
            });

            // string values

            it('should return true if value is "true"', (): void => {
                assert.strictEqual(Conversion.toBoolean('true'), true);
            });

            it('should return true if value is "tRUe"', (): void => {
                assert.strictEqual(Conversion.toBoolean('tRUe'), true);
            });

            it('should return false if value is "false"', (): void => {
                assert.strictEqual(Conversion.toBoolean('false'), false);
            });

            it('should return false if value is "fALSe"', (): void => {
                assert.strictEqual(Conversion.toBoolean('fALSe'), false);
            });

            it('should throw error if value is not "true" or "false"', (): void => {
                try {
                    Conversion.toBoolean('yes');
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.strictEqual(e.message, 'Value (yes) could not be converted to a boolean');
                }
            });

            it('should throw error if value is empty string', (): void => {
                try {
                    Conversion.toBoolean('');
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.strictEqual(e.message, 'Value () could not be converted to a boolean');
                }
            });
        });

        describe('Default value provided', (): void => {
            // nullish values

            it('should return default if value is undefined', (): void => {
                assert.strictEqual(Conversion.toBoolean(undefined, true), true);
            });

            it('should return default if value is null', (): void => {
                assert.strictEqual(Conversion.toBoolean(null, true), true);
            });

            // boolean argument

            it('should return true if value is boolean true', (): void => {
                assert.strictEqual(Conversion.toBoolean(true, false), true);
            });

            it('should return false if value is boolean false', (): void => {
                assert.strictEqual(Conversion.toBoolean(false, true), false);
            });

            // numeric values

            it('should return default if value is number', (): void => {
                // We do not support number-to-boolean conversion
                assert.strictEqual(Conversion.toBoolean(1, false), false);
            });

            // string values

            it('should return true if value is "true"', (): void => {
                assert.strictEqual(Conversion.toBoolean('true', false), true);
            });

            it('should return true if value is "tRUe"', (): void => {
                assert.strictEqual(Conversion.toBoolean('tRUe', false), true);
            });

            it('should return false if value is "false"', (): void => {
                assert.strictEqual(Conversion.toBoolean('false', true), false);
            });

            it('should return false if value is "fALSe"', (): void => {
                assert.strictEqual(Conversion.toBoolean('fALSe', true), false);
            });

            it('should return default if value is not "true" or "false"', (): void => {
                assert.strictEqual(Conversion.toBoolean('yes', false), false);
            });

            it('should return default if value is is empty string', (): void => {
                assert.strictEqual(Conversion.toBoolean('', true), true);
            });
        });
    });
});
