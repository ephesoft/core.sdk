import { assert } from 'chai';
import * as StringConversion from '../../../src/conversion/stringConversion';
import { ConversionError } from '../../../src/conversion/ConversionError';

describe('stringConversion unit tests', (): void => {
    describe('isDigit(string|undefined|null): boolean', (): void => {
        it('should return true for digit', (): void => {
            for (let number = 0; number < 10; number++) {
                assert.strictEqual(StringConversion.isDigit(number.toString()), true);
            }
        });

        it('should return false for non digit', (): void => {
            assert.strictEqual(StringConversion.isDigit('x'), false);
            assert.strictEqual(StringConversion.isDigit('.'), false);
            assert.strictEqual(StringConversion.isDigit('-'), false);
        });

        it('should return false for undefined', (): void => {
            assert.strictEqual(StringConversion.isDigit(undefined), false);
        });

        it('should return false for null', (): void => {
            assert.strictEqual(StringConversion.isDigit(null), false);
        });

        it('should return false for empty string', (): void => {
            assert.strictEqual(StringConversion.isDigit(''), false);
        });

        it('should return false if more than one character', (): void => {
            assert.strictEqual(StringConversion.isDigit('12'), false);
        });
    });

    describe('isNumber(string|undefined|null): boolean', (): void => {
        it('should return true for "0"', (): void => {
            assert.strictEqual(StringConversion.isNumber('0'), true);
        });

        it('should return true for "0.0"', (): void => {
            assert.strictEqual(StringConversion.isNumber('0.0'), true);
        });

        it('should return true for "-0"', (): void => {
            assert.strictEqual(StringConversion.isNumber('-0'), true);
        });

        it('should return true for "1"', (): void => {
            assert.strictEqual(StringConversion.isNumber('1'), true);
        });

        it('should return true for "-1"', (): void => {
            assert.strictEqual(StringConversion.isNumber('-1'), true);
        });

        it('should return true for "1.0"', (): void => {
            assert.strictEqual(StringConversion.isNumber('1.0'), true);
        });

        it('should return true for "-1.0"', (): void => {
            assert.strictEqual(StringConversion.isNumber('-1.0'), true);
        });

        it('should return true for ".1"', (): void => {
            assert.strictEqual(StringConversion.isNumber('.1'), true);
        });

        it('should return true for "-.1"', (): void => {
            assert.strictEqual(StringConversion.isNumber('-.1'), true);
        });

        it('should return true for "0.1"', (): void => {
            assert.strictEqual(StringConversion.isNumber('0.1'), true);
        });

        it('should return true for "-0.1"', (): void => {
            assert.strictEqual(StringConversion.isNumber('-0.1'), true);
        });

        it('should return true for "1.11"', (): void => {
            assert.strictEqual(StringConversion.isNumber('1.01'), true);
        });

        it('should return true for "-1.11"', (): void => {
            assert.strictEqual(StringConversion.isNumber('-1.01'), true);
        });

        it('should return false for undefined', (): void => {
            assert.strictEqual(StringConversion.isNumber(undefined), false);
        });

        it('should return false for null', (): void => {
            assert.strictEqual(StringConversion.isNumber(null), false);
        });

        it('should return false for empty string', (): void => {
            assert.strictEqual(StringConversion.isNumber(''), false);
        });

        it('should return false for misplaced -', (): void => {
            assert.strictEqual(StringConversion.isNumber('123-'), false);
        });

        it('should return false for multiple decimal points', (): void => {
            assert.strictEqual(StringConversion.isNumber('1.1.1'), false);
        });

        it('should return false for non-numeric', (): void => {
            assert.strictEqual(StringConversion.isNumber('abc'), false);
        });

        it('should return false for mixed numeric and non-numeric', (): void => {
            assert.strictEqual(StringConversion.isNumber('123abc'), false);
        });

        it('should return false if decimal point is last character', (): void => {
            assert.strictEqual(StringConversion.isNumber('100.'), false);
        });

        it('should return false for "-"', (): void => {
            assert.strictEqual(StringConversion.isNumber('-'), false);
        });

        it('should return false for "."', (): void => {
            assert.strictEqual(StringConversion.isNumber('.'), false);
        });

        it('should disallow scientific notation', (): void => {
            // For our purposes, scientific notation would likely only confuse our users
            assert.strictEqual(StringConversion.isNumber('5e-324'), false);
        });
    });

    describe('toNumber(string|undefined|null, number?): number', (): void => {
        describe('No default value provided', (): void => {
            it('should convert "0"', (): void => {
                assert.strictEqual(StringConversion.toNumber('0'), 0);
            });

            it('should convert "0.0"', (): void => {
                assert.strictEqual(StringConversion.toNumber('0.0'), 0);
            });

            it('should convert "-0"', (): void => {
                assert.strictEqual(StringConversion.toNumber('-0'), -0);
            });

            it('should convert "1"', (): void => {
                assert.strictEqual(StringConversion.toNumber('1'), 1);
            });

            it('should convert "-1"', (): void => {
                assert.strictEqual(StringConversion.toNumber('-1'), -1);
            });

            it('should convert "1.0"', (): void => {
                assert.strictEqual(StringConversion.toNumber('1.0'), 1);
            });

            it('should convert "-1.0"', (): void => {
                assert.strictEqual(StringConversion.toNumber('-1.0'), -1);
            });

            it('should convert ".1"', (): void => {
                assert.strictEqual(StringConversion.toNumber('.1'), 0.1);
            });

            it('should convert "-.1"', (): void => {
                assert.strictEqual(StringConversion.toNumber('-.1'), -0.1);
            });

            it('should convert "0.1"', (): void => {
                assert.strictEqual(StringConversion.toNumber('0.1'), 0.1);
            });

            it('should convert "-0.1"', (): void => {
                assert.strictEqual(StringConversion.toNumber('-0.1'), -0.1);
            });

            it('should convert "1.11"', (): void => {
                assert.strictEqual(StringConversion.toNumber('1.01'), 1.01);
            });

            it('should convert "-1.11"', (): void => {
                assert.strictEqual(StringConversion.toNumber('-1.01'), -1.01);
            });

            it('should throw error for undefined', (): void => {
                try {
                    StringConversion.toNumber(undefined);
                    assert.fail('expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.equal(e.message, 'Value (undefined) could not be converted to a number');
                }
            });

            it('should return throw error for null', (): void => {
                try {
                    StringConversion.toNumber(null);
                    assert.fail('expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.equal(e.message, 'Value (null) could not be converted to a number');
                }
            });

            it('should throw error for empty string', (): void => {
                try {
                    StringConversion.toNumber('');
                    assert.fail('expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.equal(e.message, 'Value () could not be converted to a number');
                }
            });

            it('should throw error for misplaced -', (): void => {
                try {
                    StringConversion.toNumber('123-');
                    assert.fail('expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.equal(e.message, 'Value (123-) could not be converted to a number');
                }
            });

            it('should throw error for multiple decimal points', (): void => {
                try {
                    StringConversion.toNumber('1.1.1');
                    assert.fail('expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.equal(e.message, 'Value (1.1.1) could not be converted to a number');
                }
            });

            it('should throw error for non-numeric', (): void => {
                try {
                    StringConversion.toNumber('abc');
                    assert.fail('expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.equal(e.message, 'Value (abc) could not be converted to a number');
                }
            });

            it('should throw error for mixed numeric and non-numeric', (): void => {
                try {
                    StringConversion.toNumber('123abc');
                    assert.fail('expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.equal(e.message, 'Value (123abc) could not be converted to a number');
                }
            });

            it('should throw error if decimal point is last character', (): void => {
                try {
                    StringConversion.toNumber('100.');
                    assert.fail('expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.equal(e.message, 'Value (100.) could not be converted to a number');
                }
            });

            it('should throw error for "-"', (): void => {
                try {
                    StringConversion.toNumber('-');
                    assert.fail('expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.equal(e.message, 'Value (-) could not be converted to a number');
                }
            });

            it('should throw error for "."', (): void => {
                try {
                    StringConversion.toNumber('.');
                    assert.fail('expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.equal(e.message, 'Value (.) could not be converted to a number');
                }
            });

            it('should throw error if text has scientific notation', (): void => {
                // For our purposes, scientific notation would likely only confuse our users
                try {
                    StringConversion.toNumber('5e-324');
                    assert.fail('expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.equal(e.message, 'Value (5e-324) could not be converted to a number');
                }
            });

            it('should throw error if text value too large', (): void => {
                // Too large to fit in number
                const text = '9'.repeat(1000);
                try {
                    StringConversion.toNumber(text);
                    assert.fail('expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof ConversionError)) {
                        throw e;
                    }
                    assert.equal(e.message, `Value (${text}) could not be converted to a number`);
                }
            });
        });

        describe('Default value provided', (): void => {
            it('should convert "0"', (): void => {
                assert.strictEqual(StringConversion.toNumber('0', 42), 0);
            });

            it('should convert "0.0"', (): void => {
                assert.strictEqual(StringConversion.toNumber('0.0', 42), 0);
            });

            it('should convert "-0"', (): void => {
                assert.strictEqual(StringConversion.toNumber('-0', 42), -0);
            });

            it('should convert "1"', (): void => {
                assert.strictEqual(StringConversion.toNumber('1', 42), 1);
            });

            it('should convert "-1"', (): void => {
                assert.strictEqual(StringConversion.toNumber('-1', 42), -1);
            });

            it('should convert "1.0"', (): void => {
                assert.strictEqual(StringConversion.toNumber('1.0', 42), 1);
            });

            it('should convert "-1.0"', (): void => {
                assert.strictEqual(StringConversion.toNumber('-1.0', 42), -1);
            });

            it('should convert ".1"', (): void => {
                assert.strictEqual(StringConversion.toNumber('.1', 42), 0.1);
            });

            it('should convert "-.1"', (): void => {
                assert.strictEqual(StringConversion.toNumber('-.1', 42), -0.1);
            });

            it('should convert "0.1"', (): void => {
                assert.strictEqual(StringConversion.toNumber('0.1', 42), 0.1);
            });

            it('should convert "-0.1"', (): void => {
                assert.strictEqual(StringConversion.toNumber('-0.1', 42), -0.1);
            });

            it('should convert "1.11"', (): void => {
                assert.strictEqual(StringConversion.toNumber('1.01', 42), 1.01);
            });

            it('should convert "-1.11"', (): void => {
                assert.strictEqual(StringConversion.toNumber('-1.01', 42), -1.01);
            });

            it('should return default value for undefined', (): void => {
                assert.strictEqual(StringConversion.toNumber(undefined, 42), 42);
            });

            it('should return return default value for null', (): void => {
                assert.strictEqual(StringConversion.toNumber(null, 42), 42);
            });

            it('should return default value for empty string', (): void => {
                assert.strictEqual(StringConversion.toNumber('', 42), 42);
            });

            it('should return default value for misplaced -', (): void => {
                assert.strictEqual(StringConversion.toNumber('123-', 42), 42);
            });

            it('should return default value for multiple decimal points', (): void => {
                assert.strictEqual(StringConversion.toNumber('1.1.1', 42), 42);
            });

            it('should return default value for non-numeric', (): void => {
                assert.strictEqual(StringConversion.toNumber('abc', 42), 42);
            });

            it('should return default value for mixed numeric and non-numeric', (): void => {
                assert.strictEqual(StringConversion.toNumber('123abc', 42), 42);
            });

            it('should return default value if decimal point is last character', (): void => {
                assert.strictEqual(StringConversion.toNumber('100.', 42), 42);
            });

            it('should return default value for "-"', (): void => {
                assert.strictEqual(StringConversion.toNumber('-', 42), 42);
            });

            it('should return default value for "."', (): void => {
                assert.strictEqual(StringConversion.toNumber('.', 42), 42);
            });

            it('should return default value if text has scientific notation', (): void => {
                // For our purposes, scientific notation would likely only confuse our users
                assert.strictEqual(StringConversion.toNumber('5e-324', 42), 42);
            });

            it('should return default value if text value too large', (): void => {
                // Too large to fit in number
                assert.strictEqual(StringConversion.toNumber('9'.repeat(1000), 42), 42);
            });
        });
    });

    describe('stringifySafe(value: unknown, space?: string | number): string', (): void => {
        it('should stringify undefined', (): void => {
            // This is actually different behavior than JSON.stringify, but is more correct
            // according to its typescript definition. :/
            assert.strictEqual(StringConversion.stringifySafe(undefined), 'undefined');
        });

        it('should stringify null', (): void => {
            assert.strictEqual(StringConversion.stringifySafe(null), 'null');
        });

        it('should stringify boolean', (): void => {
            assert.strictEqual(StringConversion.stringifySafe(false), 'false');
        });

        it('should stringify number', (): void => {
            assert.strictEqual(StringConversion.stringifySafe(42.42), '42.42');
        });

        it('should stringify string', (): void => {
            assert.strictEqual(StringConversion.stringifySafe('This is a test'), '"This is a test"');
        });

        it('should stringify object', (): void => {
            const value = { inner: { key: 'value' } };
            assert.strictEqual(StringConversion.stringifySafe(value), '{"inner":{"key":"value"}}');
        });

        it('should stringify object with circular reference', (): void => {
            const value = { inner: { key: {} } };
            // Inject a circular reference
            value.inner.key = value;

            assert.strictEqual(StringConversion.stringifySafe(value), '{"inner":{"key":"<CIRCULAR>"}}');
        });

        it('should stringify array', (): void => {
            const value = [true, 42, 'text', { key: 'value' }];
            assert.strictEqual(StringConversion.stringifySafe(value), '[true,42,"text",{"key":"value"}]');
        });

        it('should stringify array with circular reference', (): void => {
            const value: unknown[] = [true, 42, 'text', { key: 'value' }];
            // Inject a circular reference
            value[2] = value;

            assert.strictEqual(StringConversion.stringifySafe(value), '[true,42,"<CIRCULAR>",{"key":"value"}]');
        });

        it('should honor string space parameter', (): void => {
            const value = { inner1: { inner2: { key1: 'value1', key2: 42 } } };
            assert.strictEqual(
                StringConversion.stringifySafe(value, '  '),
                '{\n  "inner1": {\n    "inner2": {\n      "key1": "value1",\n      "key2": 42\n    }\n  }\n}'
            );
        });

        it('should honor numeric space parameter', (): void => {
            const value = { inner1: { inner2: { key1: 'value1', key2: 42 } } };
            assert.strictEqual(
                StringConversion.stringifySafe(value, 3),
                '{\n   "inner1": {\n      "inner2": {\n         "key1": "value1",\n         "key2": 42\n      }\n   }\n}'
            );
        });
    });
});
