import { executeAndExpectError } from '@ephesoft/test.assertions';
import { expect } from 'chai';
import { InvalidParameterError } from '../../../src/errors/InvalidParameterError';
import { TimeSpan } from '../../../src/conversion/TimeSpan';
import { TimeUnit } from '../../../src/conversion/TimeUnit';

describe('TimeSpan unit tests', function () {
    describe('zero static property', function () {
        it('should have the correct value', function () {
            expect(TimeSpan.zero.totalMilliseconds).to.equal(0);
        });
    });

    describe('maximum static property', function () {
        it('should have the correct value', function () {
            expect(TimeSpan.maximum.totalMilliseconds).to.equal(90071992547);
        });
    });

    describe('constructor', function () {
        it('should correctly populate object if initialized with amount and unit', function () {
            const timeSpan = new TimeSpan(1.712, TimeUnit.Hours);
            expect(timeSpan.toString()).to.equal('1 hour; 42 minutes; 43 seconds; 200 milliseconds');
        });

        it('should throw error if numeric value is provided without the unit', function () {
            const error = executeAndExpectError(
                () => {
                    // @ts-expect-error - Force missing unit parameter
                    new TimeSpan(42);
                },
                InvalidParameterError,
                'Unit must be provided when value is number'
            );
            expect(error.parameterName).to.equal('unit');
        });

        it('should throw error if numeric value is too large', function () {
            const error = executeAndExpectError(
                () => {
                    new TimeSpan(149, TimeUnit.Weeks);
                },
                InvalidParameterError,
                'Invalid value [149] - value is too large to be represented by a TimeSpan'
            );
            expect(error.parameterName).to.equal('value');
        });

        it('should throw error if numeric value is negative', function () {
            const error = executeAndExpectError(
                () => {
                    new TimeSpan(-42, TimeUnit.Milliseconds);
                },
                InvalidParameterError,
                'Invalid value [-42] - must be >= 0'
            );
            expect(error.parameterName).to.equal('value');
        });

        it('should throw error if millisecond value is float', function () {
            const error = executeAndExpectError(
                () => {
                    new TimeSpan(1.1, TimeUnit.Milliseconds);
                },
                InvalidParameterError,
                'Invalid value [1.1] - milliseconds value must be a positive integer'
            );
            expect(error.parameterName).to.equal('value');
        });

        it('should throw error if unit is invalid', function () {
            const error = executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in invalid unit
                    new TimeSpan(42, 'badunit');
                },
                InvalidParameterError,
                'Unsupported time unit [badunit]'
            );
            expect(error.parameterName).to.equal('unit');
        });
    });

    describe('factory methods', function () {
        it('should create TimeSpan for millisecond value', function () {
            const timeSpan = TimeSpan.fromMilliseconds(42);
            expect(timeSpan.toString()).to.equal('42 milliseconds');
        });

        it('should throw error if millisecond value is too large', function () {
            const error = executeAndExpectError(
                () => {
                    // One over maximum
                    TimeSpan.fromMilliseconds(90071992548);
                },
                InvalidParameterError,
                'Invalid milliseconds [90071992548] - value is too large to be represented by a TimeSpan'
            );
            expect(error.parameterName).to.equal('milliseconds');
        });

        it('should create TimeSpan for second value', function () {
            const timeSpan = TimeSpan.fromSeconds(42);
            expect(timeSpan.toString()).to.equal('42 seconds');
        });

        it('should throw error if second value is too large', function () {
            const error = executeAndExpectError(
                () => {
                    // One over maximum
                    TimeSpan.fromSeconds(90071993);
                },
                InvalidParameterError,
                'Invalid seconds [90071993] - value is too large to be represented by a TimeSpan'
            );
            expect(error.parameterName).to.equal('seconds');
        });

        it('should create TimeSpan for minute value', function () {
            const timeSpan = TimeSpan.fromMinutes(42);
            expect(timeSpan.toString()).to.equal('42 minutes');
        });

        it('should throw error if minute value is too large', function () {
            const error = executeAndExpectError(
                () => {
                    // One over maximum
                    TimeSpan.fromMinutes(1501200);
                },
                InvalidParameterError,
                'Invalid minutes [1501200] - value is too large to be represented by a TimeSpan'
            );
            expect(error.parameterName).to.equal('minutes');
        });

        it('should create TimeSpan for hour value', function () {
            const timeSpan = TimeSpan.fromHours(13);
            expect(timeSpan.toString()).to.equal('13 hours');
        });

        it('should throw error if hour value is too large', function () {
            const error = executeAndExpectError(
                () => {
                    // One over maximum
                    TimeSpan.fromHours(25020);
                },
                InvalidParameterError,
                'Invalid hours [25020] - value is too large to be represented by a TimeSpan'
            );
            expect(error.parameterName).to.equal('hours');
        });

        it('should create TimeSpan for day value', function () {
            const timeSpan = TimeSpan.fromDays(5);
            expect(timeSpan.toString()).to.equal('5 days');
        });

        it('should throw error if day value is too large', function () {
            const error = executeAndExpectError(
                () => {
                    // One over maximum
                    TimeSpan.fromDays(1043);
                },
                InvalidParameterError,
                'Invalid days [1043] - value is too large to be represented by a TimeSpan'
            );
            expect(error.parameterName).to.equal('days');
        });

        it('should create TimeSpan for week value', function () {
            const timeSpan = TimeSpan.fromWeeks(42);
            expect(timeSpan.toString()).to.equal('42 weeks');
        });

        it('should throw error if week value is too large', function () {
            const error = executeAndExpectError(
                () => {
                    // One over maximum
                    TimeSpan.fromWeeks(149);
                },
                InvalidParameterError,
                'Invalid weeks [149] - value is too large to be represented by a TimeSpan'
            );
            expect(error.parameterName).to.equal('weeks');
        });

        it('should create TimeSpan for year value', function () {
            const timeSpan = TimeSpan.fromYears(2);
            expect(timeSpan.toString()).to.equal('2 years');
        });

        it('should throw error if year value is too large', function () {
            const error = executeAndExpectError(
                () => {
                    // One over maximum
                    TimeSpan.fromYears(3);
                },
                InvalidParameterError,
                'Invalid years [3] - value is too large to be represented by a TimeSpan'
            );
            expect(error.parameterName).to.equal('years');
        });

        it('should have correct behavior for zero value', function () {
            expect(TimeSpan.fromYears(0).totalMilliseconds).to.equal(0);
            expect(TimeSpan.fromWeeks(0).totalMilliseconds).to.equal(0);
            expect(TimeSpan.fromDays(0).totalMilliseconds).to.equal(0);
            expect(TimeSpan.fromHours(0).totalMilliseconds).to.equal(0);
            expect(TimeSpan.fromMinutes(0).totalMilliseconds).to.equal(0);
            expect(TimeSpan.fromSeconds(0).totalMilliseconds).to.equal(0);
            expect(TimeSpan.fromMilliseconds(0).totalMilliseconds).to.equal(0);
        });

        describe('fromTimeString', function () {
            it('should correctly populate object', function () {
                const timeSpan = TimeSpan.fromTimeString('1.712 h');
                expect(timeSpan.toString()).to.equal('1 hour; 42 minutes; 43 seconds; 200 milliseconds');
            });

            it('should be case insensitive', function () {
                const timeSpan = TimeSpan.fromTimeString('2 HOURS 1m 3 MS');
                expect(timeSpan.toString()).to.equal('2 hours; 1 minute; 3 milliseconds');
            });

            it('should ignore , delimiters', function () {
                const timeSpan = TimeSpan.fromTimeString('2 h, 1m , 3 ms');
                expect(timeSpan.toString()).to.equal('2 hours; 1 minute; 3 milliseconds');
            });

            it('should ignore ; delimiters', function () {
                const timeSpan = TimeSpan.fromTimeString('2 hours; 1minute ; 3 ms');
                expect(timeSpan.toString()).to.equal('2 hours; 1 minute; 3 milliseconds');
            });

            it('should throw error if time string is invalid', function () {
                const error = executeAndExpectError(
                    () => {
                        TimeSpan.fromTimeString('whatevs');
                    },
                    InvalidParameterError,
                    'Invalid time string [whatevs] - must be a valid time string (ex: "1 hour", "30 s", "1.5 days", "1m 30s")'
                );
                expect(error.parameterName).to.equal('timeString');
            });

            it('should throw error if time string contains malformed numeric value', function () {
                const error = executeAndExpectError(
                    () => {
                        TimeSpan.fromTimeString(
                            '10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999 ms'
                        );
                    },
                    InvalidParameterError,
                    'Invalid time string [10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999 ms] - cannot parse [10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999] as number'
                );
                expect(error.parameterName).to.equal('timeString');
            });

            it('should throw error if time string contains bad unit', function () {
                const error = executeAndExpectError(
                    () => {
                        TimeSpan.fromTimeString('1 hour, 1 bob, 2ms');
                    },
                    InvalidParameterError,
                    'Invalid time string [1 hour, 1 bob, 2ms] - must be a valid time string (ex: "1 hour", "30 s", "1.5 days", "1m 30s")'
                );
                expect(error.parameterName).to.equal('timeString');
            });

            it('should throw error if time string contains milliseconds value that is a float', function () {
                const error = executeAndExpectError(
                    () => {
                        TimeSpan.fromTimeString('1.7ms');
                    },
                    InvalidParameterError,
                    'Invalid time string [1.7ms] - milliseconds value must be a positive integer'
                );
                expect(error.parameterName).to.equal('timeString');
            });

            it('should throw error if time string contains a time value that is too large', function () {
                const error = executeAndExpectError(
                    () => {
                        TimeSpan.fromTimeString('3y 1ms');
                    },
                    InvalidParameterError,
                    'Invalid time string [3y 1ms] - value is too large to be represented by a TimeSpan'
                );
                expect(error.parameterName).to.equal('timeString');
            });

            it('should throw error if time string represents a total value that is too large', function () {
                const error = executeAndExpectError(
                    () => {
                        TimeSpan.fromTimeString('2y 50w');
                    },
                    InvalidParameterError,
                    'Invalid time string [2y 50w] - value is too large to be represented by a TimeSpan'
                );
                expect(error.parameterName).to.equal('timeString');
            });
        });
    });

    describe('integer value', function () {
        it('should correctly represent 1 year', function () {
            const timeSpan = TimeSpan.fromTimeString('1y');
            validateTimeSpan(timeSpan, {
                years: 1,
                weeks: 0,
                days: 0,
                hours: 0,
                minutes: 0,
                seconds: 0,
                milliseconds: 0,

                totalYears: 1,
                totalWeeks: 52.14286,
                totalDays: 365,
                totalHours: 8760,
                totalMinutes: 525600,
                totalSeconds: 31536000,
                totalMilliseconds: 31536000000
            });
        });

        it('should correctly represent 1 week', function () {
            const timeSpan = TimeSpan.fromTimeString('1w');
            validateTimeSpan(timeSpan, {
                years: 0,
                weeks: 1,
                days: 0,
                hours: 0,
                minutes: 0,
                seconds: 0,
                milliseconds: 0,

                totalYears: 0.01918,
                totalWeeks: 1,
                totalDays: 7,
                totalHours: 168,
                totalMinutes: 10080,
                totalSeconds: 604800,
                totalMilliseconds: 604800000
            });
        });

        it('should correctly represent 1 day', function () {
            const timeSpan = TimeSpan.fromTimeString('1d');
            validateTimeSpan(timeSpan, {
                years: 0,
                weeks: 0,
                days: 1,
                hours: 0,
                minutes: 0,
                seconds: 0,
                milliseconds: 0,

                totalYears: 0.00274,
                totalWeeks: 0.14286,
                totalDays: 1,
                totalHours: 24,
                totalMinutes: 1440,
                totalSeconds: 86400,
                totalMilliseconds: 86400000
            });
        });

        it('should correctly represent 1 hour', function () {
            const timeSpan = TimeSpan.fromTimeString('1h');
            validateTimeSpan(timeSpan, {
                years: 0,
                weeks: 0,
                days: 0,
                hours: 1,
                minutes: 0,
                seconds: 0,
                milliseconds: 0,

                totalYears: 0.00011,
                totalWeeks: 0.00595,
                totalDays: 0.04167,
                totalHours: 1,
                totalMinutes: 60,
                totalSeconds: 3600,
                totalMilliseconds: 3600000
            });
        });

        it('should correctly represent 1 minute', function () {
            const timeSpan = TimeSpan.fromTimeString('1m');
            validateTimeSpan(timeSpan, {
                years: 0,
                weeks: 0,
                days: 0,
                hours: 0,
                minutes: 1,
                seconds: 0,
                milliseconds: 0,

                totalYears: 0,
                totalWeeks: 0.0001,
                totalDays: 0.00069,
                totalHours: 0.01667,
                totalMinutes: 1,
                totalSeconds: 60,
                totalMilliseconds: 60000
            });
        });

        it('should correctly represent 1 second', function () {
            const timeSpan = TimeSpan.fromTimeString('1s');
            validateTimeSpan(timeSpan, {
                years: 0,
                weeks: 0,
                days: 0,
                hours: 0,
                minutes: 0,
                seconds: 1,
                milliseconds: 0,

                totalYears: 0,
                totalWeeks: 0,
                totalDays: 0.00001,
                totalHours: 0.00028,
                totalMinutes: 0.01667,
                totalSeconds: 1,
                totalMilliseconds: 1000
            });
        });

        it('should correctly represent 1 millisecond', function () {
            const timeSpan = TimeSpan.fromTimeString('1ms');
            validateTimeSpan(timeSpan, {
                years: 0,
                weeks: 0,
                days: 0,
                hours: 0,
                minutes: 0,
                seconds: 0,
                milliseconds: 1,

                totalYears: 0,
                totalWeeks: 0,
                totalDays: 0,
                totalHours: 0,
                totalMinutes: 0.00002,
                totalSeconds: 0.001,
                totalMilliseconds: 1
            });
        });

        it('should handle 1 in each unit', function () {
            const timeSpan = TimeSpan.fromTimeString('1y1w1d1h1m1s1ms');
            validateTimeSpan(timeSpan, {
                years: 1,
                weeks: 1,
                days: 1,
                hours: 1,
                minutes: 1,
                seconds: 1,
                milliseconds: 1,

                totalYears: 1.02203,
                totalWeeks: 53.29177,
                totalDays: 373.04237,
                totalHours: 8953.01694,
                totalMinutes: 537181.01668,
                totalSeconds: 32230861.001,
                totalMilliseconds: 32230861001
            });
        });
    });

    describe('float value', function () {
        it('should correctly represent 2.5 years', function () {
            const timeSpan = TimeSpan.fromTimeString('2.5y');
            validateTimeSpan(timeSpan, {
                years: 2,
                weeks: 26,
                days: 0,
                hours: 12,
                minutes: 0,
                seconds: 0,
                milliseconds: 0,

                totalYears: 2.5,
                totalWeeks: 130.35714,
                totalDays: 912.5,
                totalHours: 21900,
                totalMinutes: 1314000,
                totalSeconds: 78840000,
                totalMilliseconds: 78840000000
            });
        });

        it('should correctly represent 2.5 weeks', function () {
            const timeSpan = TimeSpan.fromTimeString('2.5w');
            validateTimeSpan(timeSpan, {
                years: 0,
                weeks: 2,
                days: 3,
                hours: 12,
                minutes: 0,
                seconds: 0,
                milliseconds: 0,

                totalYears: 0.04795,
                totalWeeks: 2.5,
                totalDays: 17.5,
                totalHours: 420,
                totalMinutes: 25200,
                totalSeconds: 1512000,
                totalMilliseconds: 1512000000
            });
        });

        it('should correctly represent 2.5 days', function () {
            const timeSpan = TimeSpan.fromTimeString('2.5d');
            validateTimeSpan(timeSpan, {
                years: 0,
                weeks: 0,
                days: 2,
                hours: 12,
                minutes: 0,
                seconds: 0,
                milliseconds: 0,

                totalYears: 0.00685,
                totalWeeks: 0.35714,
                totalDays: 2.5,
                totalHours: 60,
                totalMinutes: 3600,
                totalSeconds: 216000,
                totalMilliseconds: 216000000
            });
        });

        it('should correctly represent 2.5 hours', function () {
            const timeSpan = TimeSpan.fromTimeString('2.5h');
            validateTimeSpan(timeSpan, {
                years: 0,
                weeks: 0,
                days: 0,
                hours: 2,
                minutes: 30,
                seconds: 0,
                milliseconds: 0,

                totalYears: 0.00029,
                totalWeeks: 0.01488,
                totalDays: 0.10417,
                totalHours: 2.5,
                totalMinutes: 150,
                totalSeconds: 9000,
                totalMilliseconds: 9000000
            });
        });

        it('should correctly represent 2.5 minutes', function () {
            const timeSpan = TimeSpan.fromTimeString('2.5m');
            validateTimeSpan(timeSpan, {
                years: 0,
                weeks: 0,
                days: 0,
                hours: 0,
                minutes: 2,
                seconds: 30,
                milliseconds: 0,

                totalYears: 0,
                totalWeeks: 0.00025,
                totalDays: 0.00174,
                totalHours: 0.04167,
                totalMinutes: 2.5,
                totalSeconds: 150,
                totalMilliseconds: 150000
            });
        });

        it('should correctly represent 2.5 seconds', function () {
            const timeSpan = TimeSpan.fromTimeString('2.5s');
            validateTimeSpan(timeSpan, {
                years: 0,
                weeks: 0,
                days: 0,
                hours: 0,
                minutes: 0,
                seconds: 2,
                milliseconds: 500,

                totalYears: 0,
                totalWeeks: 0,
                totalDays: 0.00003,
                totalHours: 0.00069,
                totalMinutes: 0.04167,
                totalSeconds: 2.5,
                totalMilliseconds: 2500
            });
        });

        // NOTE: Float values are not permitted in the milliseconds field

        it('should handle 2.5 in each unit (except milliseconds)', function () {
            const timeSpan = TimeSpan.fromTimeString('2.5y2.5w2.5d2.5h2.5m2.5s');
            validateTimeSpan(timeSpan, {
                years: 2,
                weeks: 28,
                days: 6,
                hours: 14,
                minutes: 32,
                seconds: 32,
                milliseconds: 500,

                totalYears: 2.55508,
                totalWeeks: 133.22942,
                totalDays: 932.60593,
                totalHours: 22382.54236,
                totalMinutes: 1342952.54167,
                totalSeconds: 80577152.5,
                totalMilliseconds: 80577152500
            });
        });
    });

    describe('consistency', function () {
        it('should give consistent values regarless of unit', function () {
            expect(TimeSpan.fromSeconds(1.5).totalMilliseconds).to.equal(TimeSpan.fromMilliseconds(1500).totalMilliseconds);
            expect(TimeSpan.fromMinutes(1.5).totalMilliseconds).to.equal(TimeSpan.fromSeconds(90).totalMilliseconds);
            expect(TimeSpan.fromHours(1.5).totalMilliseconds).to.equal(TimeSpan.fromMinutes(90).totalMilliseconds);
            expect(TimeSpan.fromDays(1.5).totalMilliseconds).to.equal(TimeSpan.fromHours(36).totalMilliseconds);
            expect(TimeSpan.fromWeeks(1.5).totalMilliseconds).to.equal(TimeSpan.fromDays(10.5).totalMilliseconds);
            expect(TimeSpan.fromYears(1.5).totalMilliseconds).to.equal(TimeSpan.fromDays(547.5).totalMilliseconds);
        });
    });

    describe('toString', function () {
        it('should produce a string which can be read back in to the constructor', function () {
            const timeSpan1 = TimeSpan.fromTimeString('1 year 1 week 2 days 1 hour 1 minute 1 millisecond');
            const timeSpan2 = TimeSpan.fromTimeString(timeSpan1.toString());
            expect(timeSpan2.totalMilliseconds).to.equal(timeSpan1.totalMilliseconds);
        });
    });

    interface TimeSpanExpectations {
        years: number;
        weeks: number;
        days: number;
        hours: number;
        minutes: number;
        seconds: number;
        milliseconds: number;

        totalYears: number;
        totalWeeks: number;
        totalDays: number;
        totalHours: number;
        totalMinutes: number;
        totalSeconds: number;
        totalMilliseconds: number;
    }

    function validateTimeSpan(timeSpan: TimeSpan, expectations: TimeSpanExpectations): void {
        expect(timeSpan.years).to.equal(expectations.years, 'timeSpan.years returned wrong value');
        expect(timeSpan.weeks).to.equal(expectations.weeks, 'timeSpan.weeks returned wrong value');
        expect(timeSpan.days).to.equal(expectations.days, 'timeSpan.days returned wrong value');
        expect(timeSpan.hours).to.equal(expectations.hours, 'timeSpan.hours returned wrong value');
        expect(timeSpan.minutes).to.equal(expectations.minutes, 'timeSpan.minutes returned wrong value');
        expect(timeSpan.seconds).to.equal(expectations.seconds, 'timeSpan.seconds returned wrong value');
        expect(timeSpan.milliseconds).to.equal(expectations.milliseconds, 'timeSpan.milliseconds returned wrong value');

        expect(timeSpan.totalYears).to.equal(expectations.totalYears, 'timeSpan.totalYears returned wrong value');
        expect(timeSpan.totalWeeks).to.equal(expectations.totalWeeks, 'timeSpan.totalWeeks returned wrong value');
        expect(timeSpan.totalDays).to.equal(expectations.totalDays, 'timeSpan.totalDays returned wrong value');
        expect(timeSpan.totalHours).to.equal(expectations.totalHours, 'timeSpan.totalHours returned wrong value');
        expect(timeSpan.totalMinutes).to.equal(expectations.totalMinutes, 'timeSpan.totalMinutes returned wrong value');
        expect(timeSpan.totalSeconds).to.equal(expectations.totalSeconds, 'timeSpan.totalSeconds returned wrong value');
        expect(timeSpan.totalMilliseconds).to.equal(
            expectations.totalMilliseconds,
            'timeSpan.totalMilliseconds returned wrong value'
        );

        expect(timeSpan.toJSON()).to.deep.equal({
            years: expectations.years,
            weeks: expectations.weeks,
            days: expectations.days,
            hours: expectations.hours,
            minutes: expectations.minutes,
            seconds: expectations.seconds,
            milliseconds: expectations.milliseconds
        });

        function append(text: string, value: number, unit: TimeUnit): string {
            if (value === 0 && (unit !== TimeUnit.Milliseconds || text.length)) {
                return text;
            }
            let result = text;
            if (text.length) {
                result += '; ';
            }
            result += String(value);
            switch (unit) {
                case TimeUnit.Years:
                    result += ' year';
                    break;

                case TimeUnit.Weeks:
                    result += ' week';
                    break;

                case TimeUnit.Days:
                    result += ' day';
                    break;

                case TimeUnit.Hours:
                    result += ' hour';
                    break;

                case TimeUnit.Minutes:
                    result += ' minute';
                    break;

                case TimeUnit.Seconds:
                    result += ' second';
                    break;

                case TimeUnit.Milliseconds:
                    result += ' millisecond';
                    break;
            }
            if (value !== 1) {
                result += 's';
            }
            return result;
        }

        let expectedToStringValue = '';
        expectedToStringValue = append(expectedToStringValue, timeSpan.years, TimeUnit.Years);
        expectedToStringValue = append(expectedToStringValue, timeSpan.weeks, TimeUnit.Weeks);
        expectedToStringValue = append(expectedToStringValue, timeSpan.days, TimeUnit.Days);
        expectedToStringValue = append(expectedToStringValue, timeSpan.hours, TimeUnit.Hours);
        expectedToStringValue = append(expectedToStringValue, timeSpan.minutes, TimeUnit.Minutes);
        expectedToStringValue = append(expectedToStringValue, timeSpan.seconds, TimeUnit.Seconds);
        expectedToStringValue = append(expectedToStringValue, timeSpan.milliseconds, TimeUnit.Milliseconds);

        expect(timeSpan.toString()).to.equal(expectedToStringValue);
    }
});
