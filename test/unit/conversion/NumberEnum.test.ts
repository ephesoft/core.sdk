import { expect } from 'chai';
import { NumberEnum } from '../../../src/conversion/NumberEnum';
import { InvalidParameterError } from '../../../src/errors/InvalidParameterError';
import { executeAndExpectError } from '@ephesoft/test.assertions';

describe('NumberEnum unit tests', function () {
    enum TestEnum {
        Key1 = -1,
        Key2 = 0,
        Key3 = 1.1
    }

    describe('constructor', function () {
        it('should throw error if enumType parameter is not an object', function () {
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    new NumberEnum<TestEnum>('not an object');
                },
                InvalidParameterError,
                'Invalid enumType parameter - must be the type of an enum'
            );
        });

        it('should throw error if enumType contains an empty key', function () {
            executeAndExpectError(
                () => {
                    new NumberEnum<TestEnum>({ ['']: 42 });
                },
                InvalidParameterError,
                'Invalid enumType parameter - must not have any keys which are an empty string'
            );
        });

        it('should throw error if enumType contains a value which is null', function () {
            executeAndExpectError(
                () => {
                    new NumberEnum<TestEnum>({ key: null as unknown as number });
                },
                InvalidParameterError,
                'Invalid enumType parameter - must not have any values that are undefined or null'
            );
        });

        it('should throw error if enumType contains a value which is undefined', function () {
            executeAndExpectError(
                () => {
                    new NumberEnum<TestEnum>({ key: undefined as unknown as number });
                },
                InvalidParameterError,
                'Invalid enumType parameter - must not have any values that are undefined or null'
            );
        });

        it('should throw error if enumType contains a non-number value', function () {
            executeAndExpectError(
                () => {
                    new NumberEnum<TestEnum>({ key: {} as unknown as number });
                },
                InvalidParameterError,
                'Invalid enumType parameter - all values must be numbers'
            );
        });

        it('should throw error if "enum" has more regular keys than inverse keys', function () {
            executeAndExpectError(
                () => {
                    new NumberEnum<TestEnum>({ Key1: 1, Key2: 2, ['1']: 'Key1' });
                },
                InvalidParameterError,
                'Invalid enumType parameter - must be a number enum'
            );
        });

        it('should throw error if "enum" has fewer keys than inverse keys', function () {
            executeAndExpectError(
                () => {
                    new NumberEnum<TestEnum>({ Key1: 1, ['1']: 'Key1', ['2']: 'Key2' });
                },
                InvalidParameterError,
                'Invalid enumType parameter - must be a number enum'
            );
        });

        it('should throw error if "enum" has mismatched keys and inverse keys', function () {
            executeAndExpectError(
                () => {
                    new NumberEnum<TestEnum>({ Key1: 1, ['1']: 'Key2' });
                },
                InvalidParameterError,
                'Invalid enumType parameter - must be a number enum'
            );
        });

        it('should accept valid enum type', function () {
            new NumberEnum<TestEnum>(TestEnum);
            // If it didn't throw an error we're good
        });
    });

    describe('get type', function () {
        it('should return the enum type', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            expect(numberEnum.type).to.equal(TestEnum);
        });
    });

    describe('get keys', function () {
        it('should return key names in original case', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            expect(numberEnum.keys).to.deep.equal(['Key1', 'Key2', 'Key3']);
        });

        it('should return a copy of the keys - not the working copy', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            const keys = numberEnum.keys;
            keys.push('Key4');
            expect(numberEnum.keys).to.deep.equal(['Key1', 'Key2', 'Key3']);
        });
    });

    describe('get values', function () {
        it('should return values', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            expect(numberEnum.values).to.deep.equal([-1, 0, 1.1]);
        });

        it('should return a copy of the values - not the working copy', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            const values = numberEnum.values;
            values.push(42);
            expect(numberEnum.values).to.deep.equal([-1, 0, 1.1]);
        });
    });

    describe('isKey', function () {
        it('should throw error if text parameter is undefined', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    numberEnum.isKey(undefined);
                },
                InvalidParameterError,
                'Invalid text parameter - must not be undefined or null'
            );
        });

        it('should throw error if text parameter is null', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    numberEnum.isKey(null);
                },
                InvalidParameterError,
                'Invalid text parameter - must not be undefined or null'
            );
        });

        it('should return false if key not present in enum', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            // We do not trim white space as part of the conversion
            const result = numberEnum.isKey(' Key1 ');
            expect(result).to.be.false;
        });

        it('should return false if case sensitive and case does not match', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            const result = numberEnum.isKey('key1', true);
            expect(result).to.be.false;
        });

        it('should return true for direct hit when case sensitive', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            const result = numberEnum.isKey('Key1', true);
            expect(result).to.be.true;
        });

        it('should return true if not case sensitive and case does not match', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            const result = numberEnum.isKey('key1');
            expect(result).to.be.true;
        });
    });

    describe('isValue', function () {
        it('should throw error if number parameter is undefined', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    numberEnum.isValue(undefined);
                },
                InvalidParameterError,
                'Invalid number parameter - must not be undefined or null'
            );
        });

        it('should throw error if number parameter is null', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    numberEnum.isValue(null);
                },
                InvalidParameterError,
                'Invalid number parameter - must not be undefined or null'
            );
        });

        it('should return false if value not present in enum', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            // We do not trim white space as part of the conversion
            const result = numberEnum.isValue(42);
            expect(result).to.be.false;
        });

        it('should return true if value is present in enum', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            const result = numberEnum.isValue(1.1);
            expect(result).to.be.true;
        });
    });

    describe('convertToKey', function () {
        it('should throw error if text parameter is undefined', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    numberEnum.convertToKey(undefined);
                },
                InvalidParameterError,
                'Invalid text parameter (undefined) - must be one of: Key1, Key2, Key3'
            );
        });

        it('should throw error if text is not an enum key', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    numberEnum.convertToKey('key4');
                },
                InvalidParameterError,
                'Invalid text parameter (key4) - must be one of: Key1, Key2, Key3'
            );
        });

        it('should throw error if case sensitive and case does not match', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    numberEnum.convertToKey('key1', true);
                },
                InvalidParameterError,
                'Invalid text parameter (key1) - must be one of: Key1, Key2, Key3'
            );
        });

        it('should return key if case sensitive and case matches', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            const key = numberEnum.convertToKey('Key1', true);
            expect(key).to.equal('Key1');
        });

        it('should return key if not case sensitive and case does not match', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            const key = numberEnum.convertToKey('key2');
            expect(key).to.equal('Key2');
        });
    });

    describe('convertToValue', function () {
        it('should throw error if number parameter is undefined', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    numberEnum.convertToValue(undefined);
                },
                InvalidParameterError,
                'Invalid number parameter (undefined) - must be one of: -1, 0, 1.1'
            );
        });

        it('should throw error if number is not an enum value', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    numberEnum.convertToValue(42);
                },
                InvalidParameterError,
                'Invalid number parameter (42) - must be one of: -1, 0, 1.1'
            );
        });

        it('should return value if number is an enum value', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            const value = numberEnum.convertToValue(-1);
            expect(value).to.equal(-1);
        });
    });

    describe('getValueForKey', function () {
        it('should throw error if key parameter is undefined', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    numberEnum.getValueForKey(undefined);
                },
                InvalidParameterError,
                'Invalid key parameter (undefined) - must be one of: Key1, Key2, Key3'
            );
        });

        it('should throw error if key is not a key', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    numberEnum.getValueForKey('key4');
                },
                InvalidParameterError,
                'Invalid key parameter (key4) - must be one of: Key1, Key2, Key3'
            );
        });

        it('should throw error if case sensitive and case does not match', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    numberEnum.getValueForKey('key1', true);
                },
                InvalidParameterError,
                'Invalid key parameter (key1) - must be one of: Key1, Key2, Key3'
            );
        });

        it('should return value if case sensitive and case matches', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            const value = numberEnum.getValueForKey('Key1', true);
            expect(value).to.equal(-1);
        });

        it('should return value if not case sensitive and case does not match', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            const value = numberEnum.getValueForKey('key2');
            expect(value).to.equal(0);
        });
    });

    describe('getKeyForValue', function () {
        it('should throw error if value parameter is undefined', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    numberEnum.getKeyForValue(undefined);
                },
                InvalidParameterError,
                'Invalid value parameter (undefined) - must be one of: -1, 0, 1.1'
            );
        });

        it('should throw error if value is not an enum value', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            executeAndExpectError(
                () => {
                    numberEnum.getKeyForValue(42);
                },
                InvalidParameterError,
                'Invalid value parameter (42) - must be one of: -1, 0, 1.1'
            );
        });

        it('should return key if value is an enum value', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            const key = numberEnum.getKeyForValue(1.1);
            expect(key).to.equal('Key3');
        });
    });

    describe('toJSON', function () {
        it('should serialize nicely with JSON.stringify', function () {
            const numberEnum = new NumberEnum<TestEnum>(TestEnum);
            expect(JSON.stringify(numberEnum)).to.equal('{"Key1":-1,"Key2":0,"Key3":1.1}');
        });
    });
});
