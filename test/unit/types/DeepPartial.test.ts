import { expect } from 'chai';
import { DeepPartial } from '../../../src/types/DeepPartial';

describe('DeepPartial unit tests', function () {
    it('should make top-level fields optional', () => {
        interface RequiredType {
            property1: string;
            property2: number;
            optional?: boolean;
        }
        const requiredInstance: RequiredType = { property1: 'test', property2: 42, optional: false };

        const partialInstance: DeepPartial<RequiredType> = requiredInstance;
        partialInstance.property1 = undefined;
        partialInstance.property2 = undefined;
        partialInstance.optional = undefined;

        // Verify we modified the original instance
        expect(requiredInstance).to.deep.equal({
            property1: undefined,
            property2: undefined,
            optional: undefined
        });
    });

    it('should make nested fields mutable', () => {
        interface RequiredType {
            arrayField: {
                property1: string;
                property2: number;
                optional?: boolean;
            }[];
        }
        const requiredInstance: RequiredType = { arrayField: [{ property1: 'test', property2: 42, optional: false }] };

        const partialInstance: DeepPartial<RequiredType> = requiredInstance;
        // Because we've made it a partial, we have to convince typescript everything exists
        if (partialInstance.arrayField && partialInstance.arrayField[0]) {
            partialInstance.arrayField[0].property1 = undefined;
            partialInstance.arrayField[0].property2 = undefined;
            partialInstance.arrayField[0].optional = undefined;
        } else {
            expect.fail('partialInstance.arrayField or partialInstance.arrayField[0] is undefined');
        }

        // Verify we modified the original instance
        expect(requiredInstance).to.deep.equal({
            arrayField: [
                {
                    property1: undefined,
                    property2: undefined,
                    optional: undefined
                }
            ]
        });
    });
});
