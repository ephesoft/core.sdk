import { expect } from 'chai';
import { DeepRequired } from '../../../src/types/DeepRequired';

describe('DeepRequired unit tests', function () {
    it('should make top-level fields required', () => {
        interface PartialType {
            property1?: string;
            property2?: number;
            required: boolean;
        }
        const partialInstance: PartialType = { property1: 'test', property2: 42, required: false };

        const requiredInstance = partialInstance as DeepRequired<PartialType>;
        // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the DeepRequired type is broken
        requiredInstance.property1 = undefined;
        // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the DeepRequired type is broken
        requiredInstance.property2 = undefined;
        // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the DeepRequired type is broken
        requiredInstance.required = undefined;

        // Verify we modified the original instance
        expect(partialInstance).to.deep.equal({
            property1: undefined,
            property2: undefined,
            required: undefined
        });
    });

    it('should make nested fields required', () => {
        interface PartialType {
            arrayField: {
                property1?: string;
                property2?: number;
                required: boolean;
            }[];
            property3?: string;
        }
        const partialInstance: PartialType = { arrayField: [{ property1: 'test', property2: 42, required: false }] };

        const requiredInstance = partialInstance as DeepRequired<PartialType>;
        // Convince typescript the first array entry exists
        if (requiredInstance.arrayField[0]) {
            // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the DeepRequired type is broken
            requiredInstance.arrayField[0].property1 = undefined;
            // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the DeepRequired type is broken
            requiredInstance.arrayField[0].property2 = undefined;
            // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the DeepRequired type is broken
            requiredInstance.arrayField[0].required = undefined;
            // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the DeepRequired type is broken
            requiredInstance.property3 = undefined;
        } else {
            expect.fail('requiredInstance.arrayField[0] is undefined');
        }

        // Verify we modified the original instance
        expect(partialInstance).to.deep.equal({
            arrayField: [
                {
                    property1: undefined,
                    property2: undefined,
                    required: undefined
                }
            ],
            property3: undefined
        });
    });
});
