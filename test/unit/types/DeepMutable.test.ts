import { expect } from 'chai';
import { DeepMutable } from '../../../src/types/DeepMutable';

describe('DeepMutable unit tests', function () {
    it('should make top-level fields mutable', () => {
        interface Immutable {
            readonly property1: string;
            readonly property2: number;
            writable: boolean;
        }
        const immutableInstance: Immutable = { property1: 'test', property2: 42, writable: false };

        const mutableInstance: DeepMutable<Immutable> = immutableInstance;
        mutableInstance.property1 = 'new';
        mutableInstance.property2 = 1968;
        mutableInstance.writable = true;

        // Verify we modified the original instance
        expect(immutableInstance).to.deep.equal({
            property1: 'new',
            property2: 1968,
            writable: true
        });
    });

    it('should make nested fields mutable', () => {
        interface Immutable {
            arrayField: {
                readonly property1: string;
                readonly property2: number;
                writable: boolean;
            }[];
        }
        const immutableInstance: Immutable = { arrayField: [{ property1: 'test', property2: 42, writable: false }] };

        const mutableInstance: DeepMutable<Immutable> = immutableInstance;
        mutableInstance.arrayField[0].property1 = 'new';
        mutableInstance.arrayField[0].property2 = 1968;
        mutableInstance.arrayField[0].writable = true;

        // Verify we modified the original instance
        expect(immutableInstance).to.deep.equal({
            arrayField: [
                {
                    property1: 'new',
                    property2: 1968,
                    writable: true
                }
            ]
        });
    });
});
