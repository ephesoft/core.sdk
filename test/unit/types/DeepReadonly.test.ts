import { expect } from 'chai';
import { DeepReadonly } from '../../../src/types/DeepReadonly';

describe('DeepReadonly unit tests', function () {
    it('should make top-level fields readonly', () => {
        interface MutableType {
            property1: string;
            property2: number;
            readonly immutable: boolean;
        }
        const mutableInstance: MutableType = { property1: 'test', property2: 42, immutable: false };

        const readonlyInstance: DeepReadonly<MutableType> = mutableInstance;
        // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the DeepReadonly type is broken
        readonlyInstance.property1 = 'new';
        // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the DeepReadonly type is broken
        readonlyInstance.property2 = 43;
        // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the DeepReadonly type is broken
        readonlyInstance.immutable = true;

        // Verify we modified the original instance
        expect(mutableInstance).to.deep.equal({
            property1: 'new',
            property2: 43,
            immutable: true
        });
    });

    it('should make nested fields readonly', () => {
        interface MutableType {
            arrayField: {
                property1: string;
                property2: number;
                readonly immutable: boolean;
            }[];
        }
        const partialInstance: MutableType = { arrayField: [{ property1: 'test', property2: 42, immutable: false }] };

        const readonlyInstance: DeepReadonly<MutableType> = partialInstance;
        // Convince typescript the first array entry exists
        if (readonlyInstance.arrayField[0]) {
            // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the DeepReadonly type is broken
            readonlyInstance.arrayField[0].property1 = 'new';
            // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the DeepReadonly type is broken
            readonlyInstance.arrayField[0].property2 = 43;
            // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the DeepReadonly type is broken
            readonlyInstance.arrayField[0].immutable = true;
        } else {
            expect.fail('readonlyInstance.arrayField[0] is undefined');
        }

        // Verify we modified the original instance
        expect(partialInstance).to.deep.equal({
            arrayField: [
                {
                    property1: 'new',
                    property2: 43,
                    immutable: true
                }
            ]
        });
    });
});
