import { assert } from 'chai';
import { Mutable } from '../../../src/types/Mutable';

describe('Mutable unit tests', (): void => {
    it('should make all fields mutable', () => {
        interface Immutable {
            readonly property1: string;
            readonly property2: number;
            writable: boolean;
        }
        const immutableInstance: Immutable = { property1: 'test', property2: 42, writable: false };

        const mutableInstance: Mutable<Immutable> = immutableInstance;
        mutableInstance.property1 = 'new';
        mutableInstance.property2 = 1968;
        mutableInstance.writable = true;

        assert.deepStrictEqual(immutableInstance, {
            property1: 'new',
            property2: 1968,
            writable: true
        });
    });
});
