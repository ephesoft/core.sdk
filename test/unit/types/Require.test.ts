import { expect } from 'chai';
import { Require } from '../../../src/types/Require';

describe('Require unit tests', function () {
    it('should make key property required and leave all others alone', function () {
        // This isn't a unit test in the strict sense of the word - basically, if it compiles the Require type is working

        interface TestType {
            key?: string;
            optional1?: string;
            optional2?: string;
            required: string;
        }

        const testInstance: TestType = {
            key: 'key',
            optional1: 'optional1',
            optional2: 'optional2',
            required: 'required'
        };

        const keyedInstance = testInstance as Require<TestType, 'key' | 'optional2'>;
        // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the Require type is broken
        keyedInstance.key = undefined;

        // The line below should be valid
        keyedInstance.optional1 = undefined;

        // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the Require type is broken
        keyedInstance.optional2 = undefined;

        // @ts-expect-error - DO NOT REMOVE: If the compiler or linter says this line is NOT required, the Require type is broken
        keyedInstance.required = undefined;

        expect(keyedInstance).to.exist;
    });
});
