import { assert } from 'chai';
import { InvalidDataTypeError } from '../../../src/errors/InvalidDataTypeError';
import { getMaskProperties } from '../../../src/mask/getMaskProperties';

describe('getMaskProperties', function () {
    describe('getMaskProperties no options', function () {
        it('returns single property', function () {
            const properties = ['p1'];
            const base = { p1: false };
            const mask = { p1: true };

            const response = getMaskProperties(base, mask, properties);

            assert.deepStrictEqual(response, properties);
        });

        it('returns multiple properties', function () {
            const properties = ['p1', 'name'];
            const base = { p1: false, name: 'oldName' };
            const mask = { p1: true, name: 'newName' };

            const response = getMaskProperties(base, mask, properties);

            assert.deepStrictEqual(response, properties);
        });

        it('returns only mutable properties', function () {
            const properties = ['p1'];
            const base = { p1: false, name: 'oldName' };
            const mask = { p1: true, name: 'newName' };

            const response = getMaskProperties(base, mask, properties);

            assert.deepStrictEqual(response, properties);
        });

        it('returns nested properties', function () {
            const properties = ['data.name'];
            const base = { data: { name: 'oldName' } };
            const mask = { data: { name: 'newName' } };

            const response = getMaskProperties(base, mask, properties);

            assert.deepStrictEqual(response, properties);
        });

        it('returns only nested mutable properties', function () {
            const properties = ['data.name'];
            const base = { data: { name: 'oldName', id: 0 } };
            const mask = { data: { name: 'newName', id: 1 } };

            const response = getMaskProperties(base, mask, properties);

            assert.deepStrictEqual(response, properties);
        });

        it('returns empty array if no properties are changed', function () {
            const properties = ['p1'];
            const base = { p1: false };
            const mask = { p1: false };

            const response = getMaskProperties(base, mask, properties);

            assert.deepStrictEqual(response, []);
        });

        it('returns empty array if no properties are mutable', function () {
            const properties: string[] = [];
            const base = { p1: false };
            const mask = { p1: true };

            const response = getMaskProperties(base, mask, properties);

            assert.deepStrictEqual(response, []);
        });

        it('returns empty array if mutable values are missing', function () {
            const properties = ['p2'];
            const base = { p1: false };
            const mask = { p1: true };

            const response = getMaskProperties(base, mask, properties);

            assert.deepStrictEqual(response, []);
        });

        it('errors when property value is an object', function () {
            const properties = ['data'];
            const base = { data: { name: 'oldName', id: 0 } };
            const mask = { data: { name: 'newName', id: 1 } };

            assert.throws(
                () => {
                    getMaskProperties(base, mask, properties);
                },
                InvalidDataTypeError,
                'Property: data - Object type not allowed as mutable properties.'
            );
        });

        it('errors when mask value is different data type', function () {
            const properties = ['p1'];
            const base = { p1: false };
            const mask = { p1: 'true' };

            assert.throws(
                () => {
                    getMaskProperties<unknown>(base, mask, properties);
                },
                InvalidDataTypeError,
                'Property: p1 - Mask type of string does not match base type of boolean.'
            );
        });
    });

    describe('getMaskProperties forbidNulls', function () {
        const options = { forbidNulls: true };
        it('errors when the mask property value is a null', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: null };

            assert.throws(
                () => {
                    getMaskProperties<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Property: p1 - Null values not allowed as mutable properties.'
            );
        });

        it('errors when the base property value is a null', function () {
            const properties = ['p1'];
            const base = { p1: null };
            const mask = { p1: true };

            assert.throws(
                () => {
                    getMaskProperties<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Property: p1 - Null values not allowed as mutable properties.'
            );
        });
    });

    describe('getMaskProperties allowObjects', function () {
        const options = { allowObjects: true };
        it('returns when both property values are an object', function () {
            const properties = ['p1'];
            const base = { p1: { p11: false } };
            const mask = { p1: { p11: true } };

            const response = getMaskProperties(base, mask, properties, options);
            assert.deepStrictEqual(response, properties);
        });

        it('errors when only base property value is an object', function () {
            const properties = ['p1'];
            const base = { p1: { p11: true } };
            const mask = { p1: true };

            assert.throws(
                () => {
                    getMaskProperties<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Property: p1 - Mask type of boolean does not match base type of object.'
            );
        });

        it('errors when only mask property value is an object', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: { p11: true } };

            assert.throws(
                () => {
                    getMaskProperties<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Property: p1 - Mask type of object does not match base type of boolean.'
            );
        });
    });

    describe('getMaskProperties allowDifferentTypes', function () {
        const options = { allowDifferentTypes: true };
        it('returns when mask value is a different type', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: 'false' };

            const response = getMaskProperties<unknown>(base, mask, properties, options);
            assert.deepStrictEqual(response, properties);
        });

        it('errors when mask value is an object', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: { p11: true } };

            assert.throws(
                () => {
                    getMaskProperties<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Property: p1 - Object type not allowed as mutable properties.'
            );
        });

        it('errors when base value is an object', function () {
            const properties = ['p1'];
            const base = { p1: { p11: true } };
            const mask = { p1: true };

            assert.throws(
                () => {
                    getMaskProperties<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Property: p1 - Object type not allowed as mutable properties.'
            );
        });
    });

    describe('getMaskProperties allowDifferentTypes and allowObjects', function () {
        const options = { allowDifferentTypes: true, allowObjects: true };
        it('returns when mask value is an object', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: { p11: true } };

            const response = getMaskProperties<unknown>(base, mask, properties, options);
            assert.deepStrictEqual(response, properties);
        });

        it('returns when base value is an object', function () {
            const properties = ['p1'];
            const base = { p1: { p11: true } };
            const mask = { p1: true };

            const response = getMaskProperties<unknown>(base, mask, properties, options);
            assert.deepStrictEqual(response, properties);
        });
    });
});
