import { assert } from 'chai';
import { applyMasks } from '../../../src/mask/applyMasks';

describe('applyMasks', function () {
    it('returns single overridden value', function () {
        const properties = ['p1'];
        const bases = [{ key: 1, p1: false }];
        const masks = [{ key: 1, p1: true }];

        const response = applyMasks(bases, masks, 'key', properties);

        assert.deepStrictEqual(response, masks);
    });

    it('returns base for non mutable properties', function () {
        const properties = ['p1'];
        const bases = [{ id: 1, p1: false, p2: 'default' }];
        const masks = [{ id: 1, p1: true, p2: 'override' }];

        const response = applyMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, [{ id: 1, p1: true, p2: 'default' }]);
    });

    it('ignores mutable properties that do not exist', function () {
        const properties = ['p1', 'missing'];
        const bases = [
            { id: 1, p1: 'default' },
            { id: 3, p1: 'default3' }
        ];
        const masks = [
            { id: 1, p1: 'override1' },
            { id: 3, p1: 'override3' }
        ];

        const response = applyMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, masks);
    });

    it('returns overridden values with multiple items in the array', function () {
        const properties = ['p1', 'p2'];
        const bases = [
            { id: 1, p1: false, p2: 'default' },
            { id: 2, p1: false, p2: 'default2' }
        ];
        const masks = [
            { id: 1, p1: true, p2: 'override' },
            { id: 2, p1: true, p2: 'override2' }
        ];

        const response = applyMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, masks);
    });

    it('ignores masks that do not have a matching default object key', function () {
        const properties = ['p1'];
        const bases = [
            { id: 1, p1: 'default' },
            { id: 3, p1: 'default3' }
        ];
        const masks = [
            { id: 1, p1: 'override' },
            { id: 2, p1: 'override2' },
            { id: 3, p1: 'override3' }
        ];

        const response = applyMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, [
            { id: 1, p1: 'override' },
            { id: 3, p1: 'override3' }
        ]);
    });

    it('returns the base when all values match the base', function () {
        const properties = ['p1'];
        const bases = [
            { id: 1, p1: 'default' },
            { id: 2, p1: 'default2' }
        ];
        const masks = [
            { id: 1, p1: 'default' },
            { id: 2, p1: 'default2' }
        ];

        const response = applyMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, bases);
    });

    it('returns the base array when no matching items exist', function () {
        const properties = ['p1'];
        const bases = [
            { id: 1, p1: 'default' },
            { id: 2, p1: 'default2' }
        ];
        const masks = [
            { id: 3, p1: 'override3' },
            { id: 4, p1: 'override4' }
        ];

        const response = applyMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, bases);
    });

    it('returns an empty array when passed in an empty default array', function () {
        const properties = ['p1'];
        const bases: {
            id: number;
            p1: string;
        }[] = [];
        const masks = [
            { id: 3, p1: 'override3' },
            { id: 4, p1: 'override4' }
        ];

        const response = applyMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, []);
    });

    it('returns base values if no key is found in mask', function () {
        const properties = ['p1'];
        const bases = [
            { id: 1, p1: 'default' },
            { id: 3, p1: 'default3' }
        ];
        const masks = [{ p1: 'override1' }, { id: 3, p1: 'override3' }];

        const response = applyMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, [
            { id: 1, p1: 'default' },
            { id: 3, p1: 'override3' }
        ]);
    });
});
