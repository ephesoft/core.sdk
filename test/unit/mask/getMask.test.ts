import { assert } from 'chai';
import { InvalidDataTypeError } from '../../../src/errors/InvalidDataTypeError';
import { getMask } from '../../../src/mask/getMask';

describe('getMask', function () {
    describe('getMask no options', function () {
        it('returns single mask value', function () {
            const properties = ['p1'];
            const base = { p1: false };
            const mask = { p1: true };

            const response = getMask(base, mask, properties);

            assert.deepStrictEqual(response, mask);
        });

        it('returns multiple mask values', function () {
            const properties = ['p1', 'name'];
            const base = { p1: false, name: 'oldName' };
            const mask = { p1: true, name: 'newName' };

            const response = getMask(base, mask, properties);

            assert.deepStrictEqual(response, mask);
        });

        it('returns only mutable values', function () {
            const properties = ['p1'];
            const base = { p1: false, name: 'oldName' };
            const mask = { p1: true, name: 'newName' };

            const response = getMask(base, mask, properties);

            assert.deepStrictEqual(response, { p1: true });
        });

        it('returns nested mask values', function () {
            const properties = ['data.name'];
            const base = { data: { name: 'oldName' } };
            const mask = { data: { name: 'newName' } };

            const response = getMask(base, mask, properties);

            assert.deepStrictEqual(response, { data: { name: 'newName' } });
        });

        it('returns only nested mutable values', function () {
            const properties = ['data.name'];
            const base = { data: { name: 'oldName', id: 0 } };
            const mask = { data: { name: 'newName', id: 1 } };

            const response = getMask(base, mask, properties);

            assert.deepStrictEqual(response, { data: { name: 'newName' } });
        });

        it('returns blank if nothing is changed', function () {
            const properties = ['p1'];
            const base = { p1: false };
            const mask = { p1: false };

            const response = getMask(base, mask, properties);

            assert.deepStrictEqual(response, {});
        });

        it('returns blank if nothing is mutable', function () {
            const properties: string[] = [];
            const base = { p1: false };
            const mask = { p1: true };

            const response = getMask(base, mask, properties);

            assert.deepStrictEqual(response, {});
        });

        it('handles non existent mutable values', function () {
            const properties = ['p2'];
            const base = { p1: false };
            const mask = { p1: true };

            const response = getMask(base, mask, properties);

            assert.deepStrictEqual(response, {});
        });

        it('errors when property is an object', function () {
            const properties = ['data'];
            const base = { data: { name: 'oldName', id: 0 } };
            const mask = { data: { name: 'newName', id: 1 } };

            assert.throws(
                () => {
                    getMask(base, mask, properties);
                },
                InvalidDataTypeError,
                'Object type not allowed as mutable properties.'
            );
        });

        it('errors when mask value is different data type', function () {
            const properties = ['p1'];
            const base = { p1: false };
            const mask = { p1: 'true' };

            assert.throws(
                () => {
                    getMask<unknown>(base, mask, properties);
                },
                InvalidDataTypeError,
                'Mask type of string does not match base type of boolean.'
            );
        });
    });

    describe('getMask forbidNulls', function () {
        const options = { forbidNulls: true };
        it('errors when the mask property value is a null', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: null };

            assert.throws(
                () => {
                    getMask<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Null values not allowed as mutable properties.'
            );
        });

        it('errors when the base property value is a null', function () {
            const properties = ['p1'];
            const base = { p1: null };
            const mask = { p1: true };

            assert.throws(
                () => {
                    getMask<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Null values not allowed as mutable properties.'
            );
        });
    });

    describe('getMask allowObjects', function () {
        const options = { allowObjects: true };
        it('returns when both property values are an object', function () {
            const properties = ['p1'];
            const base = { p1: { p11: false } };
            const mask = { p1: { p11: true } };

            const response = getMask(base, mask, properties, options);
            assert.deepStrictEqual(response, mask);
        });

        it('errors when only base property value is an object', function () {
            const properties = ['p1'];
            const base = { p1: { p11: true } };
            const mask = { p1: true };

            assert.throws(
                () => {
                    getMask<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Mask type of boolean does not match base type of object.'
            );
        });

        it('errors when only mask property value is an object', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: { p11: true } };

            assert.throws(
                () => {
                    getMask<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Mask type of object does not match base type of boolean.'
            );
        });
    });

    describe('getMask allowDifferentTypes', function () {
        const options = { allowDifferentTypes: true };
        it('returns when mask value is a different type', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: 'false' };

            const response = getMask<unknown>(base, mask, properties, options);
            assert.deepStrictEqual(response, mask);
        });

        it('errors when mask value is an object', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: { p11: true } };

            assert.throws(
                () => {
                    getMask<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Object type not allowed as mutable properties.'
            );
        });

        it('errors when base value is an object', function () {
            const properties = ['p1'];
            const base = { p1: { p11: true } };
            const mask = { p1: true };

            assert.throws(
                () => {
                    getMask<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Object type not allowed as mutable properties.'
            );
        });
    });

    describe('getMask allowDifferentTypes and allowObjects', function () {
        const options = { allowDifferentTypes: true, allowObjects: true };
        it('returns when mask value is an object', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: { p11: true } };

            const response = getMask<unknown>(base, mask, properties, options);
            assert.deepStrictEqual(response, mask);
        });

        it('returns when base value is an object', function () {
            const properties = ['p1'];
            const base = { p1: { p11: true } };
            const mask = { p1: true };

            const response = getMask<unknown>(base, mask, properties, options);
            assert.deepStrictEqual(response, mask);
        });
    });
});
