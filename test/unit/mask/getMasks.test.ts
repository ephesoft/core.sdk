import { assert } from 'chai';
import { getMasks } from '../../../src/mask/getMasks';

describe('getMasks', function () {
    it('returns overridden values with single item in the array', function () {
        const properties = ['p1', 'p2'];
        const bases = [{ id: 1, p1: false, p2: 'default' }];
        const masks = [{ id: 1, p1: true, p2: 'override' }];

        const response = getMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, masks);
    });

    it('does not return non mutable properties', function () {
        const properties = ['p1'];
        const bases = [{ id: 1, p1: false, p2: 'default' }];
        const masks = [{ id: 1, p1: true, p2: 'override' }];

        const response = getMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, [{ id: 1, p1: true }]);
    });

    it('ignores mutable properties that are not overridden', function () {
        const properties = ['p1', 'missing'];
        const bases = [
            { id: 1, p1: 'default' },
            { id: 3, p1: 'default3' }
        ];
        const masks = [
            { id: 1, p1: 'override1' },
            { id: 3, p1: 'override3' }
        ];

        const response = getMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, masks);
    });

    it('does not return default values', function () {
        const properties = ['p1'];
        const bases = [{ id: 1, p1: false, p2: 'default' }];
        const masks = [{ id: 1, p1: true, p2: 'default' }];

        const response = getMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, [{ id: 1, p1: true }]);
    });

    it('returns overridden values with multiple items in the array', function () {
        const properties = ['p1', 'p2'];
        const bases = [
            { id: 1, p1: false, p2: 'default' },
            { id: 2, p1: false, p2: 'default2' }
        ];
        const masks = [
            { id: 1, p1: true, p2: 'override' },
            { id: 2, p1: true, p2: 'override2' }
        ];

        const response = getMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, masks);
    });

    it('ignores masks that do not have a matching default object key', function () {
        const properties = ['p1'];
        const bases = [
            { id: 1, p1: 'default' },
            { id: 3, p1: 'default3' }
        ];
        const masks = [
            { id: 1, p1: 'override' },
            { id: 2, p1: 'override2' },
            { id: 3, p1: 'override3' }
        ];

        const response = getMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, [
            { id: 1, p1: 'override' },
            { id: 3, p1: 'override3' }
        ]);
    });

    it('returns an empty array when all values match the base', function () {
        const properties = ['p1'];
        const bases = [
            { id: 1, p1: 'default' },
            { id: 2, p1: 'default2' }
        ];
        const masks = [
            { id: 1, p1: 'default' },
            { id: 2, p1: 'default2' }
        ];

        const response = getMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, []);
    });

    it('returns an empty array when no matching items exist', function () {
        const properties = ['p1'];
        const bases = [
            { id: 1, p1: 'default' },
            { id: 2, p1: 'default2' }
        ];
        const masks = [
            { id: 3, p1: 'override3' },
            { id: 4, p1: 'override4' }
        ];

        const response = getMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, []);
    });

    it('returns an empty array when passed in an empty default array', function () {
        const properties = ['p1'];
        const bases: {
            id: number;
            p1: string;
        }[] = [];
        const masks = [
            { id: 3, p1: 'override3' },
            { id: 4, p1: 'override4' }
        ];

        const response = getMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, []);
    });

    it('does not return values if no key is found in mask', function () {
        const properties = ['p1'];
        const bases = [
            { id: 1, p1: 'default' },
            { id: 3, p1: 'default3' }
        ];
        const masks = [{ p1: 'override1' }, { id: 3, p1: 'override3' }];

        const response = getMasks(bases, masks, 'id', properties);
        assert.deepStrictEqual(response, [{ id: 3, p1: 'override3' }]);
    });
});
