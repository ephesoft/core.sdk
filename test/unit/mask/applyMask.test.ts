import { assert } from 'chai';
import { applyMask } from '../../../src/mask/applyMask';
import { InvalidDataTypeError } from '../../../src/errors/InvalidDataTypeError';

describe('applyMask', function () {
    describe('applyMask no options', function () {
        it('returns single overridden value from mask', function () {
            const properties = ['p1'];
            const base = { p1: false };
            const mask = { p1: true };

            const response = applyMask(base, mask, properties);

            assert.deepStrictEqual(response, mask);
        });

        it('returns multiple overridden values from mask', function () {
            const properties = ['p1', 'p2'];
            const base = { p1: false, p2: 'old' };
            const mask = { p1: true, p2: 'new' };

            const response = applyMask(base, mask, properties);

            assert.deepStrictEqual(response, mask);
        });

        it('returns multiple overridden values from mask and base values', function () {
            const properties = ['p1', 'p2'];
            const base = { p1: false, p2: 'old', b1: true, b2: 'default' };
            const mask = { p1: true, p2: 'new' };
            const expectedResponse = base;
            expectedResponse.p1 = true;
            expectedResponse.p2 = 'new';

            const response = applyMask(base, mask, properties);
            assert.deepStrictEqual(response, expectedResponse);
        });

        it('supports base null values', function () {
            const properties = ['p1'];
            const base = { p1: null };
            const mask = { p1: false };

            const response = applyMask<unknown>(base, mask, properties);
            assert.deepStrictEqual(response, mask);
        });

        it('returns empty object when supplied empty base object', function () {
            const properties = ['p1', 'p2'];
            const base = {};
            const mask = { p1: true, p2: 'new' };

            const response = applyMask(base, mask, properties);
            assert.deepStrictEqual(response, {});
        });

        it('errors when mask value is a different type', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: 'false' };

            assert.throws(
                () => {
                    applyMask<unknown>(base, mask, properties);
                },
                InvalidDataTypeError,
                'Mask type of string does not match base type of boolean.'
            );
        });

        it('errors when mask property value is an object', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: { p11: true } };

            assert.throws(
                () => {
                    applyMask<unknown>(base, mask, properties);
                },
                InvalidDataTypeError,
                'Object type not allowed as mutable properties.'
            );
        });

        it('errors when base property value is an object', function () {
            const properties = ['p1'];
            const base = { p1: { p11: true } };
            const mask = { p1: true };

            assert.throws(
                () => {
                    applyMask<unknown>(base, mask, properties);
                },
                InvalidDataTypeError,
                'Object type not allowed as mutable properties.'
            );
        });
    });

    describe('applyMask forbidNulls', function () {
        const options = { forbidNulls: true };
        it('errors when the mask property value is a null', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: null };

            assert.throws(
                () => {
                    applyMask<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Null values not allowed as mutable properties.'
            );
        });

        it('errors when the base property value is a null', function () {
            const properties = ['p1'];
            const base = { p1: null };
            const mask = { p1: true };

            assert.throws(
                () => {
                    applyMask<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Null values not allowed as mutable properties.'
            );
        });
    });

    describe('applyMask allowObjects', function () {
        const options = { allowObjects: true };
        it('returns when both property values are an object', function () {
            const properties = ['p1'];
            const base = { p1: { p11: false } };
            const mask = { p1: { p11: true } };

            const response = applyMask(base, mask, properties, options);
            assert.deepStrictEqual(response, mask);
        });

        it('errors when only base property value is an object', function () {
            const properties = ['p1'];
            const base = { p1: { p11: true } };
            const mask = { p1: true };

            assert.throws(
                () => {
                    applyMask<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Mask type of boolean does not match base type of object.'
            );
        });

        it('errors when only mask property value is an object', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: { p11: true } };

            assert.throws(
                () => {
                    applyMask<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Mask type of object does not match base type of boolean.'
            );
        });
    });

    describe('applyMask allowDifferentTypes', function () {
        const options = { allowDifferentTypes: true };
        it('returns when mask value is a different type', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: 'false' };

            const response = applyMask<unknown>(base, mask, properties, options);
            assert.deepStrictEqual(response, mask);
        });

        it('errors when mask value is an object', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: { p11: true } };

            assert.throws(
                () => {
                    applyMask<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Object type not allowed as mutable properties.'
            );
        });

        it('errors when base value is an object', function () {
            const properties = ['p1'];
            const base = { p1: { p11: true } };
            const mask = { p1: true };

            assert.throws(
                () => {
                    applyMask<unknown>(base, mask, properties, options);
                },
                InvalidDataTypeError,
                'Object type not allowed as mutable properties.'
            );
        });
    });

    describe('applyMask allowDifferentTypes and allowObjects', function () {
        const options = { allowDifferentTypes: true, allowObjects: true };
        it('returns when mask value is an object', function () {
            const properties = ['p1'];
            const base = { p1: true };
            const mask = { p1: { p11: true } };

            const response = applyMask<unknown>(base, mask, properties, options);
            assert.deepStrictEqual(response, mask);
        });

        it('returns when base value is an object', function () {
            const properties = ['p1'];
            const base = { p1: { p11: true } };
            const mask = { p1: true };

            const response = applyMask<unknown>(base, mask, properties, options);
            assert.deepStrictEqual(response, mask);
        });
    });
});
