import { assert, expect } from 'chai';
import { shuffleArray } from '../../../src/collections/shuffleArray';
import { InvalidParameterError } from '../../../src/errors/InvalidParameterError';

describe('shuffleArray unit tests', (): void => {
    it('should return shuffled array with default', (): void => {
        const inputArray = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        const result = shuffleArray(inputArray);
        expect(result).to.not.deep.equal(inputArray);
        expect(result).to.have.length(inputArray.length);
        expect(result).to.have.members(inputArray);
    });

    it('should return shuffled array with specified number of passes', (): void => {
        const inputArray = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        const result = shuffleArray(inputArray, 3);
        expect(result).to.not.deep.equal(inputArray);
        expect(result).to.have.length(inputArray.length);
        expect(result).to.have.members(inputArray);
    });

    it('should return shallow clone of array if only one item in array', (): void => {
        const inputArray = [1];
        const result = shuffleArray(inputArray);
        expect(result).to.deep.equal(inputArray);
        // Make sure array was cloned
        inputArray.push(6);
        expect(result).to.not.deep.equal(inputArray);
    });

    it('should return empty array if empty array passed in', (): void => {
        const inputArray: string[] = [];
        const result = shuffleArray(inputArray);
        expect(result).to.deep.equal(inputArray);
        // Make sure array was cloned
        inputArray.push('test');
        expect(result).to.not.deep.equal(inputArray);
    });

    it('should throw error if specified number of passes is less than one', (): void => {
        try {
            shuffleArray([], 0);
            assert.fail('Expected error was not thrown');
        } catch (error) {
            if (!(error instanceof InvalidParameterError)) {
                throw error;
            }
            expect(error.message).to.equal('Invalid numberOfPasses parameter (0) - must be greater than zero');
        }
    });

    it('should throw error if array parameter is undefined', (): void => {
        try {
            shuffleArray(undefined as unknown as string[], 0);
            assert.fail('Expected error was not thrown');
        } catch (error) {
            if (!(error instanceof InvalidParameterError)) {
                throw error;
            }
            expect(error.message).to.equal('Invalid array parameter (undefined) - must be an array');
        }
    });

    it('should throw error if array parameter is not an array', (): void => {
        try {
            shuffleArray({} as unknown as string[], 0);
            assert.fail('Expected error was not thrown');
        } catch (error) {
            if (!(error instanceof InvalidParameterError)) {
                throw error;
            }
            expect(error.message).to.equal('Invalid array parameter ([object Object]) - must be an array');
        }
    });
});
