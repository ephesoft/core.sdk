import { Semaphore } from '../../../src/concurrency/Semaphore';
import { executeAndExpectError, executeAsyncAndExpectError } from '@ephesoft/test.assertions';
import { TimeSpan } from '../../../src/conversion/TimeSpan';
import { expect } from 'chai';
import { sleep } from '../../../src/concurrency/sleep';
import { InvalidOperationError } from '../../../src/errors/InvalidOperationError';
import { Mutable } from '../../../src/types/Mutable';
import { TimeoutError } from '../../../src/errors/TimeoutError';
import { SemaphoreAbortedError } from '../../../src/concurrency/SemaphoreAbortedError';

describe('Semaphore unit tests', function () {
    describe('maximumOperations', function () {
        it('should return correct value', async function () {
            const semaphore = new Semaphore({ maximumConcurrentAccess: 5 });
            expect(semaphore.maximumOperations).to.equal(5);

            await semaphore.wait(TimeSpan.fromSeconds(1));
            // maximumOperations should be unaffected
            expect(semaphore.maximumOperations).to.equal(5);

            semaphore.release();
            // maximumOperations should be unaffected
            expect(semaphore.maximumOperations).to.equal(5);
        });
    });

    describe('currentOperations', function () {
        it('should return correct value', async function () {
            const semaphore = new Semaphore({ maximumConcurrentAccess: 5 });
            expect(semaphore.currentOperations).to.equal(0);

            await semaphore.wait(TimeSpan.fromSeconds(1));
            expect(semaphore.currentOperations).to.equal(1);

            await semaphore.wait(TimeSpan.fromSeconds(1));
            expect(semaphore.currentOperations).to.equal(2);

            await semaphore.wait(TimeSpan.fromSeconds(1));
            expect(semaphore.currentOperations).to.equal(3);

            await semaphore.wait(TimeSpan.fromSeconds(1));
            expect(semaphore.currentOperations).to.equal(4);

            await semaphore.wait(TimeSpan.fromSeconds(1));
            expect(semaphore.currentOperations).to.equal(5);

            semaphore.release();
            expect(semaphore.currentOperations).to.equal(4);

            semaphore.release();
            expect(semaphore.currentOperations).to.equal(3);

            semaphore.release();
            expect(semaphore.currentOperations).to.equal(2);

            semaphore.release();
            expect(semaphore.currentOperations).to.equal(1);

            semaphore.release();
            expect(semaphore.currentOperations).to.equal(0);
        });
    });

    describe('wait', function () {
        it('should return immediately if semaphore is available', async function () {
            const semaphore = new Semaphore({ maximumConcurrentAccess: 1 });
            const startTime = Date.now();
            await semaphore.wait(TimeSpan.fromSeconds(30));
            const elapsedTime = TimeSpan.fromMilliseconds(Date.now() - startTime);
            // NOTE: Time-based tests are frail if the CPU is under load, but I'm not sure
            //       how else to test this...
            expect(elapsedTime.totalSeconds).to.be.lessThan(1);
        });

        it('should wait until semaphore is available', async function () {
            const semaphore = new Semaphore({ maximumConcurrentAccess: 1 });

            // Fill the semaphore
            const operation1 = await startOperation({
                name: 'operation1',
                semaphore: semaphore,
                runUntilSignaled: true,
                wait: WaitType.SemaphoreEntered
            });

            // This operation should wait on the semaphore
            const operation2 = await startOperation({
                name: 'operation2',
                semaphore: semaphore,
                runUntilSignaled: false,
                wait: WaitType.Started
            });

            expect(operation1.enteredSemaphore, 'Operation 1 should be accessing the semaphore').to.be.true;
            expect(operation2.enteredSemaphore, 'Operation 2 should NOT be accessing the semaphore').to.be.false;

            // stop the first operation
            operation1.continueRunning = false;
            await operation1.promise;

            await operation2.promise;

            expect(operation2.enteredSemaphore, 'Operation2 should have received access to the semaphore').to.be.true;
            expect(operation2.exitedSemaphore, 'Operation 2 should have exited').to.be.true;
        });

        it('should time out if semaphore does not become available within the timeout period', async function () {
            const semaphore = new Semaphore({ maximumConcurrentAccess: 1 });

            // Fill the semaphore
            const operation1 = await startOperation({
                name: 'operation1',
                semaphore: semaphore,
                runUntilSignaled: true,
                wait: WaitType.SemaphoreEntered
            });

            try {
                await executeAsyncAndExpectError(
                    async () => {
                        await semaphore.wait(TimeSpan.fromMilliseconds(10));
                    },
                    TimeoutError,
                    'Timed out waiting for Semaphore'
                );
            } finally {
                operation1.continueRunning = false;
                await operation1.promise;
            }
        });

        it('should throw error if abort called', async function () {
            const semaphore = new Semaphore({ maximumConcurrentAccess: 1 });

            // Fill the semaphore
            const operation1 = await startOperation({
                name: 'operation1',
                semaphore: semaphore,
                runUntilSignaled: true,
                wait: WaitType.SemaphoreEntered
            });

            // This operation should wait on the semaphore
            const operation2 = await startOperation({
                name: 'operation2',
                semaphore: semaphore,
                runUntilSignaled: false,
                wait: WaitType.Started
            });

            expect(operation1.enteredSemaphore, 'Operation 1 should be accessing the semaphore').to.be.true;
            expect(operation2.enteredSemaphore, 'Operation 2 should NOT be accessing the semaphore').to.be.false;

            // abort the semaphore
            semaphore.abort();

            // stop the first operation
            operation1.continueRunning = false;
            await operation1.promise;

            await executeAsyncAndExpectError(
                async () => {
                    await operation2.promise;
                },
                SemaphoreAbortedError,
                'Semaphore was aborted'
            );
        });

        it('should not allow more than the specified number of operations simultaneously', async function () {
            const iterations = 500;
            const maximumConcurrency = 5;
            const semaphore = new Semaphore({ maximumConcurrentAccess: maximumConcurrency });

            let workersInSemaphore = 0;
            let maximumWorkersInSemaphore = 0;
            async function worker(): Promise<void> {
                await semaphore.wait();
                workersInSemaphore++;
                if (workersInSemaphore > maximumWorkersInSemaphore) {
                    maximumWorkersInSemaphore = workersInSemaphore;
                }
                await sleep(Math.ceil(Math.random() * 50));
                workersInSemaphore--;
                semaphore.release();
            }

            const promises: Promise<void>[] = [];
            for (let i = 0; i < iterations; i++) {
                promises.push(worker());
            }

            // This is just to make tsc happy
            expect(promises.length).to.equal(iterations);

            await semaphore.flush();

            expect(maximumWorkersInSemaphore).to.equal(maximumConcurrency);
        });

        it('should throw error if semaphore is aborted before wait call', async function () {
            const semaphore = new Semaphore({ maximumConcurrentAccess: 1 });
            semaphore.abort();
            await executeAsyncAndExpectError(async () => {
                await semaphore.wait();
            }, SemaphoreAbortedError);
        });

        it('should throw error if semaphore is aborted while promise is still waiting', async function () {
            const semaphore = new Semaphore({ maximumConcurrentAccess: 1 });
            await semaphore.wait(TimeSpan.fromSeconds(30));
            const waitPromise = semaphore.wait(TimeSpan.fromSeconds(30));
            semaphore.abort();
            await executeAsyncAndExpectError(async () => {
                await waitPromise;
            }, SemaphoreAbortedError);
        });
    });

    describe('release', function () {
        it('should throw error if no operations have entered the Semaphore', function () {
            const semaphore = new Semaphore({ maximumConcurrentAccess: 1 });
            executeAndExpectError(
                () => {
                    semaphore.release();
                },
                InvalidOperationError,
                'Semaphore does not have any wait handles to release'
            );
        });

        it('should throw error if all operations have exited the Semaphore', async function () {
            const semaphore = new Semaphore({ maximumConcurrentAccess: 1 });

            await semaphore.wait(TimeSpan.fromSeconds(1));
            semaphore.release();

            executeAndExpectError(
                () => {
                    semaphore.release();
                },
                InvalidOperationError,
                'Semaphore does not have any wait handles to release'
            );
        });
    });

    describe('flush', function () {
        it('should return immediately if no operations have been started', async function () {
            const semaphore = new Semaphore({ maximumConcurrentAccess: 1 });
            const startTime = Date.now();
            await semaphore.flush(TimeSpan.fromSeconds(1));
            const elapsedTime = TimeSpan.fromMilliseconds(Date.now() - startTime);
            // NOTE: Time-based tests are frail if the CPU is under load, but I'm not sure
            //       how else to test this...
            expect(elapsedTime.totalSeconds).to.be.lessThan(1);
        });

        it('should wait until all operations have completed', async function () {
            const semaphore = new Semaphore({ maximumConcurrentAccess: 2 });
            async function flush(): Promise<number> {
                await semaphore.flush();
                return Date.now();
            }

            // simulate multiple operations entering the semaphore
            await semaphore.wait(TimeSpan.fromSeconds(30));
            await semaphore.wait(TimeSpan.fromSeconds(30));

            const flushPromise = flush();

            semaphore.release();
            const releaseTime = Date.now();
            semaphore.release();

            const flushTime = await flushPromise;

            expect(flushTime).to.be.greaterThan(releaseTime);
        });

        it('should return immediately if semaphore already flushed', async function () {
            const semaphore = new Semaphore({ maximumConcurrentAccess: 2 });

            // simulate multiple operations entering the semaphore
            await semaphore.wait(TimeSpan.fromSeconds(30));
            await semaphore.wait(TimeSpan.fromSeconds(30));

            const flushPromise = semaphore.flush(TimeSpan.fromSeconds(30));

            semaphore.release();
            semaphore.release();

            await flushPromise;

            const startTime = Date.now();
            await semaphore.flush(TimeSpan.fromSeconds(1));
            const elapsedTime = TimeSpan.fromMilliseconds(Date.now() - startTime);
            // NOTE: Time-based tests are frail if the CPU is under load, but I'm not sure
            //       how else to test this...
            expect(elapsedTime.totalSeconds).to.be.lessThan(1);
        });

        it('should throw error if all operations have not exited within the timeout', async function () {
            const semaphore = new Semaphore({ maximumConcurrentAccess: 1 });
            await semaphore.wait(TimeSpan.fromSeconds(30));
            await executeAsyncAndExpectError(async () => {
                await semaphore.flush(TimeSpan.fromSeconds(1));
            }, TimeoutError);
        });
    });
});

class OperationState {
    public readonly name!: string;
    public continueRunning!: boolean;
    public readonly started!: boolean;
    public readonly enteredSemaphore!: boolean;
    public readonly exitedSemaphore!: boolean;
    public readonly promise!: Promise<unknown>;

    public toJSON(): object {
        return {
            name: this.name,
            continueRunning: this.continueRunning,
            started: this.started,
            enteredSemaphore: this.enteredSemaphore,
            exitedSemaphore: this.exitedSemaphore
        };
    }
}

enum WaitType {
    None = 0,
    Started = 1,
    SemaphoreEntered = 2,
    SemaphoreExited = 3
}

interface OperationConfiguration {
    semaphore: Semaphore;
    name: string;
    wait?: WaitType;
    runUntilSignaled?: boolean;
}

async function startOperation(configuration: OperationConfiguration): Promise<OperationState> {
    const state = new OperationState() as Mutable<OperationState>;
    state.name = configuration.name;
    state.continueRunning = Boolean(configuration.runUntilSignaled);
    state.started = false;
    state.enteredSemaphore = false;
    state.exitedSemaphore = false;

    async function asyncOperation(): Promise<void> {
        state.started = true;
        await configuration.semaphore.wait(TimeSpan.fromSeconds(30));
        try {
            state.enteredSemaphore = true;
            while (state.continueRunning) {
                await sleep(10);
            }
        } finally {
            configuration.semaphore.release();
            state.exitedSemaphore = true;
        }
    }

    state.promise = asyncOperation();

    if (!configuration.wait) {
        return state;
    }

    if (configuration.wait) {
        const date = Date.now();
        const timeoutDateTime = date + TimeSpan.fromMinutes(1).totalMilliseconds;
        while (Date.now() < timeoutDateTime) {
            if (
                (configuration.wait === WaitType.Started && state.started) ||
                (configuration.wait === WaitType.SemaphoreEntered && state.enteredSemaphore) ||
                (configuration.wait === WaitType.SemaphoreExited && state.exitedSemaphore)
            ) {
                return state;
            }
            await sleep(1);
        }
        expect.fail(
            `Timed out waiting for ${configuration.name} operation to enter the desired state\n${JSON.stringify(state, null, 2)}`
        );
    }
    return state;
}
