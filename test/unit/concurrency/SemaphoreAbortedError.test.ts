import { expect } from 'chai';
import { SemaphoreAbortedError } from '../../../src/concurrency/SemaphoreAbortedError';

describe('SemaphoreAbortedError unit tests', function () {
    describe('constructor', function () {
        it('should correctly populate error object if only message supplied', function () {
            const message = 'test message';
            const error = new SemaphoreAbortedError(message);
            expect(error.message).to.equal(message);
            expect(error.innerError).to.equal(null);
            expect(error.name).to.equal('SemaphoreAbortedError');
        });

        it('should correctly populate error object if message and inner error supplied', function () {
            const message = 'test message';
            const innerError = new Error('inner error');
            const error = new SemaphoreAbortedError(message, innerError);
            expect(error.message).to.equal(message);
            expect(error.innerError).to.equal(innerError);
            expect(error.name).to.equal('SemaphoreAbortedError');
        });
    });
});
