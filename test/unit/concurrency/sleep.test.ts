import { sleep } from '../../../src/concurrency/sleep';
import { assert } from 'chai';

describe('sleep(number): Promise<void>', (): void => {
    it('should sleep the specified number of milliseconds', async (): Promise<void> => {
        const start = Date.now();
        await sleep(100);
        const elapsed = Date.now() - start;
        console.log(`Requested Delay: 100 milliseconds; Elapsed milliseconds: ${elapsed}`);
        // Give ourselves a little leeway as thread scheduling isn't exact
        if (elapsed < 95) {
            assert.fail(`Expected to sleep for 100 milliseconds, but only slept ${elapsed}`);
        }
        // The upper bound is largely dependent on CPU load - we won't attempt to validate it
    });
});
