import { expect } from 'chai';
import { regexEscape } from '../../../src/regex/regexEscape';

describe('regexEscape unit tests', function () {
    it('should not modify empty string', function () {
        const text = '';
        expect(regexEscape(text)).to.equal(text);
    });

    it('should not modify string without control characters', function () {
        const text = 'a~B!c@ 1% 2 3#';
        expect(regexEscape(text)).to.equal(text);
    });

    it('should escape \\ character', function () {
        expect(regexEscape('\\')).to.equal('\\\\');

        const text = '\\rando\\m text\\';
        expect(regexEscape(text)).to.equal('\\\\rando\\\\m text\\\\');
    });

    it('should escape \\ character', function () {
        verifyCharacterEscaped('\\');
    });

    it('should escape ^ character', function () {
        verifyCharacterEscaped('^');
    });

    it('should escape $ character', function () {
        verifyCharacterEscaped('$');
    });

    it('should escape | character', function () {
        verifyCharacterEscaped('|');
    });

    it('should escape . character', function () {
        verifyCharacterEscaped('.');
    });

    it('should escape * character', function () {
        verifyCharacterEscaped('*');
    });

    it('should escape + character', function () {
        verifyCharacterEscaped('+');
    });

    it('should escape ? character', function () {
        verifyCharacterEscaped('?');
    });

    it('should escape ( character', function () {
        verifyCharacterEscaped('(');
    });

    it('should escape ) character', function () {
        verifyCharacterEscaped(')');
    });

    it('should escape [ character', function () {
        verifyCharacterEscaped('[');
    });

    it('should escape ] character', function () {
        verifyCharacterEscaped(']');
    });

    it('should escape { character', function () {
        verifyCharacterEscaped('{');
    });

    it('should escape ) character', function () {
        verifyCharacterEscaped('}');
    });

    it('should escape mixed characters', function () {
        expect(regexEscape('~\\[abc)xy\\\\z*&')).to.equal('~\\\\\\[abc\\)xy\\\\\\\\z\\*&');
    });

    function verifyCharacterEscaped(character: string): void {
        expect(regexEscape(character)).to.equal(`\\${character}`);

        expect(regexEscape(`${character}some ran${character}dom text ~!@#%&"_=:;<>/${character}`)).to.equal(
            `\\${character}some ran\\${character}dom text ~!@#%&"_=:;<>/\\${character}`
        );
    }
});
