import { assert } from 'chai';
import { NestedError } from '../../../src/errors/NestedError';

describe('NestedError unit tests', (): void => {
    describe('constructor(string, Error?)', (): void => {
        it('should correctly populate error object if only message supplied', (): void => {
            const error = new NestedError('test message');
            assert.strictEqual(error.message, 'test message');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'NestedError');
        });

        it('should correctly populate error object if message and null inner error supplied', (): void => {
            const error = new NestedError('test message', null);
            assert.strictEqual(error.message, 'test message');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'NestedError');
        });

        it('should correctly populate error object if message and inner error supplied', (): void => {
            const innerError = new Error('inner error');
            const error = new NestedError('outer error', innerError);
            assert.strictEqual(error.message, 'outer error');
            assert.deepStrictEqual(error.innerError, innerError);
            assert.strictEqual(error.name, 'NestedError');
        });

        it('should correctly populate error if subclassed', (): void => {
            const innerError = new Error('inner error');
            const error = new TestError('outer error', innerError);
            assert.strictEqual(error.message, 'outer error');
            assert.deepStrictEqual(error.innerError, innerError);
            assert.strictEqual(error.name, 'TestError');
        });
    });

    describe('toString()', (): void => {
        it('should return correct string if no inner error', (): void => {
            // use try/catch to simulate real-life scenario
            try {
                throw new NestedError('test message');
            } catch (e) {
                assert.strictEqual(String(e), 'NestedError: test message');
            }
        });

        it('should return correct string if inner error is present', (): void => {
            // use try/catch to simulate real-life scenario
            try {
                try {
                    try {
                        throw new NestedError('bottom');
                    } catch (e) {
                        throw new NestedError('middle', e as Error);
                    }
                } catch (e) {
                    throw new NestedError('top', e as Error);
                }
            } catch (e) {
                // only the top-level error should be printed
                assert.strictEqual(String(e), 'NestedError: top');
            }
        });
    });
});

class TestError extends NestedError {
    public constructor(message: string, innerError: Error) {
        super(message, innerError);
    }
}
