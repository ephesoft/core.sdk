import { assert } from 'chai';
import { InvalidOperationError } from '../../../src/errors/InvalidOperationError';

describe('InvalidOperationError unit tests', (): void => {
    describe('constructor(string, Error?)', (): void => {
        it('should correctly populate error object if only message supplied', (): void => {
            const error = new InvalidOperationError('test message');
            assert.strictEqual(error.message, 'test message');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'InvalidOperationError');
        });

        it('should correctly populate error object if message and inner error supplied', (): void => {
            const innerError = new Error('inner error');
            const error = new InvalidOperationError('outer error', innerError);
            assert.strictEqual(error.message, 'outer error');
            assert.deepStrictEqual(error.innerError, innerError);
            assert.strictEqual(error.name, 'InvalidOperationError');
        });
    });

    describe('toString(): string', (): void => {
        it('should return correct text', (): void => {
            const error = new InvalidOperationError('test message');
            assert.strictEqual(error.toString(), 'InvalidOperationError: test message');
        });
    });
});
