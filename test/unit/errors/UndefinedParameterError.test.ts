import { assert } from 'chai';
import { UndefinedParameterError } from '../../../src/errors/UndefinedParameterError';

describe('UndefinedParameterError unit tests', (): void => {
    describe('constructor(string, Error?)', (): void => {
        it('should correctly populate error object if only parameter name supplied', (): void => {
            const error = new UndefinedParameterError('testParameter');
            assert.strictEqual(error.message, 'Parameter was null or undefined');
            assert.strictEqual(error.name, 'UndefinedParameterError');
            assert.strictEqual(error.parameterName, 'testParameter');
            assert.strictEqual(error.innerError, null);
        });

        it('should correctly populate error object if parameter name and message supplied', (): void => {
            const error = new UndefinedParameterError('testParameter', 'test message');
            assert.strictEqual(error.message, 'test message');
            assert.strictEqual(error.name, 'UndefinedParameterError');
            assert.strictEqual(error.parameterName, 'testParameter');
            assert.strictEqual(error.innerError, null);
        });

        it('should correctly populate error object if message and inner error supplied', (): void => {
            const innerError = new Error('inner error');
            const error = new UndefinedParameterError('testParameter', 'test message', innerError);
            assert.strictEqual(error.message, 'test message');
            assert.strictEqual(error.name, 'UndefinedParameterError');
            assert.strictEqual(error.parameterName, 'testParameter');
            assert.deepStrictEqual(error.innerError, innerError);
        });
    });

    describe('toString(): string', (): void => {
        it('should return correct text if message specified', (): void => {
            const error = new UndefinedParameterError('testParameter', 'test message');
            assert.strictEqual(error.toString(), 'UndefinedParameterError: test message (parameter: testParameter)');
        });

        it('should return correct text if message not specified', (): void => {
            const error = new UndefinedParameterError('testParameter');
            assert.strictEqual(
                error.toString(),
                'UndefinedParameterError: Parameter was null or undefined (parameter: testParameter)'
            );
        });
    });
});
