import { assert } from 'chai';
import { AggregateError } from '../../../src/errors/AggregateError';

describe('AggregateError unit tests', (): void => {
    describe('constructor(string, Error[])', (): void => {
        it('should correctly populate error object', (): void => {
            const innerErrors = [new Error('error1'), new Error('error2')];
            const error = new AggregateError(innerErrors, 'outer error');
            assert.strictEqual(error.message, 'outer error');
            assert.deepStrictEqual(error.errors, innerErrors);
            assert.strictEqual(error.name, 'AggregateError');
        });

        it('should correctly populate error object if innerErrors argument omitted', (): void => {
            // @ts-expect-error - Force in undefined value
            const error = new AggregateError(undefined, 'outer error');
            assert.strictEqual(error.message, 'outer error');
            assert.deepStrictEqual(error.errors, []);
            assert.strictEqual(error.name, 'AggregateError');
        });
    });

    describe('toString(): string', (): void => {
        it('should return correct text', (): void => {
            const error = new AggregateError([new Error('error1'), new Error('error2')], 'outer error');
            assert.strictEqual(error.toString(), 'AggregateError: outer error');
        });
    });
});
