import { assert } from 'chai';
import { InvalidDataTypeError } from '../../../src/errors/InvalidDataTypeError';

describe('InvalidDataTypeError unit tests', (): void => {
    describe('constructor()', (): void => {
        it('should correctly populate error object if no parameters are supplied', (): void => {
            const error = new InvalidDataTypeError();
            assert.strictEqual(error.message, 'Invalid Data Type');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'InvalidDataTypeError');
        });
    });

    describe('constructor(string, Error?)', (): void => {
        it('should correctly populate error object if only message supplied', (): void => {
            const error = new InvalidDataTypeError('test message');
            assert.strictEqual(error.message, 'test message');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'InvalidDataTypeError');
        });

        it('should correctly populate error object if message and inner error supplied', (): void => {
            const innerError = new Error('inner error');
            const error = new InvalidDataTypeError('outer error', innerError);
            assert.strictEqual(error.message, 'outer error');
            assert.deepStrictEqual(error.innerError, innerError);
            assert.strictEqual(error.name, 'InvalidDataTypeError');
        });
    });

    describe('toString(): string', (): void => {
        it('should return correct text', (): void => {
            const error = new InvalidDataTypeError('test message');
            assert.strictEqual(error.toString(), 'InvalidDataTypeError: test message');
        });
    });
});
