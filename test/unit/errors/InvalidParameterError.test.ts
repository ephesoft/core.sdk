import { assert } from 'chai';
import { InvalidParameterError } from '../../../src/errors/InvalidParameterError';

describe('InvalidParameterError unit tests', (): void => {
    describe('constructor(string, string?, Error?)', (): void => {
        it('should correctly populate error object if only parameter name supplied', (): void => {
            const error = new InvalidParameterError('testParameter');
            assert.strictEqual(error.message, 'Invalid parameter');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'InvalidParameterError');
            assert.strictEqual(error.parameterName, 'testParameter');
        });

        it('should correctly populate error object if parameter name and message supplied', (): void => {
            const error = new InvalidParameterError('testParameter', 'test message');
            assert.strictEqual(error.message, 'test message');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'InvalidParameterError');
            assert.strictEqual(error.parameterName, 'testParameter');
        });

        it('should correctly populate error object if parameter name, message, and inner error supplied', (): void => {
            const innerError = new Error('inner error');
            const error = new InvalidParameterError('testParameter', 'outer error', innerError);
            assert.strictEqual(error.message, 'outer error');
            assert.strictEqual(error.name, 'InvalidParameterError');
            assert.strictEqual(error.parameterName, 'testParameter');
            assert.deepStrictEqual(error.innerError, innerError);
        });
    });

    describe('getLogData(): unknown', (): void => {
        it('should output the parameter name', (): void => {
            const parameterName = 'testParameter';
            const error = new InvalidParameterError(parameterName, 'test message');
            const logData = error.getLogData();
            assert.deepStrictEqual(logData, { parameterName });
        });
    });

    describe('toString()', (): void => {
        it('should return correct value if only parameter name is specified', (): void => {
            // simulate real-life usage
            try {
                throw new InvalidParameterError('testParameter');
            } catch (e) {
                assert.strictEqual(String(e), 'InvalidParameterError: Invalid parameter (parameter: testParameter)');
            }
        });

        it('should return correct value if parameter name and message are specified', (): void => {
            // simulate real-life usage
            try {
                throw new InvalidParameterError('testParameter', 'test message');
            } catch (e) {
                assert.strictEqual(String(e), 'InvalidParameterError: test message (parameter: testParameter)');
            }
        });

        it('should return correct value object if parameter name, message, and inner error are specified', (): void => {
            // simulate real-life usage
            try {
                try {
                    throw new Error('inner');
                } catch (e1) {
                    throw new InvalidParameterError('testParameter', 'outer', e1 as Error);
                }
            } catch (e) {
                assert.strictEqual(String(e), 'InvalidParameterError: outer (parameter: testParameter)');
            }
        });
    });
});
