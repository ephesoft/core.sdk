import { expect } from 'chai';
import { TimeoutError } from '../../../src/errors/TimeoutError';

describe('TimeoutError unit tests', (): void => {
    describe('constructor', (): void => {
        it('should correctly populate error object if only message supplied', (): void => {
            const error = new TimeoutError('test message');
            expect(error.message).to.equal('test message');
            expect(error.innerError).to.equal(null);
            expect(error.name).to.equal('TimeoutError');
        });

        it('should correctly populate error object if message and inner error supplied', (): void => {
            const innerError = new Error('inner error');
            const error = new TimeoutError('outer error', innerError);
            expect(error.message).to.equal('outer error');
            expect(error.innerError).to.equal(innerError);
            expect(error.name).to.equal('TimeoutError');
        });
    });

    describe('toString', (): void => {
        it('should return correct text', (): void => {
            const error = new TimeoutError('test message');
            expect(error.toString()).to.equal('TimeoutError: test message');
        });
    });
});
