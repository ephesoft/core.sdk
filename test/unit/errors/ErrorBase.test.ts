import { assert } from 'chai';
import { ErrorBase } from '../../../src/errors/ErrorBase';
import { NestedError } from '../../../src/errors/NestedError';
import { AggregateError } from '../../../src/errors/AggregateError';

describe('ErrorBase unit tests', (): void => {
    describe('toJSON(): unknown', (): void => {
        it('should produce JSON with minimal data', (): void => {
            const error = new NestedError('Test Message');
            const json = error.toJSON() as ErrorJson;
            assert.strictEqual(json.name, error.name);
            assert.strictEqual(json.message, error.message);
            validateStack(json.stack, error.stack);
            assert.notOk('innerError' in json, 'JSON should not have included an innerError property');
            assert.notOk('errors' in json, 'JSON should not have included an errors property');
            assert.notOk('data' in json, 'JSON should not have included a data property');
        });

        it('should work with NestedError', (): void => {
            const innerError = new NestedError('Inner Error');
            const error = new NestedError('Test Message', innerError);
            const json = error.toJSON() as ErrorJson;
            assert.strictEqual(json.name, error.name);
            assert.strictEqual(json.message, error.message);
            validateStack(json.stack, error.stack);
            assert.strictEqual(json.innerError, innerError);
            assert.notOk('errors' in json, 'JSON should not have included an errors property');
            assert.notOk('data' in json, 'JSON should not have included a data property');
        });

        it('should work with AggregateError', (): void => {
            const error1 = new NestedError('Error1');
            const error2 = new NestedError('Error2');
            const error = new AggregateError([error1, error2], 'Test Message');
            const json = error.toJSON() as ErrorJson;
            assert.strictEqual(json.name, error.name);
            assert.strictEqual(json.message, error.message);
            validateStack(json.stack, error.stack);
            assert.notOk('innerError' in json, 'JSON should not have included an innerError property');
            assert.strictEqual(json.errors?.length, 2);
            assert.strictEqual(json.errors?.[0], error1);
            assert.strictEqual(json.errors?.[1], error2);
            assert.notOk('data' in json, 'JSON should not have included a data property');
        });

        it('should not error if stack is undefined', (): void => {
            const error = new NestedError('Test Message');
            error.stack = undefined;
            const json = error.toJSON() as ErrorJson;
            assert.deepStrictEqual(json.stack, []);
        });

        it('should not error if stack is not string', (): void => {
            const error = new NestedError('Test Message');
            // @ts-expect-error - Force garbage in stack property
            error.stack = {};
            const json = error.toJSON() as ErrorJson;
            assert.deepStrictEqual(json.stack, []);
        });

        it('should produce JSON with all data', (): void => {
            const error = new TestError('Test Message');
            const innerError = new NestedError('Inner Error');
            error.innerError = innerError;
            const error1 = new NestedError('Error1');
            const error2 = new NestedError('Error2');
            error.errors = [error1, error2];
            const json = error.toJSON() as ErrorJson;
            assert.strictEqual(json.name, error.name);
            assert.strictEqual(json.message, error.message);
            validateStack(json.stack, error.stack);
            assert.strictEqual(json.innerError, innerError);
            assert.strictEqual(json.errors?.length, 2);
            assert.strictEqual(json.errors?.[0], error1);
            assert.strictEqual(json.errors?.[1], error2);
            assert.deepStrictEqual(json.data, { key: 'value' });
        });

        // Manual test only for checking output - don't check this in enabled
        it.skip('should pass manual validation', (): void => {
            const error = new TestError('Test Error');
            error.innerError = new NestedError('Inner Error', new NestedError('Deeply-Nested Error'));
            error.errors = [
                new NestedError('Error1', new NestedError('Error1-Nested')),
                new AggregateError(
                    [
                        new NestedError('Error2-Nested1'),
                        new NestedError('Error2-Nested2', new NestedError('Error2-Nested2-Nested1'))
                    ],
                    'Error2'
                )
            ];

            assert.fail(`Stringified Error:\n${JSON.stringify(error, null, 2)}`);
        });

        function validateStack(stackJson: string[], stack?: string): void {
            assert.ok(stackJson.length > 0, 'No stack data was output');
            const numberOfLinesInStack = ((stack ?? '').match(/\n/g) || '').length;
            assert.strictEqual(numberOfLinesInStack, stackJson.length);
            for (const line of stackJson) {
                assert.ok(line.trim() === line, `Stack line was not trimmed: "${line}"`);
                assert.ok(line.trim().length !== 0, 'Empty stack item was present');
            }
        }

        interface ErrorJson {
            name: string;
            message: string;
            stack: string[];
            innerError?: Error;
            errors?: Error[];
            data?: unknown;
        }

        class TestError extends ErrorBase {
            public constructor(message: string) {
                super(message);
                this.name = 'TestError';
            }

            innerError?: Error;

            errors?: Error[];

            public getLogData(): unknown {
                return { key: 'value' };
            }
        }
    });
});
