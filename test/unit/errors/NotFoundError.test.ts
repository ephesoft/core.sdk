import { assert } from 'chai';
import { NotFoundError } from '../../../src/errors/NotFoundError';

describe('NotFoundError unit tests', (): void => {
    describe('constructor(string, Error?)', (): void => {
        it('should correctly populate error object if no parameters supplied', (): void => {
            const error = new NotFoundError();
            assert.strictEqual(error.message, 'Not found');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'NotFoundError');
        });

        it('should correctly populate error object if only message supplied', (): void => {
            const error = new NotFoundError('test message');
            assert.strictEqual(error.message, 'test message');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'NotFoundError');
        });

        it('should correctly populate error object if message and inner error supplied', (): void => {
            const innerError = new Error('inner error');
            const error = new NotFoundError('outer error', innerError);
            assert.strictEqual(error.message, 'outer error');
            assert.deepStrictEqual(error.innerError, innerError);
            assert.strictEqual(error.name, 'NotFoundError');
        });
    });

    describe('toString(): string', (): void => {
        it('should return correct text', (): void => {
            const error = new NotFoundError('test message');
            assert.strictEqual(error.toString(), 'NotFoundError: test message');
        });
    });
});
