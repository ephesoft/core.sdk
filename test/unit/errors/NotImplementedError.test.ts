import { assert } from 'chai';
import { NotImplementedError } from '../../../src/errors/NotImplementedError';

describe('NotImplementedError unit tests', (): void => {
    describe('constructor(string, Error?)', (): void => {
        it('should correctly populate error object if no parameters supplied', (): void => {
            const error = new NotImplementedError();
            assert.strictEqual(error.message, 'Not implemented');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'NotImplementedError');
        });

        it('should correctly populate error object if only message supplied', (): void => {
            const error = new NotImplementedError('test message');
            assert.strictEqual(error.message, 'test message');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'NotImplementedError');
        });

        it('should correctly populate error object if message and inner error supplied', (): void => {
            const innerError = new Error('inner error');
            const error = new NotImplementedError('outer error', innerError);
            assert.strictEqual(error.message, 'outer error');
            assert.deepStrictEqual(error.innerError, innerError);
            assert.strictEqual(error.name, 'NotImplementedError');
        });
    });

    describe('toString(): string', (): void => {
        it('should return correct text', (): void => {
            const error = new NotImplementedError('test message');
            assert.strictEqual(error.toString(), 'NotImplementedError: test message');
        });
    });
});
