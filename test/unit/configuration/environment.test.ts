import { assert, expect } from 'chai';
import * as Environment from '../../../src/configuration/environment';
import { TimeSpan } from '../../../src/conversion/TimeSpan';
import { MissingEnvironmentVariableError } from '../../../src/configuration/MissingEnvironmentVariableError';
import { InvalidEnvironmentVariableError } from '../../../src/configuration/InvalidEnvironmentVariableError';
import { executeAndExpectError } from '@ephesoft/test.assertions';

describe('environment unit tests', function () {
    describe('isVariableDefined', function () {
        it('should return true if environment variable is defined', function () {
            process.env.UNIT_TEST_VALUE = 'This is a test';
            assert.strictEqual(Environment.isVariableDefined('UNIT_TEST_VALUE'), true);
        });

        it('should return true if environment variable is defined as empty string', function () {
            process.env.UNIT_TEST_VALUE = '';
            assert.strictEqual(Environment.isVariableDefined('UNIT_TEST_VALUE'), true);
        });

        it('should return false if environment variable is undefined', function () {
            assert.strictEqual(Environment.isVariableDefined('DOES_NOT_EXIST'), false);
        });
    });

    describe('getVariable', function () {
        describe('No default value provided', function () {
            it('should return variable value if defined', function () {
                const environmentVariableValue = 'This is a test';
                process.env.UNIT_TEST_VALUE = environmentVariableValue;
                const value = Environment.getVariable('UNIT_TEST_VALUE');
                assert.strictEqual(value, environmentVariableValue);
            });

            it('should return variable value if defined as empty string', function () {
                const environmentVariableValue = '';
                process.env.UNIT_TEST_VALUE = environmentVariableValue;
                const value = Environment.getVariable('UNIT_TEST_VALUE');
                assert.strictEqual(value, environmentVariableValue);
            });

            it('should throw error if variable is not defined', function () {
                try {
                    Environment.getVariable('DOES_NOT_EXIST');
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof MissingEnvironmentVariableError)) {
                        throw e;
                    }
                    assert.equal(e.message, '"DOES_NOT_EXIST" environment variable is not defined');
                }
            });
        });

        describe('Default value provided', function () {
            it('should return variable value if defined', function () {
                const environmentVariableValue = 'This is a test';
                process.env.UNIT_TEST_VALUE = environmentVariableValue;
                const value = Environment.getVariable('UNIT_TEST_VALUE', 'default value');
                assert.strictEqual(value, environmentVariableValue);
            });

            it('should return variable value if defined as empty string', function () {
                const environmentVariableValue = '';
                process.env.UNIT_TEST_VALUE = environmentVariableValue;
                const value = Environment.getVariable('UNIT_TEST_VALUE', 'default value');
                assert.strictEqual(value, environmentVariableValue);
            });

            it('should return default value if variable is not defined', function () {
                const defaultValue = 'default value';
                const value = Environment.getVariable('DOES_NOT_EXIST', defaultValue);
                assert.strictEqual(value, defaultValue);
            });
        });
    });

    describe('getVariableAsBoolean', function () {
        describe('No default value provided', function () {
            it('should return variable value if defined as boolean', function () {
                process.env.UNIT_TEST_VALUE = 'true';
                const value = Environment.getVariableAsBoolean('UNIT_TEST_VALUE');
                assert.strictEqual(value, true);
            });

            it('should throw error if variable is not defined', function () {
                try {
                    Environment.getVariableAsBoolean('DOES_NOT_EXIST');
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof MissingEnvironmentVariableError)) {
                        throw e;
                    }
                    assert.equal(e.message, '"DOES_NOT_EXIST" environment variable is not defined');
                }
            });

            it('should throw error if variable value is defined as empty string', function () {
                process.env.UNIT_TEST_VALUE = '';
                try {
                    Environment.getVariableAsBoolean('UNIT_TEST_VALUE');
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof InvalidEnvironmentVariableError)) {
                        throw e;
                    }
                    assert.strictEqual(e.message, '"UNIT_TEST_VALUE" environment variable has a value that is not a boolean');
                }
            });

            it('should throw error if variable value is not numeric', function () {
                process.env.UNIT_TEST_VALUE = 'not a number';
                try {
                    Environment.getVariableAsBoolean('UNIT_TEST_VALUE');
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof InvalidEnvironmentVariableError)) {
                        throw e;
                    }
                    assert.strictEqual(e.message, '"UNIT_TEST_VALUE" environment variable has a value that is not a boolean');
                }
            });
        });

        describe('Default value provided', function () {
            it('should return variable value if defined as boolean', function () {
                process.env.UNIT_TEST_VALUE = 'true';
                const value = Environment.getVariableAsBoolean('UNIT_TEST_VALUE', false);
                assert.strictEqual(value, true);
            });

            it('should return default value if variable is not defined', function () {
                const defaultValue = true;
                const value = Environment.getVariableAsBoolean('DOES_NOT_EXIST', defaultValue);
                assert.strictEqual(value, defaultValue);
            });

            it('should throw error if variable value is defined as empty string', function () {
                process.env.UNIT_TEST_VALUE = '';
                try {
                    Environment.getVariableAsBoolean('UNIT_TEST_VALUE', true);
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof InvalidEnvironmentVariableError)) {
                        throw e;
                    }
                    assert.strictEqual(e.message, '"UNIT_TEST_VALUE" environment variable has a value that is not a boolean');
                }
            });

            it('should throw error if variable value is not "true" or "false"', function () {
                process.env.UNIT_TEST_VALUE = 'not a number';
                try {
                    Environment.getVariableAsBoolean('UNIT_TEST_VALUE', true);
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof InvalidEnvironmentVariableError)) {
                        throw e;
                    }
                    assert.strictEqual(e.message, '"UNIT_TEST_VALUE" environment variable has a value that is not a boolean');
                }
            });
        });
    });

    describe('getVariableAsNumber', function () {
        describe('No default value provided', function () {
            it('should return variable value if defined as number', function () {
                process.env.UNIT_TEST_VALUE = '-42.24';
                const value = Environment.getVariableAsNumber('UNIT_TEST_VALUE');
                assert.strictEqual(value, -42.24);
            });

            it('should throw error if variable is not defined', function () {
                try {
                    Environment.getVariableAsNumber('DOES_NOT_EXIST');
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof MissingEnvironmentVariableError)) {
                        throw e;
                    }
                    assert.equal(e.message, '"DOES_NOT_EXIST" environment variable is not defined');
                }
            });

            it('should throw error if variable value is defined as empty string', function () {
                process.env.UNIT_TEST_VALUE = '';
                try {
                    Environment.getVariableAsNumber('UNIT_TEST_VALUE');
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof InvalidEnvironmentVariableError)) {
                        throw e;
                    }
                    assert.strictEqual(e.message, '"UNIT_TEST_VALUE" environment variable has a value that is not a number');
                }
            });

            it('should throw error if variable value is not numeric', function () {
                process.env.UNIT_TEST_VALUE = 'not a number';
                try {
                    Environment.getVariableAsNumber('UNIT_TEST_VALUE');
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof InvalidEnvironmentVariableError)) {
                        throw e;
                    }
                    assert.strictEqual(e.message, '"UNIT_TEST_VALUE" environment variable has a value that is not a number');
                }
            });
        });

        describe('Default value provided', function () {
            it('should return variable value if defined as number', function () {
                process.env.UNIT_TEST_VALUE = '-42.24';
                const value = Environment.getVariableAsNumber('UNIT_TEST_VALUE', -1);
                assert.strictEqual(value, -42.24);
            });

            it('should return default value if variable is not defined', function () {
                const value = Environment.getVariableAsNumber('DOES_NOT_EXIST', -1);
                assert.strictEqual(value, -1);
            });

            it('should throw error if variable value is defined as empty string', function () {
                process.env.UNIT_TEST_VALUE = '';
                try {
                    Environment.getVariableAsNumber('UNIT_TEST_VALUE', -1);
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof InvalidEnvironmentVariableError)) {
                        throw e;
                    }
                    assert.strictEqual(e.message, '"UNIT_TEST_VALUE" environment variable has a value that is not a number');
                }
            });

            it('should throw error if variable value is not numeric', function () {
                process.env.UNIT_TEST_VALUE = 'not a number';
                try {
                    Environment.getVariableAsNumber('UNIT_TEST_VALUE', -1);
                    assert.fail('Expected error was not thrown');
                } catch (e) {
                    if (!(e instanceof InvalidEnvironmentVariableError)) {
                        throw e;
                    }
                    assert.strictEqual(e.message, '"UNIT_TEST_VALUE" environment variable has a value that is not a number');
                }
            });
        });
    });

    describe('getVariableAsTimeSpan', function () {
        describe('No default value provided', function () {
            it('should return variable value if defined as time string', function () {
                process.env.UNIT_TEST_VALUE = '1 hour; 2 minutes; 30 seconds';
                const value = Environment.getVariableAsTimeSpan('UNIT_TEST_VALUE');
                expect(value.toJSON()).to.deep.equal({
                    years: 0,
                    weeks: 0,
                    days: 0,
                    hours: 1,
                    minutes: 2,
                    seconds: 30,
                    milliseconds: 0
                });
            });

            it('should throw error if variable is not defined', function () {
                executeAndExpectError(
                    () => {
                        Environment.getVariableAsTimeSpan('DOES_NOT_EXIST');
                    },
                    MissingEnvironmentVariableError,
                    '"DOES_NOT_EXIST" environment variable is not defined'
                );
            });

            it('should throw error if variable value is defined as empty string', function () {
                process.env.UNIT_TEST_VALUE = '';
                executeAndExpectError(
                    () => {
                        Environment.getVariableAsTimeSpan('UNIT_TEST_VALUE');
                    },
                    InvalidEnvironmentVariableError,
                    '"UNIT_TEST_VALUE" environment variable has an invalid value: Invalid time string [] - must be a valid time string (ex: "1 hour", "30 s", "1.5 days", "1m 30s")'
                );
            });

            it('should throw error if variable value cannot be parsed as a time string', function () {
                process.env.UNIT_TEST_VALUE = 'not a time string';
                executeAndExpectError(
                    () => {
                        Environment.getVariableAsTimeSpan('UNIT_TEST_VALUE');
                    },
                    InvalidEnvironmentVariableError,
                    '"UNIT_TEST_VALUE" environment variable has an invalid value: Invalid time string [not a time string] - must be a valid time string (ex: "1 hour", "30 s", "1.5 days", "1m 30s")'
                );
            });
        });

        describe('Default value provided', function () {
            it('should return variable value if defined as number', function () {
                process.env.UNIT_TEST_VALUE = '1 hour; 2 minutes; 30 seconds';
                const value = Environment.getVariableAsTimeSpan('UNIT_TEST_VALUE', TimeSpan.fromSeconds(0));
                expect(value.toJSON()).to.deep.equal({
                    years: 0,
                    weeks: 0,
                    days: 0,
                    hours: 1,
                    minutes: 2,
                    seconds: 30,
                    milliseconds: 0
                });
            });

            it('should return default value if variable is not defined', function () {
                const defaultValue = TimeSpan.fromDays(3);
                const value = Environment.getVariableAsTimeSpan('DOES_NOT_EXIST', defaultValue);
                // Use object reference compare for this one
                expect(value).to.equal(defaultValue);
            });

            it('should throw error if variable value is defined as empty string', function () {
                process.env.UNIT_TEST_VALUE = '';
                executeAndExpectError(
                    () => {
                        Environment.getVariableAsTimeSpan('UNIT_TEST_VALUE', TimeSpan.fromSeconds(0));
                    },
                    InvalidEnvironmentVariableError,
                    '"UNIT_TEST_VALUE" environment variable has an invalid value: Invalid time string [] - must be a valid time string (ex: "1 hour", "30 s", "1.5 days", "1m 30s")'
                );
            });

            it('should throw error if variable value cannot be parsed as a time string', function () {
                process.env.UNIT_TEST_VALUE = 'not a time string';
                executeAndExpectError(
                    () => {
                        Environment.getVariableAsTimeSpan('UNIT_TEST_VALUE', TimeSpan.fromSeconds(0));
                    },
                    InvalidEnvironmentVariableError,
                    '"UNIT_TEST_VALUE" environment variable has an invalid value: Invalid time string [not a time string] - must be a valid time string (ex: "1 hour", "30 s", "1.5 days", "1m 30s")'
                );
            });
        });
    });
});
