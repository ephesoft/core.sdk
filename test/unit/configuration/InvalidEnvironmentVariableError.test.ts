import { assert } from 'chai';
import { InvalidEnvironmentVariableError } from '../../../src/configuration/InvalidEnvironmentVariableError';

describe('InvalidEnvironmentVariableError unit tests', (): void => {
    describe('constructor(string, string?, Error?)', (): void => {
        it('should correctly populate error object if only variable name supplied', (): void => {
            const error = new InvalidEnvironmentVariableError('TEST_VARIABLE');
            assert.strictEqual(error.message, 'Invalid environment variable');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'InvalidEnvironmentVariableError');
            assert.strictEqual(error.variableName, 'TEST_VARIABLE');
        });

        it('should correctly populate error object if environment variable name and message supplied', (): void => {
            const error = new InvalidEnvironmentVariableError('TEST_VARIABLE', 'test message');
            assert.strictEqual(error.message, 'test message');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'InvalidEnvironmentVariableError');
            assert.strictEqual(error.variableName, 'TEST_VARIABLE');
        });

        it('should correctly populate error object if environment variable name, message, and inner error supplied', (): void => {
            const innerError = new Error('inner error');
            const error = new InvalidEnvironmentVariableError('TEST_VARIABLE', 'outer error', innerError);
            assert.strictEqual(error.message, 'outer error');
            assert.strictEqual(error.name, 'InvalidEnvironmentVariableError');
            assert.strictEqual(error.variableName, 'TEST_VARIABLE');
            assert.deepStrictEqual(error.innerError, innerError);
        });
    });

    describe('getLogData(): unknown', (): void => {
        it('should output the variable name', (): void => {
            const variableName = 'testVariable';
            const error = new InvalidEnvironmentVariableError(variableName, 'test message');
            const logData = error.getLogData();
            assert.deepStrictEqual(logData, { variableName });
        });
    });

    describe('toString()', (): void => {
        it('should return correct value if only environment variable name is specified', (): void => {
            // simulate real-life usage
            try {
                throw new InvalidEnvironmentVariableError('TEST_VARIABLE');
            } catch (error) {
                assert.strictEqual(
                    String(error),
                    'InvalidEnvironmentVariableError: Invalid environment variable (variable: TEST_VARIABLE)'
                );
            }
        });

        it('should return correct value if environment variable name and message are specified', (): void => {
            // simulate real-life usage
            try {
                throw new InvalidEnvironmentVariableError('TEST_VARIABLE', 'test message');
            } catch (error) {
                assert.strictEqual(String(error), 'InvalidEnvironmentVariableError: test message (variable: TEST_VARIABLE)');
            }
        });

        it('should return correct value object if environment variable name, message, and inner error are specified', (): void => {
            // simulate real-life usage
            try {
                try {
                    throw new Error('inner');
                } catch (e1) {
                    throw new InvalidEnvironmentVariableError('TEST_VARIABLE', 'outer', e1 as Error);
                }
            } catch (error) {
                assert.strictEqual(String(error), 'InvalidEnvironmentVariableError: outer (variable: TEST_VARIABLE)');
            }
        });
    });
});
