import { assert } from 'chai';
import { MissingEnvironmentVariableError } from '../../../src/configuration/MissingEnvironmentVariableError';

describe('MissingEnvironmentVariableError unit tests', (): void => {
    describe('constructor(string, string?, Error?)', (): void => {
        it('should correctly populate error object if only variable name supplied', (): void => {
            const error = new MissingEnvironmentVariableError('TEST_VARIABLE');
            assert.strictEqual(error.message, 'Missing environment variable');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'MissingEnvironmentVariableError');
            assert.strictEqual(error.variableName, 'TEST_VARIABLE');
        });

        it('should correctly populate error object if environment variable name and message supplied', (): void => {
            const error = new MissingEnvironmentVariableError('TEST_VARIABLE', 'test message');
            assert.strictEqual(error.message, 'test message');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'MissingEnvironmentVariableError');
            assert.strictEqual(error.variableName, 'TEST_VARIABLE');
        });

        it('should correctly populate error object if environment variable name, message, and inner error supplied', (): void => {
            const innerError = new Error('inner error');
            const error = new MissingEnvironmentVariableError('TEST_VARIABLE', 'outer error', innerError);
            assert.strictEqual(error.message, 'outer error');
            assert.strictEqual(error.name, 'MissingEnvironmentVariableError');
            assert.strictEqual(error.variableName, 'TEST_VARIABLE');
            assert.deepStrictEqual(error.innerError, innerError);
        });
    });

    describe('toString()', (): void => {
        it('should return correct value if only environment variable name is specified', (): void => {
            // simulate real-life usage
            try {
                throw new MissingEnvironmentVariableError('TEST_VARIABLE');
            } catch (e) {
                assert.strictEqual(
                    String(e),
                    'MissingEnvironmentVariableError: Missing environment variable (variable: TEST_VARIABLE)'
                );
            }
        });

        it('should return correct value if environment variable name and message are specified', (): void => {
            // simulate real-life usage
            try {
                throw new MissingEnvironmentVariableError('TEST_VARIABLE', 'test message');
            } catch (e) {
                assert.strictEqual(String(e), 'MissingEnvironmentVariableError: test message (variable: TEST_VARIABLE)');
            }
        });

        it('should return correct value object if environment variable name, message, and inner error are specified', (): void => {
            // simulate real-life usage
            try {
                try {
                    throw new Error('inner');
                } catch (e1) {
                    throw new MissingEnvironmentVariableError('TEST_VARIABLE', 'outer', e1 as Error);
                }
            } catch (e) {
                assert.strictEqual(String(e), 'MissingEnvironmentVariableError: outer (variable: TEST_VARIABLE)');
            }
        });
    });
});
