import { assert } from 'chai';
import { setPropertyIfValue } from '../../../src/properties/setPropertyIfValue';

describe('setPropertyIfValue unit tests', (): void => {
    it('should set property if it has a value', (): void => {
        const receiver: TestReceiver = {};
        const result = setPropertyIfValue('property', 'test', receiver);
        assert.deepStrictEqual(receiver, { property: 'test' });
        assert.strictEqual(result, true, 'setPropertyIfValue should have returned true');
    });

    it('should not set property if value is null', (): void => {
        const receiver: TestReceiver = {};
        const result = setPropertyIfValue('property', null, receiver);
        assert.deepStrictEqual(receiver, {});
        assert.strictEqual(result, false, 'setPropertyIfValue should have returned false');
    });

    it('should not set property if value is undefined', (): void => {
        const receiver: TestReceiver = {};
        const result = setPropertyIfValue('property', undefined, receiver);
        assert.deepStrictEqual(receiver, {});
        assert.strictEqual(result, false, 'setPropertyIfValue should have returned false');
    });

    interface TestReceiver {
        property?: string;
    }
});
