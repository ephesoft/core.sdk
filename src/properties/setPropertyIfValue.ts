/**
 * Sets a property on the specified object ONLY IF the value is not undefined or null.
 * @param key The property key to store the value under.
 * @param value The value to assign the property.
 * @param obj The object to set the property value on.
 * @typeParam TObject The type of the obj parameter - you should be able to omit this type parameter.
 * @typeParam TKey The keys which may be used - you should be able to omit this type parameter.
 * @returns true if the object property was set; false otherwise.
 */
export function setPropertyIfValue<TObject, TKey extends keyof TObject>(
    key: TKey,
    value: TObject[TKey] | undefined | null,
    obj: TObject
): boolean {
    if (value !== null && value !== undefined) {
        obj[key] = value;
        return true;
    }
    return false;
}
