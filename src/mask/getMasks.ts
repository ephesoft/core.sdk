// Had to use full package https://github.com/lodash/lodash/issues/5005
import get from 'lodash/get';
import has from 'lodash/has';
import merge from 'lodash/merge';
import isEmpty from 'lodash/isEmpty';
import pick from 'lodash/pick';
import { KeyedOptional } from '../types/KeyedOptional';
import { getMask } from './getMask';
import { MaskOptions } from './maskOptions';

/**
 * Get valid masks for an array of objects based on a key.  Allows the user to pass in an array of base and mask objects and get back only valid overrides (values not set to default and properties allowed to be changed).
 * @param bases The array of default objects to compare against.  If the mask is the same value it will not be returned.
 * @param masks The array of mask objects to evaluate and pull the valid properties from.
 * @param key The key used to align the objects between the base and mask array.
 * @param mutableProperties The properties that are allowed to be overridden.  If the property is not in this list it will not be returned.
 * @param options MaskOptions that can be set to control the allowed types
 * @returns An array of masks with the valid overridden properties.  If the object has no valid modified properties a blank object is returned.
 */
export function getMasks<T, K extends keyof T>(
    bases: T[],
    masks: T[],
    key: K,
    mutableProperties: string[], // Couldn't use K for nested properties as we need property1.property2 to work
    options?: MaskOptions
): KeyedOptional<T, K>[] {
    // Get the valid masks
    const validMasks = bases.map((x) => {
        // Find matching object
        const mask = masks.find((y) => {
            return has(y, key) && get(x, key) === get(y, key);
        });

        if (mask != undefined) {
            // Find valid mask
            const validMask = getMask(x, mask, mutableProperties, options);

            if (isEmpty(validMask) === false) {
                // Get the key and merge in mask
                return merge(pick(x, key), validMask);
            }
        }
        return null;
    });

    // Filter out null and undefined items from the array
    return validMasks.filter((i): i is T => i != null);
}
