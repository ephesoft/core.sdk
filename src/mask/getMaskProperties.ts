// Had to use full package https://github.com/lodash/lodash/issues/5005
import get from 'lodash/get';
import has from 'lodash/has';
import isObject from 'lodash/isObject';
import { InvalidDataTypeError } from '../errors/InvalidDataTypeError';
import { MaskOptions } from './maskOptions';
import { DeepPartial } from '../types/DeepPartial';

/**
 * Get a list of valid mask properties for an object.  Allows the user to pass in a default and mask object and get back only valid override properties (values not set to default and properties allowed to be changed).
 * @param base The default object to compare against.  If the mask is the same value it will not be returned.
 * @param mask The mask object to evaluate and pull the valid properties from.
 * @param mutableProperties The properties that are allowed to be overridden.  If the property is not in this list it will not be returned.
 * @param options MaskOptions that can be set to control the allowed types
 * @returns An array of valid mask properties.  If the mask has no valid modified properties a blank array is returned.
 */
export function getMaskProperties<T>(
    base: T,
    mask: DeepPartial<T>,
    mutableProperties: string[],
    options?: MaskOptions
): string[] {
    // Find valid overridden values
    const validProperties: string[] = [];

    mutableProperties.forEach((property) => {
        if (has(base, property) && has(mask, property)) {
            const baseValue: unknown = get(base, property);
            const maskValue: unknown = get(mask, property);

            // Validate properties aren't objects
            if (options?.allowObjects !== true && (isObject(baseValue) || isObject(maskValue))) {
                throw new InvalidDataTypeError(
                    `Property: ${property} - Object type not allowed as mutable properties.  You can override this using options.allowObjects.`
                );
            }

            // Validate properties aren't null
            if (options?.forbidNulls === true && (baseValue === null || maskValue === null)) {
                throw new InvalidDataTypeError(
                    `Property: ${property} - Null values not allowed as mutable properties.  You can override this using options.forbidNulls.`
                );
            }

            // Validate property data types match, a null value will not match
            if (
                options?.allowDifferentTypes !== true &&
                baseValue !== null &&
                maskValue !== null &&
                typeof baseValue !== typeof maskValue
            ) {
                throw new InvalidDataTypeError(
                    `Property: ${property} - Mask type of ${typeof maskValue} does not match base type of ${typeof baseValue}.  You can override this using options.allowDifferentTypes.`
                );
            }

            // Save property for fetching
            if (baseValue !== maskValue) {
                validProperties.push(property);
            }
        }
    });

    return validProperties;
}
