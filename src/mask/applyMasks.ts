// Had to use full package https://github.com/lodash/lodash/issues/5005
import get from 'lodash/get';
import has from 'lodash/has';
import { applyMask } from './applyMask';
import { MaskOptions } from './maskOptions';

/**
 * Get an array of masked objects.  Allows the user to pass in an array of default and override or mask objects and get back an array of combined object with any valid masks applied.
 * @param bases The array of default objects to use as the initial value if not masked.
 * @param masks The array of mask objects to evaluate and override the default values with.  The mask will not override undefined properties.
 * @param key The key used to align the objects between the base and mask array.
 * @param mutableProperties The properties that are allowed to be masked.  If the property is not in this list it will not be updated.
 * @param options MaskOptions that can be set to control the allowed types
 * @returns An array of masked objects with the default values or valid overridden values from the mask.
 */
export function applyMasks<T, K extends keyof T>(
    bases: T[],
    masks: Partial<T>[],
    key: K, // Couldn't use K for nested properties as we need property1.property2 to work
    mutableProperties: string[],
    options?: MaskOptions
): T[] {
    // Get the object with mask applied
    const masked = bases.map((x) => {
        // Find matching object
        const mask = masks.find((y) => {
            return has(y, key) && get(x, key) === get(y, key);
        });

        if (mask != undefined) {
            // Get Computed Value
            return applyMask<T>(x, mask, mutableProperties, options);
        }
        return x;
    });

    return masked;
}
