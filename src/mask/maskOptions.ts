/**
 * Options used by the mask functions
 * @forbidNulls Throw an error if a null is found in the mutableProperties
 * @allowObjects Allow the mutableProperties to reference objects instead of primitive types
 * @allowDifferentTypes Allow mask property type to differ from base type
 */
export interface MaskOptions {
    forbidNulls?: boolean;
    allowObjects?: boolean;
    allowDifferentTypes?: boolean;
}
