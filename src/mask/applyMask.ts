// Had to use full package https://github.com/lodash/lodash/issues/5005
import merge from 'lodash/merge';
import pick from 'lodash/pick';
import { DeepPartial } from '../types/DeepPartial';
import { getMaskProperties } from './getMaskProperties';
import { MaskOptions } from './maskOptions';

/**
 * Get the masked object.  Allows the user to pass in a default and override or mask object and get back the combined object with any valid masks applied.
 * @param base The default object to use as the initial value if not masked.
 * @param mask The mask object to evaluate and override the default values with.  The mask will not override undefined properties.
 * @param mutableProperties The properties that are allowed to be masked.  If the property is not in this list it will not be updated.
 * @param options MaskOptions that can be set to control the allowed types
 * @returns A masked object with the default values or valid overridden values from the mask.
 */
export function applyMask<T>(base: T, mask: DeepPartial<T>, mutableProperties: string[], options?: MaskOptions): T {
    // Find valid overridden properties
    const validProperties = getMaskProperties(base, mask, mutableProperties, options);

    const masked = pick(mask, validProperties);

    // Create new object and merge in values
    return merge({}, base, masked);
}
