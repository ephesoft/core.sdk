// Had to use full package https://github.com/lodash/lodash/issues/5005
import pick from 'lodash/pick';
import { MaskOptions } from './maskOptions';
import { DeepPartial } from '../types/DeepPartial';
import { getMaskProperties } from './getMaskProperties';

/**
 * Get a valid mask for an object.  Allows the user to pass in a default and mask object and get back only valid overrides (values not set to default and properties allowed to be changed).
 * @param base The default object to compare against.  If the mask is the same value it will not be returned.
 * @param mask The mask object to evaluate and pull the valid properties from.
 * @param mutableProperties The properties that are allowed to be overridden.  If the property is not in this list it will not be returned.
 * @param options MaskOptions that can be set to control the allowed types
 * @returns A mask with the valid overridden properties.  If the object has no valid modified properties a blank object is returned.
 */
export function getMask<T>(base: T, mask: DeepPartial<T>, mutableProperties: string[], options?: MaskOptions): DeepPartial<T> {
    // Find valid overridden values
    const validProperties = getMaskProperties(base, mask, mutableProperties, options);

    if (validProperties.length != 0) {
        // Get valid overridden values
        return pick(mask, validProperties);
    }
    return {};
}
