const regexControlCharactersExpression = /[\\^$.*+?()[\]{}|]/g;

/**
 * Escapes any regular expression control characters in the specified text by adding a
 * "\" before the control character.  The following characters are considered control
 * characters: ^ $ \ . * + ? ( ) [ ] { } |
 *
 * @param text The text to be escaped.
 * @returns The escaped text.
 */
export function regexEscape(text: string): string {
    // Add a "\" in front of any control characters
    return text.replace(regexControlCharactersExpression, '\\$&');
}
