import { TimeSpan } from '../conversion/TimeSpan';
import { InvalidOperationError } from '../errors/InvalidOperationError';
import { TimeoutError } from '../errors/TimeoutError';
import { SemaphoreAbortedError } from './SemaphoreAbortedError';
import { sleep } from './sleep';

/**
 * Configuration for a [[Semaphore]] instance.
 */
export interface SemaphoreConfiguration {
    /**
     * The maximum number of operations that can access the [[Semaphore]] concurrently.
     */
    maximumConcurrentAccess: number;

    /**
     * The interval at which to check if new operations can access the [[Semaphore]].
     *
     * Defaults to 5 milliseconds.
     */
    checkInterval?: TimeSpan;
}

/**
 * Control structure for limiting concurrent access to a resource to a
 * specified number of operations.
 * ```typescript
 * // Restricts the number of concurrent HTTP DELETE requests to 5
 * export class SimpleExample {
 *     readonly #semaphore = new Semaphore({ maximumConcurrentAccess: 5 });
 *     readonly #timeout = TimeSpan.fromMinutes(1);
 *
 *     public async delete(id: number): Promise<void> {
 *         await this.#semaphore.wait(this.#timeout);
 *         try {
 *             await axios.delete(`https://rate-limited-domain.org/items/${id}`);
 *         } finally {
 *             this.#semaphore.release();
 *         }
 *     }
 *
 *     public async drain(): Promise<void> {
 *         // Wait for all processes to exit
 *         await this.#semaphore.flush(this.#timeout);
 *     }
 *
 *     public async destroy(): Promise<void> {
 *         // Terminate any waiting processes
 *         this.#semaphore.abort();
 *         // Wait for all processes to exit the Semaphore
 *         await this.#semaphore.flush(this.#timeout);
 *     }
 * }
 * ```
 */
export class Semaphore {
    readonly #maximumConcurrentAccess: number;
    readonly #checkIntervalMilliseconds: number;
    #numberOfOperations: number;
    #abort: boolean;

    /**
     * Initializes a new instance of the [[Semaphore]] class.
     * @param configuration Configuration options for the new [[Semaphore]] instance.
     */
    public constructor(configuration: SemaphoreConfiguration) {
        this.#maximumConcurrentAccess = configuration.maximumConcurrentAccess;
        this.#checkIntervalMilliseconds = configuration.checkInterval?.totalMilliseconds ?? 5;
        this.#numberOfOperations = 0;
        this.#abort = false;
    }

    /**
     * Maximum operations allowed to access the [[Semaphore]] concurrently.
     */
    public get maximumOperations(): number {
        return this.#maximumConcurrentAccess;
    }

    /**
     * Number of operations currently accessing the [[Semaphore]].
     */
    public get currentOperations(): number {
        return this.#numberOfOperations;
    }

    /**
     * Waits for access to the [[Semaphore]].
     * @param timeout The amount of time to wait before giving up (defaults to no timeout).
     * @throws [[TimeoutError]] If the [[Semaphore]] does not become available within the timeout period.
     * @throws [[SemaphoreAbortedError]] If [[wait]] is called after [[abort]] is called.
     */
    public async wait(timeout: TimeSpan = TimeSpan.maximum): Promise<void> {
        const timeoutDateTime = Date.now() + timeout.totalMilliseconds;
        while (!this.#abort && this.#numberOfOperations >= this.#maximumConcurrentAccess) {
            if (Date.now() > timeoutDateTime) {
                throw new TimeoutError('Timed out waiting for Semaphore');
            }
            await sleep(this.#checkIntervalMilliseconds);
        }
        if (this.#abort) {
            throw new SemaphoreAbortedError('Semaphore was aborted');
        }
        this.#numberOfOperations++;
    }

    /**
     * Informs the [[Semaphore]] that the current operation has finished.
     * @throws [[InvalidOperationError]] If all operations have already called [[release]].
     */
    public release(): void {
        if (this.#numberOfOperations <= 0) {
            throw new InvalidOperationError('Semaphore does not have any wait handles to release');
        }
        this.#numberOfOperations--;
    }

    /**
     * Waits for all operations to exit the [[Semaphore]].
     * @param timeout The amount of time to wait before giving up (defaults to no timeout).
     * @throws [[TimeoutError]] If all operations don't call [[release]] within the timeout period.
     */
    public async flush(timeout: TimeSpan = TimeSpan.maximum): Promise<void> {
        const timeoutDateTime = Date.now() + timeout.totalMilliseconds;
        while (this.#numberOfOperations > 0) {
            if (Date.now() > timeoutDateTime) {
                throw new TimeoutError('Timed out waiting for semaphore to exit');
            }
            await sleep(this.#checkIntervalMilliseconds);
        }
    }

    /**
     * Aborts any operations currently waiting on the [[Semaphore]].
     *
     * NOTE: This method does not wait for all operations to exit the [[Semaphore]].
     *       Call [[flush]] after calling [[abort]] to wait for any currently-running operations.
     */
    public abort(): void {
        this.#abort = true;
    }
}
