import { NestedError } from '../errors/NestedError';

/**
 * Indicates a [[Semaphore]] was aborted.
 */
export class SemaphoreAbortedError extends NestedError {
    /**
     * Initializes a new instance of the [[SemaphoreAbortedError]] class.
     * @param message A description of the error.
     * @param innerError The error that caused the [[SemaphoreAbortedError]].
     */
    public constructor(message: string, innerError?: Error) {
        super(message, innerError);
        this.name = 'SemaphoreAbortedError';
    }
}
