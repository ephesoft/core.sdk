/**
 * Pauses execution for the specified number of milliseconds.
 * @param milliseconds The number of milliseconds to sleep.
 * @returns A Promise that will resolve after the specified number of milliseconds.
 */
export async function sleep(milliseconds: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, milliseconds));
}
