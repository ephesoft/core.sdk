/**
 * Makes all properties in T required recursively.
 *
 * This differs from Required in that Required only makes top-level properties required.
 *
 * Example:
 * ```typescript
 * interface User {
 *     id?: string;
 *     email: string;
 *     roles: {
 *         id: string;
 *         name?: string;
 *     }[];
 *     name: {
 *         first: string;
 *         middle?: string;
 *         last: string;
 *         suffix?: string;
 *     },
 *     birthDate: Date;
 * }
 * // If we cast the type above as DeepRequired<User>, it becomes:
 * // {
 * //     id: string;            // All properties (even nested) are made required
 * //     email: string;
 * //     roles: {
 * //         id: string;
 * //         name: string;
 * //     }[];
 * //     name: {
 * //         first: string;
 * //         middle: string;
 * //         last: string;
 * //         suffix: string;
 * //     },
 * //     birthDate: Date;
 * // }
 * ```
 * @typeParam T The type to cast to [[DeepRequired]].
 */
export type DeepRequired<T> = Required<{ [P in keyof T]: DeepRequired<T[P]> }>;
