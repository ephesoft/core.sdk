/**
 * Generic collection of key-value pairs similar to a Map in Java or a Dictionary in .NET.
 * @typeparam TValue The type of values to be stored in the map.
 */
export interface Map<TValue> {
    [key: string]: TValue;
}
