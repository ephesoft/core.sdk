import { DeepPartial } from './DeepPartial';

/**
 * Constructs a type with a required key and all other properties optional
 * Unlike Partial it is recursive and sets all properties to optional.
 * @typeparam T The type to be made KeyedOptional
 * @typeparam K The key to use for typing
 */
export type KeyedOptional<T, K extends keyof T> = Pick<T, K> & DeepPartial<T>;
