/**
 * Constructs a type with all properties of Type set to read/write.
 *
 * Example:
 * ```typescript
 * interface User {
 *     readonly id?: string;
 *     email: string;
 *     roles: {
 *         readonly id: string;
 *         name?: string;
 *     }[];
 *     name: {
 *         readonly first: string;
 *         middle?: string;
 *         readonly last: string;
 *         suffix?: string;
 *     },
 *     birthDate: Date;
 * }
 * // If we cast the type above as Mutable<TestType>, it becomes:
 * // {
 * //     id?: string;            // All top-level properties are made mutable
 * //     email: string;          // All nested properties remain as they were
 * //     roles: {
 * //         readonly id: string;
 * //         name?: string;
 * //     }[];
 * //     name: {
 * //         readonly first: string;
 * //         middle?: string;
 * //         readonly last: string;
 * //         suffix?: string;
 * //     },
 * //     birthDate: Date;
 * // }
 * ```
 * @typeparam T The type to be made mutable.
 */
export type Mutable<T> = {
    -readonly [P in keyof T]: T[P];
};
