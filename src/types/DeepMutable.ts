// This implementation was found here: https://typeofnan.dev/creating-your-own-deeppartial-type-in-typescript/

import { Mutable } from './Mutable';

/**
 * Makes all properties in T mutable recursively.
 *
 * This differs from Mutable in that Mutable only makes top-level properties mutable.
 *
 * Example:
 * ```typescript
 * interface User {
 *     readonly id?: string;
 *     email: string;
 *     roles: {
 *         readonly id: string;
 *         name?: string;
 *     }[];
 *     name: {
 *         readonly first: string;
 *         middle?: string;
 *         readonly last: string;
 *         suffix?: string;
 *     },
 *     birthDate: Date;
 * }
 * // If we cast the type above as DeepMutable<User>, it becomes:
 * // {
 * //     id?: string;          // readonly is removed from all properties including nested ones
 * //     email: string;
 * //     roles: {
 * //         id: string;
 * //         name?: string;
 * //     }[];
 * //     name: {
 * //         first: string;
 * //         middle?: string;
 * //         last: string;
 * //         suffix?: string;
 * //     },
 * //     birthDate: Date;
 * // }
 * ```
 * @typeparam T The type to be made mutable.
 */
export type DeepMutable<T> = Mutable<{ [P in keyof T]: DeepMutable<T[P]> }>;
