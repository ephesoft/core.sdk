/**
 * Constructs a type with a one or more properties made required.
 *
 * This differs from Required in that you can specify which properties are required.
 *
 * Example:
 * ```typescript
 * interface User {
 *     id?: string;
 *     email: string;
 *     roles: {
 *         id: string;
 *         name?: string;
 *     }[];
 *     name: {
 *         first: string;
 *         middle?: string;
 *         last: string;
 *         suffix?: string;
 *     },
 *     birthDate?: Date;
 * }
 * // If we cast the type above as Require<User, 'id' | 'birthDate'>, it becomes:
 * // {
 * //     id: string;           // id and birthdate properties becomes required
 * //     email: string;        // All other properties are left as they were
 * //     roles: {
 * //         id: string;
 * //         name?: string;
 * //     }[];
 * //     name: {
 * //         first: string;
 * //         middle?: string;
 * //         last: string;
 * //         suffix?: string;
 * //     },
 * //     birthDate: Date;
 * // }
 * ```
 * @typeparam T The type to make properties required in
 * @typeparam K The key or keys to be made required
 */
export type Require<T, K extends keyof T> = Omit<T, K> & Required<Pick<T, K>>;
