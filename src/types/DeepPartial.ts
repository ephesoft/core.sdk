// This implementation was taken from https://typeofnan.dev/creating-your-own-deeppartial-type-in-typescript/

/**
 * Makes all properties in T optional recursively.
 *
 * This differs from Partial in that Partial only makes top-level properties optional.
 *
 * Example:
 * ```typescript
 * interface User {
 *     id?: string;
 *     email: string;
 *     roles: {
 *         id: string;
 *         name?: string;
 *     }[];
 *     name: {
 *         first: string;
 *         middle?: string;
 *         last: string;
 *         suffix?: string;
 *     },
 *     birthDate: Date;
 * }
 * // If we cast the type above as DeepPartial<User>, it becomes:
 * // {
 * //     id?: string;            // All properties (even nested) are made optional
 * //     email?: string;
 * //     roles?: {
 * //         id?: string;
 * //         name?: string;
 * //     }[];
 * //     name?: {
 * //         first?: string;
 * //         middle?: string;
 * //         last?: string;
 * //         suffix?: string;
 * //     },
 * //     birthDate?: Date;
 * // }
 * ```
 * @typeParam T The type to cast to [[DeepPartial]].
 */
export type DeepPartial<T> = Partial<{ [P in keyof T]: DeepPartial<T[P]> }>;
