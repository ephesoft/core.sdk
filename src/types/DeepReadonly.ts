/**
 * Makes all properties in T read-only recursively.
 *
 * This differs from Readonly in that Readonly only makes top-level properties read-only.
 *
 * Example:
 * ```typescript
 * interface User {
 *     id?: string;
 *     email: string;
 *     roles: {
 *         id: string;
 *         name?: string;
 *     }[];
 *     name: {
 *         first: string;
 *         middle?: string;
 *         last: string;
 *         suffix?: string;
 *     },
 *     birthDate: Date;
 * }
 * // If we cast the type above as DeepReadonly<User>, it becomes:
 * // {
 * //     readonly id?: string;            // All properties (even nested) are made read-only
 * //     readonly email: string;
 * //     readonly roles: {
 * //         readonly id: string;
 * //         readonly name?: string;
 * //     }[];
 * //     readonly name: {
 * //         readonly first: string;
 * //         readonly middle?: string;
 * //         readonly last: string;
 * //         readonly suffix?: string;
 * //     },
 * //     readonly birthDate: Date;
 * // }
 * ```
 * @typeParam T The type to cast to [[DeepReadonly]].
 */
export type DeepReadonly<T> = Readonly<{ [P in keyof T]: DeepReadonly<T[P]> }>;
