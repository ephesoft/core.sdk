import { InvalidEnvironmentVariableError } from './InvalidEnvironmentVariableError';

/**
 * Indicates a required environment variable is not defined.
 */
export class MissingEnvironmentVariableError extends InvalidEnvironmentVariableError {
    /**
     * Initializes a new instance of the [[MissingEnvironmentVariableError]] class.
     * @param variableName The name of the missing environment variable.
     * @param message A description of the error.
     * @param innerError The error that caused the [[MissingEnvironmentVariableError]].
     */
    public constructor(variableName: string, message?: string, innerError?: Error) {
        super(variableName, message ?? 'Missing environment variable', innerError);
        this.name = 'MissingEnvironmentVariableError';
    }
}
