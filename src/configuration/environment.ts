import * as Conversion from '../conversion/conversion';
import * as StringConversion from '../conversion/stringConversion';
import { MissingEnvironmentVariableError } from './MissingEnvironmentVariableError';
import { InvalidEnvironmentVariableError } from './InvalidEnvironmentVariableError';
import { TimeSpan } from '../conversion/TimeSpan';

/**
 * Returns whether the specified environment variable is defined.\
 * **NOTE**: Even if the variable is assigned a value of '', it is considered defined.
 * @param name The name of the variable to test.
 * @returns true if the variable is defined; false otherwise.
 */
export function isVariableDefined(name: string): boolean {
    const value: string | undefined = process.env[name];
    return typeof value === 'string';
}

/**
 * Gets the value of the specified environment variable.  If the environment variable is not defined,
 * the behavior depends on whether a defaultValue parameter is provided:
 * - If defaultValue is provided, that value is returned.
 * - If defaultValue is not provided, a [[MissingEnvironmentVariableError]] is thrown.
 * @param name The name of the environment variable to retrieve.
 * @param defaultValue The value to be returned if the requested environment variable does not exist.
 * @returns The value of the requested environment variable or defaultValue.
 * @throws [[MissingEnvironmentVariableError]] if the requested environment variable is not defined
 *                                             and defaultValue is not provided.
 */
export function getVariable(name: string, defaultValue?: string): string {
    const value: string | undefined = process.env[name];
    if (typeof value === 'string') {
        return value;
    }
    if (typeof defaultValue === 'string') {
        return defaultValue;
    }
    throw new MissingEnvironmentVariableError(name, `"${name}" environment variable is not defined`);
}

/**
 * Gets the value of the specified environment variable as a boolean.  If the environment variable is not
 * defined, the behavior depends on whether a defaultValue parameter is provided:
 * - If defaultValue is provided, that value is returned.
 * - If defaultValue is not provided, a [[MissingEnvironmentVariableError]] is thrown.
 * @param name The name of the environment variable to retrieve.
 * @param defaultValue The value to be returned if the requested environment variable does not exist.
 * @returns The value of the requested environment variable or defaultValue.
 * @throws [[MissingEnvironmentVariableError]] if the requested environment variable is not defined and
 *                                             defaultValue is not provided.
 * @throws [[InvalidEnvironmentVariableError]] if the environment variable exists, but cannot be converted
 *                                             to a boolean.
 */
export function getVariableAsBoolean(name: string, defaultValue?: boolean): boolean {
    const textValue: string | undefined = process.env[name];
    if (typeof textValue === 'string') {
        try {
            return Conversion.toBoolean(textValue);
        } catch (ConversionError) {
            throw new InvalidEnvironmentVariableError(name, `"${name}" environment variable has a value that is not a boolean`);
        }
    }
    if (typeof defaultValue === 'boolean') {
        return defaultValue;
    }
    throw new MissingEnvironmentVariableError(name, `"${name}" environment variable is not defined`);
}

/**
 * Gets the value of the specified environment variable as a number.  If the environment variable is not
 * defined, the behavior depends on whether a defaultValue parameter is provided:\
 * - If defaultValue is provided, that value is returned.\
 * - If defaultValue is not provided, a [[MissingEnvironmentVariableError]] is thrown.
 * @param name The name of the environment variable to retrieve.
 * @param defaultValue The value to be returned if the requested environment variable does not exist.
 * @returns The value of the requested environment variable or defaultValue.
 * @throws [[MissingEnvironmentVariableError]] if the requested environment variable is not defined and
 *                                             defaultValue is not provided.
 * @throws [[InvalidEnvironmentVariableError]] if the environment variable exists, but cannot be converted
 *                                             to a number.
 */
export function getVariableAsNumber(name: string, defaultValue?: number): number {
    const textValue: string | undefined = process.env[name];
    if (typeof textValue === 'string') {
        try {
            return StringConversion.toNumber(textValue);
        } catch (ConversionError) {
            throw new InvalidEnvironmentVariableError(name, `"${name}" environment variable has a value that is not a number`);
        }
    }
    if (typeof defaultValue === 'number') {
        return defaultValue;
    }
    throw new MissingEnvironmentVariableError(name, `"${name}" environment variable is not defined`);
}

/**
 * Gets the value of the specified environment variable as a [[TimeSpan]].
 * @param name The name of the environment variable to retrieve.
 * @param defaultValue The value to be returned if the requested environment variable does not exist.
 * @returns The value of the requested environment variable or defaultValue.
 * @throws [[MissingEnvironmentVariableError]] if the requested environment variable is not defined and
 *                                             defaultValue is not provided.
 * @throws [[InvalidEnvironmentVariableError]] if the environment variable exists, but cannot be converted
 *                                             to a TimeSpan.
 */
export function getVariableAsTimeSpan(name: string, defaultValue?: TimeSpan): TimeSpan {
    const textValue: string | undefined = process.env[name];
    if (typeof textValue === 'string') {
        try {
            return TimeSpan.fromTimeString(textValue);
        } catch (error) {
            throw new InvalidEnvironmentVariableError(
                name,
                `"${name}" environment variable has an invalid value: ${(error as Error).message}`,
                error as Error
            );
        }
    }
    if (typeof defaultValue === 'object') {
        return defaultValue;
    }
    throw new MissingEnvironmentVariableError(name, `"${name}" environment variable is not defined`);
}
