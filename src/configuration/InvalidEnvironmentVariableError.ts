import { NestedError } from '../errors/NestedError';

/**
 * Indicates an environment variable value was found to be invalid.
 */
export class InvalidEnvironmentVariableError extends NestedError {
    /**
     * The name of the invalid environment variable.
     */
    public readonly variableName: string;

    /**
     * Initializes a new instance of the [[InvalidEnvironmentVariableError]] class.
     * @param variableName The name of the invalid environment variable.
     * @param message A description of the error.
     * @param innerError The error that caused the [[InvalidEnvironmentVariableError]].
     */
    public constructor(variableName: string, message?: string, innerError?: Error) {
        super(message ?? 'Invalid environment variable', innerError);
        this.variableName = variableName;
        this.name = 'InvalidEnvironmentVariableError';
    }

    /**
     * Creates a string representation of this error.
     * @returns A string representation of this error.
     */
    public toString(): string {
        // This does not output nested errors as they may contain sensitive data.
        // Best to let the consumer decide what to expose.
        return `${this.constructor.name}: ${this.message} (variable: ${this.variableName})`;
    }

    /**
     * Returns data intended to be consumed by a logger.
     * @returns A data structure to be included in the log entry.
     */
    public getLogData(): unknown {
        return {
            variableName: this.variableName
        };
    }
}
