/**
 * Represents a unit of time.
 */
export enum TimeUnit {
    /**
     * Millisecond (1/1000 of a second)
     */
    Milliseconds = 'milliseconds',

    /**
     * Second
     */
    Seconds = 'seconds',

    /**
     * Minute (60 seconds)
     */
    Minutes = 'minutes',

    /**
     * Hour (60 minutes)
     */
    Hours = 'hours',

    /**
     * Day (24 hours)
     */
    Days = 'days',

    /**
     * Week (7 days)
     */
    Weeks = 'weeks',

    /**
     * Year (365 days)
     */
    Years = 'years'
}
