import { TimeUnit } from './TimeUnit';
import { InvalidParameterError } from '../errors/InvalidParameterError';

// Value used to convert decimals to integers and vise versa
const DECIMAL_FACTOR = 100000;
const MULTIPLIER: Record<TimeUnit, number> = {
    [TimeUnit.Milliseconds]: 1,
    [TimeUnit.Seconds]: 1000,
    [TimeUnit.Minutes]: 60 * 1000,
    [TimeUnit.Hours]: 60 * 60 * 1000,
    [TimeUnit.Days]: 24 * 60 * 60 * 1000,
    [TimeUnit.Weeks]: 7 * 24 * 60 * 60 * 1000,
    [TimeUnit.Years]: 365 * 24 * 60 * 60 * 1000
};
function getMultiplier(unit: TimeUnit): number {
    const result = MULTIPLIER[unit];
    if (result === undefined) {
        throw new InvalidParameterError('unit', `Unsupported time unit [${unit}]`);
    }
    return result;
}
const MAXIMUM_VALUE: Record<TimeUnit, number> = {
    [TimeUnit.Milliseconds]: Math.floor(Number.MAX_SAFE_INTEGER / DECIMAL_FACTOR),
    [TimeUnit.Seconds]: Number.MAX_SAFE_INTEGER / DECIMAL_FACTOR / 1000,
    [TimeUnit.Minutes]: Number.MAX_SAFE_INTEGER / DECIMAL_FACTOR / 1000 / 60,
    [TimeUnit.Hours]: Number.MAX_SAFE_INTEGER / DECIMAL_FACTOR / 1000 / 60 / 60,
    [TimeUnit.Days]: Number.MAX_SAFE_INTEGER / DECIMAL_FACTOR / 1000 / 60 / 60 / 24,
    [TimeUnit.Weeks]: Number.MAX_SAFE_INTEGER / DECIMAL_FACTOR / 1000 / 60 / 60 / 24 / 7,
    [TimeUnit.Years]: Number.MAX_SAFE_INTEGER / DECIMAL_FACTOR / 1000 / 60 / 60 / 24 / 365
};
const TEXT_TO_UNIT: Record<string, TimeUnit> = {
    ms: TimeUnit.Milliseconds,
    millisecond: TimeUnit.Milliseconds,
    milliseconds: TimeUnit.Milliseconds,

    s: TimeUnit.Seconds,
    second: TimeUnit.Seconds,
    seconds: TimeUnit.Seconds,

    m: TimeUnit.Minutes,
    minute: TimeUnit.Minutes,
    minutes: TimeUnit.Minutes,

    h: TimeUnit.Hours,
    hour: TimeUnit.Hours,
    hours: TimeUnit.Hours,

    d: TimeUnit.Days,
    day: TimeUnit.Days,
    days: TimeUnit.Days,

    w: TimeUnit.Weeks,
    week: TimeUnit.Weeks,
    weeks: TimeUnit.Weeks,

    y: TimeUnit.Years,
    year: TimeUnit.Years,
    years: TimeUnit.Years
};

/**
 * Parses a time string (ex: "1h", "1.5 days", "1m30s") into milliseconds.
 *
 * Supported time units are as follows:
 *
 * * y, years         - 3.154e+10 milliseconds
 * * w, weeks         - 6.048e+8 milliseconds
 * * d, days          - 8.64e+7 milliseconds
 * * h, hours         - 3.6e+6 milliseconds
 * * m, minutes       - 60000 milliseconds
 * * s, seconds       - 1000 milliseconds
 * * ms, milliseconds - 1 millisecond
 * @param timeString The time string to convert to milliseconds.
 * @returns The value in milliseconds.
 */
function parseMilliseconds(timeString: string): number {
    const tokens: string[] = tokenize(timeString);
    let milliseconds = 0;
    for (const token of tokens) {
        const parsedToken = parseToken(token, timeString);
        if (parsedToken.value > MAXIMUM_VALUE[parsedToken.unit]) {
            throw new InvalidParameterError(
                'timeString',
                `Invalid time string [${timeString}] - value is too large to be represented by a TimeSpan`
            );
        }
        const tokenMilliseconds = round(parsedToken.value * getMultiplier(parsedToken.unit));
        if (tokenMilliseconds > MAXIMUM_VALUE[TimeUnit.Milliseconds] - milliseconds) {
            throw new InvalidParameterError(
                'timeString',
                `Invalid time string [${timeString}] - value is too large to be represented by a TimeSpan`
            );
        }
        milliseconds += tokenMilliseconds;
    }
    return Math.round(milliseconds);
}

/**
 * Breaks down a time string into tokens consisting of a value and a unit.
 * @param timeString The time string to break down.
 * @returns The time tokens.
 * @throws InvalidParameterError If [[time]] is invalid.
 */
function tokenize(timeString: string): string[] {
    const tokens: string[] = [];
    let tokenMatches: string[] | null;
    let text = timeString;
    let checkText = '';
    do {
        tokenMatches =
            /[;,]{0,1}\s*[0-9.]+\s*(milliseconds|millisecond|ms|seconds|second|s|minutes|minute|m|hours|hour|h|days|day|d|weeks|week|w|years|year|y){1}\s*/is.exec(
                text
            );
        if (tokenMatches?.length) {
            const matchText = tokenMatches[0];
            checkText += matchText;
            text = text.substr(matchText.length);
            tokens.push(matchText.trim());
        }
    } while (tokenMatches?.length);
    if (timeString !== checkText || tokens.length === 0) {
        throw new InvalidParameterError(
            'timeString',
            `Invalid time string [${timeString}] - must be a valid time string (ex: "1 hour", "30 s", "1.5 days", "1m 30s")`
        );
    }
    return tokens;
}

/**
 * Parses a single time token.
 * @param token The token to be parsed.
 * @param timeString The complete time string.
 * @returns A parsed time token consisting of a numeric value and a [[TimeUnit]].
 * @throws InvalidParameterError If [[token]] is invalid.
 */
function parseToken(token: string, timeString: string): { value: number; unit: TimeUnit } {
    // strip delimiters
    token = token.replace(/^\s*[,;]\s*/, '');
    const elementMatches =
        /^([0-9.]+)\s*(milliseconds|millisecond|ms|seconds|second|s|minutes|minute|m|hours|hour|h|days|day|d|weeks|week|w|years|year|y)$/is.exec(
            token
        ) as string[];
    if ((elementMatches?.length ?? 0) !== 3) {
        // In theory this code should be unreachable...
        throw new InvalidParameterError(
            'value',
            `Invalid time string [${timeString}] - must be a valid time string (ex: "1 hour", "30 s", "1.5 days")`
        );
    }

    const result = {
        value: convertToNumber(elementMatches[1], timeString),
        unit: convertToTimeUnit(elementMatches[2], timeString)
    };

    if (result.unit === TimeUnit.Milliseconds && Math.floor(result.value) !== result.value) {
        throw new InvalidParameterError(
            'timeString',
            `Invalid time string [${timeString}] - milliseconds value must be a positive integer`
        );
    }

    return result;
}

/**
 * Converts the specified text to a number.
 * @param valueText The text to be converted.
 * @param timeString The complete time string.
 * @returns The numeric value of [[valueText]].
 * @throws InvalidParameterError If [[valueText]] is invalid.
 */
function convertToNumber(valueText: string, timeString: string): number {
    const numericValue = parseFloat(valueText);
    if (isNaN(numericValue) || !isFinite(numericValue)) {
        throw new InvalidParameterError(
            'timeString',
            `Invalid time string [${timeString}] - cannot parse [${valueText}] as number`
        );
    }
    return numericValue;
}

/**
 * Converts the specified unit text to a [[TimeUnit]].
 * @param unitText The text to be converted.
 * @param timeString The complete time string.
 * @returns A [[TimeUnit]] value corresponding to [[unitText]].
 * @throws InvalidParameterError If [[unitText]] is invalid.
 */
function convertToTimeUnit(unitText: string, timeString: string): TimeUnit {
    const result = TEXT_TO_UNIT[unitText.toLowerCase()];
    if (result === undefined) {
        // In theory it should be impossible to get here as the tokenizer regex should weed out bad values
        throw new InvalidParameterError(
            'timeString',
            `Invalid time string [${timeString}] - unsupported time unit: [${unitText}]`
        );
    }
    return result;
}

function divide(dividend: number, divisor: number): number {
    return round(dividend / divisor);
}

function multiply(multiplier: number, multiplicand: number): number {
    return round(multiplier * multiplicand);
}

function round(value: number): number {
    return Math.round(value * DECIMAL_FACTOR) / DECIMAL_FACTOR;
}

/**
 * Represents a period of time.
 *
 * NOTE: This class is only accurate to 5 decimal places!
 */
export class TimeSpan {
    readonly #value: number;

    /**
     * A [[TimeSpan]] instance representing no time.
     */
    public static readonly zero: TimeSpan = new TimeSpan(0, TimeUnit.Milliseconds);

    /**
     * A [[TimeSpan]] instance representing the maximum time which may be represented.
     */
    public static readonly maximum: TimeSpan = new TimeSpan(MAXIMUM_VALUE[TimeUnit.Milliseconds], TimeUnit.Milliseconds);

    /**
     * Initializes a new instance of the [[TimeSpan]] class.
     * @param value The time value.
     * @param unit The time unit.
     */
    public constructor(value: number, unit: TimeUnit) {
        if (value < 0) {
            throw new InvalidParameterError('value', `Invalid value [${value}] - must be >= 0`);
        }
        if (unit === undefined) {
            throw new InvalidParameterError('unit', 'Unit must be provided when value is number');
        }
        if (unit === TimeUnit.Milliseconds && Math.floor(value) !== value) {
            throw new InvalidParameterError('value', `Invalid value [${value}] - milliseconds value must be a positive integer`);
        }
        if (value > MAXIMUM_VALUE[unit]) {
            throw new InvalidParameterError(
                'value',
                `Invalid value [${value}] - value is too large to be represented by a TimeSpan`
            );
        }
        // Insure we never exceed the maximum
        this.#value = Math.min(multiply(value, getMultiplier(unit)), MAXIMUM_VALUE[TimeUnit.Milliseconds]);
    }

    /**
     * Creates a [[TimeSpan]] instance for the specified time in milliseconds.
     * @param milliseconds The time span in milliseconds.
     * @returns A new [[TimeSpan]] instance.
     * @throws [[InvalidParameterError]] If milliseconds is too large to be represented as a [[TimeSpan]].
     */
    public static fromMilliseconds(milliseconds: number): TimeSpan {
        if (milliseconds > MAXIMUM_VALUE[TimeUnit.Milliseconds]) {
            throw new InvalidParameterError(
                'milliseconds',
                `Invalid milliseconds [${milliseconds}] - value is too large to be represented by a TimeSpan`
            );
        }
        return new TimeSpan(milliseconds, TimeUnit.Milliseconds);
    }

    /**
     * Creates a [[TimeSpan]] instance for the specified time in seconds.
     * @param seconds The time span in seconds.
     * @returns A new [[TimeSpan]] instance.
     */
    public static fromSeconds(seconds: number): TimeSpan {
        if (seconds > MAXIMUM_VALUE[TimeUnit.Seconds]) {
            throw new InvalidParameterError(
                'seconds',
                `Invalid seconds [${seconds}] - value is too large to be represented by a TimeSpan`
            );
        }
        return new TimeSpan(seconds, TimeUnit.Seconds);
    }

    /**
     * Creates a [[TimeSpan]] instance for the specified time in minutes.
     * @param minutes The time span in minutes.
     * @returns A new [[TimeSpan]] instance.
     */
    public static fromMinutes(minutes: number): TimeSpan {
        if (minutes > MAXIMUM_VALUE[TimeUnit.Minutes]) {
            throw new InvalidParameterError(
                'minutes',
                `Invalid minutes [${minutes}] - value is too large to be represented by a TimeSpan`
            );
        }
        return new TimeSpan(minutes, TimeUnit.Minutes);
    }

    /**
     * Creates a [[TimeSpan]] instance for the specified time in hours.
     * @param hours The time span in hours.
     * @returns A new [[TimeSpan]] instance.
     */
    public static fromHours(hours: number): TimeSpan {
        if (hours > MAXIMUM_VALUE[TimeUnit.Hours]) {
            throw new InvalidParameterError(
                'hours',
                `Invalid hours [${hours}] - value is too large to be represented by a TimeSpan`
            );
        }
        return new TimeSpan(hours, TimeUnit.Hours);
    }

    /**
     * Creates a [[TimeSpan]] instance for the specified time in days.
     * @param days The time span in days.
     * @returns A new [[TimeSpan]] instance.
     */
    public static fromDays(days: number): TimeSpan {
        if (days > MAXIMUM_VALUE[TimeUnit.Days]) {
            throw new InvalidParameterError(
                'days',
                `Invalid days [${days}] - value is too large to be represented by a TimeSpan`
            );
        }
        return new TimeSpan(days, TimeUnit.Days);
    }

    /**
     * Creates a [[TimeSpan]] instance for the specified time in weeks.
     * @param weeks The time span in weeks.
     * @returns A new [[TimeSpan]] instance.
     */
    public static fromWeeks(weeks: number): TimeSpan {
        if (weeks > MAXIMUM_VALUE[TimeUnit.Weeks]) {
            throw new InvalidParameterError(
                'weeks',
                `Invalid weeks [${weeks}] - value is too large to be represented by a TimeSpan`
            );
        }
        return new TimeSpan(weeks, TimeUnit.Weeks);
    }

    /**
     * Creates a [[TimeSpan]] instance for the specified time in years.
     * @param years The time span in years.
     * @returns A new [[TimeSpan]] instance.
     */
    public static fromYears(years: number): TimeSpan {
        if (years > MAXIMUM_VALUE[TimeUnit.Years]) {
            throw new InvalidParameterError(
                'years',
                `Invalid years [${years}] - value is too large to be represented by a TimeSpan`
            );
        }
        return new TimeSpan(years, TimeUnit.Years);
    }

    /**
     * Creates a [[TimeSpan]] instance for the specified time string.
     *
     * Example: "1 hour", "30 s", "1.5 days", or "1m 30s".
     *
     * Supported time units are:
     * * y / year / years
     * * w / week / weeks
     * * d / day / days
     * * h / hour /hours
     * * m / minute / minutes
     * * s / second / seconds
     * * ms / millisecond / milliseconds
     * @param timeString The time string to convert to a [[TimeSpan]] instance.
     * @returns A new [[TimeSpan]] instance.
     */
    public static fromTimeString(timeString: string): TimeSpan {
        const milliseconds = parseMilliseconds(timeString);
        return new TimeSpan(milliseconds, TimeUnit.Milliseconds);
    }

    /**
     * Gets just the milliseconds portion of time span.
     */
    public get milliseconds(): number {
        return Math.floor(this.#value - multiply(Math.floor(this.totalSeconds), getMultiplier(TimeUnit.Seconds)));
    }

    /**
     * Gets just the seconds portion of time span.
     */
    public get seconds(): number {
        const result = Math.floor(
            divide(
                this.#value - multiply(Math.floor(this.totalMinutes), getMultiplier(TimeUnit.Minutes)),
                getMultiplier(TimeUnit.Seconds)
            )
        );
        return result >= 0 ? result : 0;
    }

    /**
     * Gets just the minutes portion of time span.
     */
    public get minutes(): number {
        const result = Math.floor(
            divide(
                this.#value - multiply(Math.floor(this.totalHours), getMultiplier(TimeUnit.Hours)),
                getMultiplier(TimeUnit.Minutes)
            )
        );
        return result >= 0 ? result : 0;
    }

    /**
     * Gets just the hours portion of time span.
     */
    public get hours(): number {
        const result = Math.floor(
            divide(
                this.#value - multiply(Math.floor(this.totalDays), getMultiplier(TimeUnit.Days)),
                getMultiplier(TimeUnit.Hours)
            )
        );
        return result >= 0 ? result : 0;
    }

    /**
     * Gets just the days portion of time span.
     */
    public get days(): number {
        const result = Math.floor(
            divide(
                this.#value -
                    (multiply(this.years, getMultiplier(TimeUnit.Years)) + multiply(this.weeks, getMultiplier(TimeUnit.Weeks))),
                getMultiplier(TimeUnit.Days)
            )
        );
        return result >= 0 ? result : 0;
    }

    /**
     * Gets just the weeks portion of time span.
     */
    public get weeks(): number {
        const result = Math.floor(
            divide(
                this.#value - multiply(Math.floor(this.totalYears), getMultiplier(TimeUnit.Years)),
                getMultiplier(TimeUnit.Weeks)
            )
        );
        return result >= 0 ? result : 0;
    }

    /**
     * Gets just the years portion of time span.
     */
    public get years(): number {
        const result = Math.floor(divide(this.#value, getMultiplier(TimeUnit.Years)));
        return result >= 0 ? result : 0;
    }

    /**
     * Gets the total time span represented as milliseconds.
     */
    public get totalMilliseconds(): number {
        return this.#value;
    }

    /**
     * Gets the total time span represented as seconds.
     */
    public get totalSeconds(): number {
        return divide(this.#value, getMultiplier(TimeUnit.Seconds));
    }

    /**
     * Gets the total time span represented as minutes.
     */
    public get totalMinutes(): number {
        return divide(this.#value, getMultiplier(TimeUnit.Minutes));
    }

    /**
     * Gets the total time span represented as hours.
     */
    public get totalHours(): number {
        return divide(this.#value, getMultiplier(TimeUnit.Hours));
    }

    /**
     * Gets the total time span represented as days.
     */
    public get totalDays(): number {
        return divide(this.#value, getMultiplier(TimeUnit.Days));
    }

    /**
     * Gets the total time span represented as weeks.
     */
    public get totalWeeks(): number {
        return divide(this.#value, getMultiplier(TimeUnit.Weeks));
    }

    /**
     * Gets the total time span represented as years.
     */
    public get totalYears(): number {
        return divide(this.#value, getMultiplier(TimeUnit.Years));
    }

    /**
     * Creates a string representation of the [[TimeSpan]].
     * @returns A string representation of the time span.
     */
    public toString(): string {
        function append(value: number, unit: string, text: string, force?: boolean): string {
            if (value || force) {
                if (text.length) {
                    text += '; ';
                }
                text += `${value} ${unit}${value === 1 ? '' : 's'}`;
            }
            return text;
        }

        let result = '';
        result = append(this.years, 'year', result);
        result = append(this.weeks, 'week', result);
        result = append(this.days, 'day', result);
        result = append(this.hours, 'hour', result);
        result = append(this.minutes, 'minute', result);
        result = append(this.seconds, 'second', result);
        result = append(this.milliseconds, 'millisecond', result, result.length === 0);
        return result;
    }

    /**
     * Returns a JSON representation of the current object instance.
     * @returns A JSON representation of the current object instance.
     */
    public toJSON(): object {
        return {
            years: this.years,
            weeks: this.weeks,
            days: this.days,
            hours: this.hours,
            minutes: this.minutes,
            seconds: this.seconds,
            milliseconds: this.milliseconds
        };
    }
}
