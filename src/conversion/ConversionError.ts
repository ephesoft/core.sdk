import { NestedError } from '../errors/NestedError';

/**
 * Indicates a value could not be converted to another type.
 */
export class ConversionError extends NestedError {
    /**
     * Initializes a new instance of the [[ConversionError]] class.
     * @param message A description of the error.
     * @param innerError The error that caused the [[ConversionError]].
     */
    public constructor(message: string, innerError?: Error) {
        super(message, innerError);
        this.name = 'ConversionError';
    }
}
