import { InvalidParameterError } from '../errors/InvalidParameterError';

/**
 * Collection of helper functions for working with number enums.
 *
 * Example:
 *
 * ```typescript
 * enum MyNumberEnum {
 *     Key1,
 *     Key2,
 *     Key3
 * }
 *
 * // NOTE: It can't infer the type from the type param - types are not available
 * //       at runtime in TypeScript.
 * const numberEnum = new NumberEnum<MyNumberEnum>(MyNumberEnum);
 *
 * const keys = numberEnum.keys; // keys === ['Key1', 'Key2', 'Key3']
 *
 * const isKey = numberEnum.isKey('kEy3'); // isKey === true
 * const isValue = numberEnum.isValue('key3'); // isValue === false - text is a key; not a value
 *
 * const value1 = numberEnum.getValueForKey('key3'); // value === MyNumberEnum.Key3 (2)
 * const value2 = numberEnum.convertToValue(1); // value === MyNumberEnum.Key2 (1)
 *
 * const key = numberEnum.convertToKey('key2'); // key === 'Key2'
 * numberEnum.convertToKey('key2', true); // throws error - case doesn't match
 * ```
 * @typeParam TEnum The type of the enum.
 */
export class NumberEnum<TEnum> {
    readonly #enumType: Record<string, string | number>;
    readonly #keys: string[];
    readonly #values: number[];
    readonly #keyMap: Record<string, KeyMapping>;
    readonly #valueMap: Record<string, KeyMapping>;

    /**
     * Initializes a new instance of the [[NumberEnum]] class.
     * @param enumType The type of the enum.
     * @throws [[InvalidParameterError]] If enumType parameter is not a valid number enum.
     */
    public constructor(enumType: Record<string, string | number>) {
        if (typeof enumType !== 'object') {
            throw new InvalidParameterError('enumType', 'Invalid enumType parameter - must be the type of an enum');
        }
        this.#enumType = enumType;
        this.#keys = [];
        this.#values = [];
        this.#keyMap = {};
        this.#valueMap = {};
        const inverseKeys = new Set<string>();
        for (const key in enumType) {
            if (!key) {
                throw new InvalidParameterError(
                    'enumType',
                    'Invalid enumType parameter - must not have any keys which are an empty string'
                );
            }
            const value = enumType[key];
            if (value === null || value === undefined) {
                throw new InvalidParameterError(
                    'enumType',
                    'Invalid enumType parameter - must not have any values that are undefined or null'
                );
            }
            if (typeof key === 'string' && typeof value === 'number') {
                this.#keys.push(key);
                this.#values.push(value);
                const lowerCaseKey = key.toLowerCase();
                const keyMapping: KeyMapping = { key, value };
                this.#keyMap[lowerCaseKey] = keyMapping;
                this.#valueMap[value] = keyMapping;
            } else if (typeof key === 'string' && typeof value === 'string') {
                // This *should* be the reverse lookup for the number
                inverseKeys.add(value);
            } else {
                throw new InvalidParameterError('enumType', 'Invalid enumType parameter - all values must be numbers');
            }
        }
        // Sanity check - make sure inverse keys match the keys we picked up
        let inconsistencyDetected = false;
        for (const key of this.#keys) {
            if (inverseKeys.has(key)) {
                inverseKeys.delete(key);
            } else {
                inconsistencyDetected = true;
                break;
            }
        }
        if (inconsistencyDetected || inverseKeys.size) {
            throw new InvalidParameterError('enumType', 'Invalid enumType parameter - must be a number enum');
        }
    }

    /**
     * The enum type.
     */
    public get type(): Record<string, string | number> {
        return this.#enumType;
    }

    /**
     * The keys of the enum.
     */
    public get keys(): string[] {
        return [...this.#keys];
    }

    /**
     * The values of the enum.
     */
    public get values(): TEnum[] {
        return [...this.#values] as unknown as TEnum[];
    }

    /**
     * Returns whether the specified text is a key in the enum (case insensitive).
     * @param text The text to be checked.
     * @param caseSensitive true if the comparison should be case sensitive.  Defaults to false.
     * @returns true if text is a key in the enum; false otherwise.
     * @throws [[InvalidParameterError]] If text is undefined or null.
     */
    public isKey(text: string, caseSensitive: boolean = false): boolean {
        if (text === undefined || text === null) {
            throw new InvalidParameterError('text', 'Invalid text parameter - must not be undefined or null');
        }

        if (caseSensitive) {
            const mapping = this.#keyMap[text.toLowerCase()];
            return Boolean(mapping && text === mapping.key);
        }

        return text.toLowerCase() in this.#keyMap;
    }

    /**
     * Returns whether the specified number is a value in the enum.
     * @param number The number to be checked.
     * @returns true if number is a value in the enum; false otherwise.
     * @throws [[InvalidParameterError]] If text is undefined or null.
     */
    public isValue(number: number): boolean {
        if (number === undefined || number === null) {
            throw new InvalidParameterError('number', 'Invalid number parameter - must not be undefined or null');
        }

        return number in this.#valueMap;
    }

    /**
     * Converts the specified text to an enum key.
     * @param text The text to convert to an enum key.
     * @param caseSensitive true if the comparison should be case sensitive.  Defaults to false.
     * @returns text as an enum key.
     * @throws [[InvalidParameterError]] If text is not a key of the enum.
     */
    public convertToKey(text: string, caseSensitive: boolean = false): string {
        if (text) {
            const mapping = this.#keyMap[text.toLowerCase()];
            if (caseSensitive) {
                if (mapping && text === mapping.key) {
                    return mapping.key;
                }
            } else if (mapping) {
                return mapping.key;
            }
        }
        throw new InvalidParameterError('text', `Invalid text parameter (${text}) - must be one of: ${this.#keys.join(', ')}`);
    }

    /**
     * Converts the specified number to an enum value.
     * @param number The number to convert to an enum value.
     * @returns number as an enum value.
     * @throws [[InvalidParameterError]] If number is not a value of the enum.
     */
    public convertToValue(number: number): TEnum {
        if (number !== null && number !== undefined) {
            const mapping = this.#valueMap[number];
            if (mapping) {
                // TypeScript's typing isn't strong enough for us to tell it what we're doing - we'll have to force the cast
                return mapping.value as unknown as TEnum;
            }
        }
        throw new InvalidParameterError(
            'number',
            `Invalid number parameter (${number}) - must be one of: ${this.#values.join(', ')}`
        );
    }

    /**
     * Gets the value for the specified key.
     * @param key The key of the enum value.
     * @param caseSensitive true if the comparison should be case sensitive.  Defaults to false.
     * @returns The appropriate enum value.
     * @throws [[InvalidParameterError]] If key is not present in enumType.
     */
    public getValueForKey(key: string, caseSensitive: boolean = false): TEnum {
        if (key) {
            const mapping = this.#keyMap[key.toLowerCase()];
            if (caseSensitive) {
                if (mapping && key === mapping.key) {
                    // TypeScript's typing isn't strong enough for us to tell it what we're doing - we'll have to force the cast
                    return mapping.value as unknown as TEnum;
                }
            } else if (mapping) {
                // TypeScript's typing isn't strong enough for us to tell it what we're doing - we'll have to force the cast
                return mapping.value as unknown as TEnum;
            }
        }
        throw new InvalidParameterError('key', `Invalid key parameter (${key}) - must be one of: ${this.#keys.join(', ')}`);
    }

    /**
     * Gets the key for the specified enum value.
     * @param value The value to get the key for.
     * @param enumType The type of the enum.
     * @throws [[InvalidParameterError]] If value is not a value for enumType.
     */
    public getKeyForValue(value: number): string {
        if (value !== null && value !== undefined) {
            const mapping = this.#valueMap[value];
            if (mapping) {
                return mapping.key;
            }
        }
        throw new InvalidParameterError(
            'value',
            `Invalid value parameter (${value}) - must be one of: ${this.#values.join(', ')}`
        );
    }

    /**
     * Returns a JSON representation of the current object instance.
     * @returns A JSON representation of the current object instance.
     */
    public toJSON(): unknown {
        const numberOfKeys = this.#keys.length;
        const result: Record<string, number> = {};
        for (let index = 0; index < numberOfKeys; index++) {
            result[this.#keys[index]] = this.#values[index];
        }
        return result;
    }
}

interface KeyMapping {
    key: string;
    value: number;
}
