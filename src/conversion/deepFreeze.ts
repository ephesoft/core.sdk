// Basic implementation taken from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze

/**
 * Makes the specified object or array immutable at runtime.\
 * NOTE: This is a destructive operation - the same object is returned as was passed in (just as with
 *       [Object.freeze](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze)).
 * @param object The object to be frozen.
 * @returns The value of the object parameter.
 */
export function deepFreeze<TObject extends object>(object: TObject): TObject {
    return deepFreezeRecursive(object, new WeakSet<object>());
}

function deepFreezeRecursive<TObject extends object>(object: TObject, encountered: WeakSet<object>): TObject {
    // typeof null === 'object' - JavaScript ♥
    if (object === null || typeof object !== 'object') {
        return object;
    }

    encountered.add(object);

    // Freeze properties before freezing self
    const readableObject = object as Record<string, unknown>;
    for (const propertyName of Object.getOwnPropertyNames(object)) {
        const value = readableObject[propertyName];
        if (value && typeof value === 'object' && !encountered.has(value)) {
            deepFreezeRecursive(value, encountered);
        }
    }

    return Object.freeze(object);
}
