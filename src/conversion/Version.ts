import { InvalidParameterError } from '../errors/InvalidParameterError';
import { stringifySafe } from './stringConversion';

/**
 * Type of a version.
 */
export enum VersionType {
    /**
     * Available to everyone.
     */
    GeneralAvailability = 'GA',

    /**
     * Pre-release - interface shouldn't change (or at least not much).
     */
    Beta = 'beta',

    /**
     * Pre-release - interface may change a lot.
     */
    Alpha = 'alpha'
}

/**
 * Contract for the data passed to the [[Version]] constructor.
 */
export interface VersionDetails {
    /**
     * Major version number.
     */
    major: number;

    /**
     * Minor version number.
     */
    minor: number;

    /**
     * Bugfix version number.
     */
    bugfix?: number;

    /**
     * Prerelease version number.
     *
     * NOTE: This may only be specified if type is not [[VersionType.GeneralAvailability]].
     */
    prerelease?: number;

    /**
     * Version type.
     *
     * Must be specified if prerelease is specified.
     */
    type?: VersionType;
}

/**
 * Object representation of a version.
 */
export class Version {
    /**
     * Parses a version string like "1.0", "1.32.752", "1.0.0-alpha.1", or "1.3.7-beta.5".
     * @param versionText The version text to be parsed.
     * @returns The parsed version.
     */
    public static parse(versionText: string): Version {
        const matches = /^([0-9]+)\.([0-9]+)((\.([0-9]+))(-(alpha|beta)\.([0-9]+)){0,1}){0,1}$/i.exec(versionText);
        if (!matches?.length) {
            throw new InvalidParameterError(
                'versionString',
                `Invalid version [${versionText}] - must be a valid version like 2.5, 1.2.3, 1.0.0-alpha.1, or 1.0.0-beta.1`
            );
        }
        const details: VersionDetails = {
            major: Number(matches[1]),
            minor: Number(matches[2])
        };
        if (typeof matches[5] === 'string') {
            details.bugfix = Number(matches[5]);
            if (typeof matches[8] === 'string') {
                details.prerelease = Number(matches[8]);
                details.type = matches[7].toLowerCase() as VersionType;
            }
        }
        return new Version(details);
    }

    readonly #major: number;
    readonly #minor: number;
    readonly #bugfix?: number;
    readonly #prerelease?: number;
    readonly #type?: VersionType;

    /**
     * Initializes a new instance of the [[Version]] class.
     * @param details The version details.
     * @throws [[InvalidParameterError]] If version is invalid.
     */
    public constructor(details: VersionDetails) {
        if (typeof details.prerelease === 'number') {
            // Can't have a prerelease version without a bugfix version
            if (typeof details.bugfix !== 'number') {
                throw new InvalidParameterError(
                    'details',
                    `Invalid version [${stringifySafe(
                        details
                    )}] - prerelease version may not be specified if bugfix version is not specified`
                );
            }
            if (!details.type || details.type === VersionType.GeneralAvailability) {
                throw new InvalidParameterError(
                    'details',
                    `Invalid version [${stringifySafe(
                        details
                    )}] - type must not be VersionType.GeneralAvailability if prerelease version is specified`
                );
            }
        } else if (details.type && details.type !== VersionType.GeneralAvailability) {
            throw new InvalidParameterError(
                'details',
                `Invalid version [${stringifySafe(
                    details
                )}] - type must be VersionType.GeneralAvailability if prerelease version is not specified`
            );
        }
        Version.validateSegment(details.major, 'major', details);
        this.#major = details.major;
        Version.validateSegment(details.minor, 'minor', details);
        this.#minor = details.minor;
        Version.validateSegment(details.bugfix ?? 0, 'bugfix', details);
        this.#bugfix = details.bugfix;
        Version.validateSegment(details.prerelease ?? 0, 'prerelease', details);
        this.#prerelease = details.prerelease;
        this.#type = details.type;
    }

    /**
     * Gets the major version.
     */
    public get major(): number {
        return this.#major;
    }

    /**
     * Gets the minor version.
     */
    public get minor(): number {
        return this.#minor;
    }

    /**
     * Gets the bugfix version.
     *
     * Returns 0 if bugfix version was not specified.
     */
    public get bugfix(): number {
        return this.#bugfix ?? 0;
    }

    /**
     * Gets the prerelease version.
     *
     * Returns 0 if prerelease version was not specified.
     *
     * NOTE: This value only has relevance if type is not [[VersionType.GeneralAvailability]].
     */
    public get prerelease(): number {
        return this.#prerelease ?? 0;
    }

    /**
     * Gets the type of the version.
     *
     * Returns [[VersionType.GeneralAvailability]] if type was not specified.
     */
    public get type(): VersionType {
        return this.#type ?? VersionType.GeneralAvailability;
    }

    /**
     * Returns a JSON representation of the current object instance.
     * @returns A JSON representation of the current object instance.
     */
    public toJSON(): unknown {
        const json: Record<string, unknown> = { major: this.#major, minor: this.#minor };
        if (typeof this.#bugfix === 'number') {
            json.bugfix = this.#bugfix;
        }
        if (typeof this.#prerelease === 'number') {
            json.prerelease = this.#prerelease;
        }
        if (typeof this.#type === 'string') {
            json.type = this.#type;
        }
        return json;
    }

    /**
     * Creates a string representation of the current object instance.
     * @returns A string representation of the current object instance.
     */
    public toString(): string {
        let versionText = `${this.#major}.${this.#minor}`;
        if (typeof this.#bugfix === 'number') {
            versionText += `.${this.#bugfix}`;
            if (this.#type && this.#type !== VersionType.GeneralAvailability) {
                versionText += `-${this.#type}.${this.#prerelease}`;
            }
        }
        return versionText;
    }

    /**
     * Validates a segment of the version.
     * @param value The segment value.
     * @param name The name of the segment.
     * @param details All version details.
     * @throws [[InvalidParameterError]] If value is negative.
     */
    private static validateSegment(value: number, name: string, details: VersionDetails) {
        if (value !== Math.floor(value) || value < 0) {
            throw new InvalidParameterError(
                'details',
                `Invalid version [${stringifySafe(details)}] - ${name} segment must be a positive integer`
            );
        }
    }
}
