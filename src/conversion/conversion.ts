import { ConversionError } from './ConversionError';

// BOOLEAN CONVERSION

/**
 * Returns the specified value as a boolean.\
 * This function will only consider values of type boolean and the strings "true" or
 * "false" (case insensitive) as boolean values.
 * @param value The value to be converted.
 * @param defaultValue The value to be returned if value cannot be converted to a boolean.
 * @returns value as a boolean or defaultValue if value cannot be converted to a boolean.
 * @throws [[ConversionError]] If value cannot be converted to a boolean and defaultValue
 *                             is not provided.
 */
export function toBoolean(value: unknown, defaultValue?: boolean): boolean {
    const valueIsNullish = value === null || value === undefined;
    const defaultValueIsNullish = defaultValue == null || defaultValue === undefined;
    if (valueIsNullish) {
        if (defaultValueIsNullish) {
            throw new ConversionError(`Value (${String(value)}) could not be converted to a boolean`);
        }
        return defaultValue;
    }

    if (typeof value === 'boolean') {
        return value;
    }

    const text = String(value).toLowerCase();
    if (text === 'true') {
        return true;
    } else if (text === 'false') {
        return false;
    } else if (defaultValueIsNullish) {
        throw new ConversionError(`Value (${String(value)}) could not be converted to a boolean`);
    } else {
        return defaultValue;
    }
}
