import { ConversionError } from './ConversionError';

// Type Guard - 'text is string' predicate tells TypeScript this function verifies we're working with a string
function isString(text: string | undefined | null): text is string {
    return typeof text === 'string';
}

// NUMBER CONVERSION

/**
 * Returns whether the specified character is a digit (0-9);
 * @param character The character to be tested.
 * @returns true if character is a digit; false otherwise.
 */
export function isDigit(character: string | undefined | null): boolean {
    return isString(character) && /^[0-9]$/.test(character);
}

/**
 * Returns whether the specified text is a number (integer or float).
 * @param text The text to be tested.
 * @returns true if text is a number; false otherwise.
 */
export function isNumber(text: string | undefined | null): boolean {
    return isString(text) && /(^(-{0,1})[0-9]+(\.[0-9]+){0,1}$)|(^(-{0,1})\.[0-9]+$)/.test(text);
}

/**
 * Returns the specified text as a number.  If text cannot be converted to a number, the
 * behavior depends on whether a defaultValue parameter is supplied:\
 * - If defaultValue is supplied, it is returned.\
 * - If defaultValue is not supplied, a [[ConversionError]] is thrown.
 * @param text The text to be converted.
 * @param defaultValue The value to be returned if text cannot be converted to a number.
 * @returns The value of text as a number or Number.NaN if text was not a number.
 * @throws [[ConversionError]] If text cannot be converted to a number and defaultValue
 *                             is not supplied.
 */
export function toNumber(text: string | undefined | null, defaultValue?: number): number {
    if (isString(text) && isNumber(text)) {
        const numericValue: number = Number(text);
        if (!Number.isNaN(numericValue) && Number.isFinite(numericValue)) {
            return numericValue;
        }
    }
    if (typeof defaultValue === 'number') {
        return defaultValue;
    }
    throw new ConversionError(`Value (${String(text)}) could not be converted to a number`);
}

// The stringify code below was copied from the Mozilla example at
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cyclic_object_value
/**
 * Contract for replacer functions passed in to JSON.stringify
 */
type ReplacerDelegate = (this: unknown, key: string, value: unknown) => unknown;
/**
 * Creates a replacer function for use in JSON.stringify that strips circular references.
 */
const getCircularReplacer = (): ReplacerDelegate => {
    const seen = new WeakSet();
    return (key: string, value: unknown): unknown => {
        if (typeof value === 'object' && value !== null) {
            if (seen.has(value)) {
                return '<CIRCULAR>';
            }
            seen.add(value);
        }
        return value;
    };
};
/**
 * Converts the specified value to a JavaScript Object Notation (JSON) string.\
 * Unlike JSON.stringify, this function will not throw an error if value contains a circular reference.
 * Instead, the value of the circular reference will be replaced with the text "```<CIRCULAR>```".\
 * NOTE: This implementation is not perfect - if an array contains the same object twice, the second item
 *       will be output as "```<CIRCULAR>```".
 * @param value The value to be stringified.
 * @param space A string or number object that's used to insert white space into the output JSON string for readability purposes.\
 *              If this is a number, it indicates the number of space characters to use as white space; this number is capped at 10 (if it is greater, the value is 10). Values less than 1 indicate that no space should be used.\
 *              If this is a string, the string (or the first 10 characters of the string, if it's longer than that) is used as white space. If this parameter is not provided (or is null), no white space is used.
 * @returns A JSON string representation of the specified value.
 */
export function stringifySafe(value: unknown, space?: string | number): string {
    return JSON.stringify(value, getCircularReplacer(), space) ?? 'undefined';
}
