import { DataTypeExtractionError } from '../errors/DataTypeExtractionError';
import { isNumber, isDigit } from './stringConversion';
import { ConversionError } from './ConversionError';

/**
 * There are fields on our documents that are numbers, but the actual value
 * has characters like dollar signs, commas and others.
 * This function will strip off the non-numeric characters to ensure it's a
 * valid number.
 *
 * @param value The string value to convert to a number
 * @throws [[DataTypeExtractionError]] when original value can't be properly extracted as a number.
 */
export function extractNumber(value: string | number, defaultValue?: number | null): number | null {
    try {
        if (typeof value === 'number') {
            // Make sure the number is safe before returning it
            if (!isSafeNumber(value)) {
                throw new ConversionError(`Value must be a safe number`);
            }
            return value;
        }

        const val: string = value.toString();
        let trimmedValue = trimEnd(val);
        trimmedValue = trimBeginning(trimmedValue);
        trimmedValue = filterCharacters(trimmedValue);

        if (!isNumber(trimmedValue)) {
            throw new ConversionError(`Unsupported number format`);
        }

        const number = Number(trimmedValue);
        // I don't *think* Number will ever return NaN, but better safe than sorry
        if (isNaN(number)) {
            throw new ConversionError(`Unsupported number format`);
        }

        // Make sure number is safe after it's been converted from a string
        if (!isSafeNumber(number)) {
            throw new ConversionError(`Value must be a safe number`);
        }

        return number;
    } catch (error) {
        if (defaultValue !== undefined) {
            return defaultValue;
        }
        throw new DataTypeExtractionError(
            `Failed to parse value (${value}) as a number: ${(error as Error).message}`,
            error as Error
        );
    }
}

function trimEnd(value: string): string {
    if (!value) {
        return '';
    }

    let i = value.length - 1;
    for (; i >= 0; i--) {
        if (isDigit(value[i])) break;
    }

    return value.substring(0, i + 1);
}

function trimBeginning(value: string): string {
    if (!value) {
        return '';
    }

    // eslint-disable-next-line no-useless-escape
    const characterFilter = /[0-9\-]/;
    const trimmedValue: string = value;

    // If we have a comma followed by EXACTLY two digits, we'll assume the comma should have been a period
    if (/^,[0-9]{2}$/.test(trimmedValue)) {
        return trimmedValue;
    }

    let newValue = '';
    let dotEncountered: boolean = false;
    for (let i = 0; i < value.length; i++) {
        if (value[i] === '.') {
            if (!dotEncountered) {
                dotEncountered = true;
                newValue += value[i];
            }
        } else if (characterFilter.test(value[i])) {
            return newValue + value.substring(i);
        }
    }

    return newValue;
}

function filterCharacters(value: string): string {
    if (!value) {
        return '';
    }

    // If the string ends with a comma and EXACTLY two digits, assume the comma should be a period
    if (/,[0-9]{2}$/.test(value)) {
        // treat all commas as periods - we'll weed out all but the last one later
        value = value.replace(/,/g, '.');
    }

    const numberOfCharacters = value.length;
    let filteredText = '';
    for (let characterIndex = 0; characterIndex < numberOfCharacters; characterIndex++) {
        const character = value.charAt(characterIndex);
        if (characterIndex === 0) {
            // The first character should have already been vetted by trimLeft - we can ignore it
            filteredText += character;
            continue;
        }
        if (isDigit(character)) {
            filteredText += character;
            continue;
        }
        if (character === '.') {
            // Is it the last one?
            if (value.includes('.', characterIndex + 1)) {
                // not the last one - skip it
                continue;
            }
            filteredText += character;
        }
    }
    return filteredText;
}

function isSafeNumber(value: number): boolean {
    if (Number.isInteger(value)) {
        return Number.isSafeInteger(value);
    }

    /**
     * We're not evaluating against Number.MIN_VALUE also as it is the smallest positive number value > 0
     * and doesn't give us a reliable way to check negative number values that may be out of range.
     * The Number.isSafeInteger() check above does evaluate both positive and negative integer values, so
     * we get some coverage there for negative values.
     * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/MIN_VALUE
     * */
    return value < Number.MAX_VALUE;
}
