import { DataTypeExtractionError } from '../errors/DataTypeExtractionError';
import parseISO from 'date-fns/parseISO';
import isValid from 'date-fns/isValid';
import format from 'date-fns/format';

/**
 * Take a value and convert it to the specified date format. The value passed in must first be converted
 * to ISO 8601 date format so that date-fns can work validate and format the the date correctly.
 *
 * @param value The string that represents a date
 * @param options Specifies the options used to convert a date. defaultValue, if supplied, is used if the value specified is
 * not a valid date and must be a valid ISO 8601 date. dateFormat specifies the date format to be returned. Valid date formats
 * are defined at https://date-fns.org/v2.16.1/docs/format
 *
 * Date issues: https://stackoverflow.com/questions/7556591/is-the-javascript-date-object-always-one-day-off
 * date-fns issues: https://stackoverflow.com/questions/58561169/date-fns-how-do-i-format-to-utc
 * To avoid date-fns issues with iso8601 dates we filter them out using a regex
 */
export function extractDate(value: string, options?: { defaultValue?: string | null; dateFormat?: string }): string | null {
    const dateFormat = options?.dateFormat ?? 'yyyy-MM-dd';
    try {
        return formatDate(value, dateFormat);
    } catch (error) {
        /** Fall back to default value */
        if (options?.defaultValue === null) {
            return null;
        } else if (options?.defaultValue) {
            try {
                return formatDate(options.defaultValue, dateFormat);
            } catch (e) {
                throw new DataTypeExtractionError(
                    `Failed to parse both value (${value}) and default value (${options.defaultValue}) as a date: ${
                        (e as Error).message
                    }`,
                    e as Error
                );
            }
        }

        throw new DataTypeExtractionError(
            `Failed to parse value (${value}) as a date: ${(error as Error).message}`,
            error as Error
        );
    }
}

function formatDate(value: string, dateFormat: string): string {
    // HACK: Anything beginning with this pattern ends up being parsed in GMT and adjusted for time zone
    //       This hack works around that, taking the date as-is.
    //       See: https://stackoverflow.com/questions/7556591/is-the-javascript-date-object-always-one-day-off
    const match = /^[0-9]{4}-[0-9]{2}-[0-9]{2}/.exec(value);
    let iso8601Date: string = '';
    if (match?.length === 1) {
        iso8601Date = match[0];
    } else {
        const date = new Date(value);
        // Would be nice if an invalid value caused an error, but no such luck
        if (isNaN(date as unknown as number)) {
            throw new DataTypeExtractionError('Unsupported date format');
        }
        const year = date.getFullYear();
        const month = date.getMonth() + 1;
        const day = date.getDate();
        iso8601Date = `${year}-${pad(month)}-${pad(day)}`;
    }

    const parsedDate: Date = parseISO(iso8601Date);
    if (!isValid(parsedDate)) {
        throw new DataTypeExtractionError(`Date was found to be invalid after formatting (${iso8601Date})`);
    }
    return format(parsedDate, dateFormat);
}

function pad(value: number): string | number {
    if (value < 10) {
        return `0${value}`;
    }
    return value;
}
