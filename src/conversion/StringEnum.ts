import { InvalidParameterError } from '../errors/InvalidParameterError';

/**
 * Collection of helper functions for working with string enums.\
 * Example:
 *
 * ```typescript
 * enum MyStringEnum {
 *     Key1: 'value1',
 *     Key2: 'value2',
 *     Key3: 'value3'
 * }
 *
 * // NOTE: It can't infer the type from the type param - types are not available
 * //       at runtime in TypeScript.
 * const stringEnum = new StringEnum<MyStringEnum>(MyStringEnum);
 *
 * const keys = stringEnum.keys; // keys === ['Key1', 'Key2', 'Key3']
 *
 * const isKey = stringEnum.isKey('kEy3'); // isKey === true
 * const isValue = stringEnum.isValue('key3'); // isValue === false - text is a key; not a value
 *
 * const value1 = stringEnum.getValueForKey('key3'); // value === MyStringEnum.Key3 ('value3')
 * const value2 = stringEnum.convertToValue('VALUE2'); // value === MyStringEnum.Key2 ('value2')
 *
 * const key = stringEnum.convertToKey('key2'); // key === 'Key2'
 * stringEnum.convertToKey('key2', true); // throws error - case doesn't match
 * ```
 * @typeParam TEnum The type of the enum.
 */
export class StringEnum<TEnum> {
    readonly #enumType: Record<string, string>;
    readonly #keys: string[];
    readonly #values: string[];
    readonly #keyMap: Record<string, KeyMapping>;
    readonly #valueMap: Record<string, KeyMapping>;

    /**
     * Initializes a new instance of the [[StringEnum]] class.
     * @param enumType The type of the enum.
     * @throws [[InvalidParameterError]] If enumType parameter is not a valid string enum.
     */
    public constructor(enumType: Record<string, string>) {
        if (typeof enumType !== 'object') {
            throw new InvalidParameterError('enumType', 'Invalid enumType parameter - must be the type of an enum');
        }
        this.#enumType = enumType;
        this.#keys = [];
        this.#values = [];
        this.#keyMap = {};
        this.#valueMap = {};
        let key: keyof typeof enumType;
        for (key in enumType) {
            if (!key) {
                throw new InvalidParameterError(
                    'enumType',
                    'Invalid enumType parameter - must not have any keys which are an empty string'
                );
            }
            const value = enumType[key];
            if (!value) {
                throw new InvalidParameterError(
                    'enumType',
                    'Invalid enumType parameter - must not have any values that are undefined, null, or an empty string'
                );
            }
            if (typeof value !== 'string') {
                throw new InvalidParameterError('enumType', 'Invalid enumType parameter - all values must be strings');
            }
            this.#keys.push(key);
            this.#values.push(value);
            const lowerCaseKey = key.toLowerCase();
            const keyMapping: KeyMapping = { key, value };
            this.#keyMap[lowerCaseKey] = keyMapping;
            const lowerCaseValue = value.toLowerCase();
            this.#valueMap[lowerCaseValue] = keyMapping;
        }
    }

    /**
     * The enum type.
     */
    public get type(): Record<string, string> {
        return this.#enumType;
    }

    /**
     * The keys of the enum.
     */
    public get keys(): string[] {
        return [...this.#keys];
    }

    /**
     * The values of the enum.
     */
    public get values(): TEnum[] {
        return [...this.#values] as unknown as TEnum[];
    }

    /**
     * Returns whether the specified text is a key in the enum (case insensitive).
     * @param text The text to be checked.
     * @param caseSensitive true if the comparison should be case sensitive.  Defaults to false.
     * @returns true if text is a key in the enum; false otherwise.
     * @throws [[InvalidParameterError]] If text is undefined or null.
     */
    public isKey(text: string, caseSensitive: boolean = false): boolean {
        if (text === undefined || text === null) {
            throw new InvalidParameterError('text', 'Invalid text parameter - must not be undefined or null');
        }

        if (caseSensitive) {
            const mapping = this.#keyMap[text.toLowerCase()];
            return Boolean(mapping && text === mapping.key);
        }

        return text.toLowerCase() in this.#keyMap;
    }

    /**
     * Returns whether the specified text is a value in the enum (case insensitive).
     * @param text The text to be checked.
     * @param caseSensitive true if the comparison should be case sensitive.  Defaults to false.
     * @returns true if text is a value in the enum; false otherwise.
     * @throws [[InvalidParameterError]] If text is undefined or null.
     */
    public isValue(text: string, caseSensitive: boolean = false): boolean {
        if (text === undefined || text === null) {
            throw new InvalidParameterError('text', 'Invalid text parameter - must not be undefined or null');
        }

        if (caseSensitive) {
            const mapping = this.#valueMap[text.toLowerCase()];
            return Boolean(mapping && text === mapping.value);
        }

        return text.toLowerCase() in this.#valueMap;
    }

    /**
     * Converts the specified text to an enum key.
     * @param text The text to convert to an enum key.
     * @param caseSensitive true if the comparison should be case sensitive.  Defaults to false.
     * @returns text as an enum key.
     * @throws [[InvalidParameterError]] If text is not a key of the enum.
     */
    public convertToKey(text: string, caseSensitive: boolean = false): string {
        if (text) {
            const mapping = this.#keyMap[text.toLowerCase()];
            if (caseSensitive) {
                if (mapping && text === mapping.key) {
                    return mapping.key;
                }
            } else if (mapping) {
                return mapping.key;
            }
        }
        throw new InvalidParameterError('text', `Invalid text parameter (${text}) - must be one of: ${this.#keys.join(', ')}`);
    }

    /**
     * Converts the specified text to an enum value.
     * @param text The text to convert to an enum value.
     * @param caseSensitive true if the comparison should be case sensitive.  Defaults to false.
     * @returns text as an enum value.
     * @throws [[InvalidParameterError]] If text is not a value of the enum.
     */
    public convertToValue(text: string, caseSensitive: boolean = false): TEnum {
        if (text) {
            const mapping = this.#valueMap[text.toLowerCase()];
            if (caseSensitive) {
                if (mapping && text === mapping.value) {
                    // TypeScript's typing isn't strong enough for us to tell it what we're doing - we'll have to force the cast
                    return mapping.value as unknown as TEnum;
                }
            } else if (mapping) {
                // TypeScript's typing isn't strong enough for us to tell it what we're doing - we'll have to force the cast
                return mapping.value as unknown as TEnum;
            }
        }
        throw new InvalidParameterError('text', `Invalid text parameter (${text}) - must be one of: ${this.#values.join(', ')}`);
    }

    /**
     * Gets the value for the specified key.
     * @param key The key of the enum value.
     * @param caseSensitive true if the comparison should be case sensitive.  Defaults to false.
     * @returns The appropriate enum value.
     * @throws [[InvalidParameterError]] If key is not present in enumType.
     */
    public getValueForKey(key: string, caseSensitive: boolean = false): TEnum {
        if (key) {
            const mapping = this.#keyMap[key.toLowerCase()];
            if (caseSensitive) {
                if (mapping && key === mapping.key) {
                    // TypeScript's typing isn't strong enough for us to tell it what we're doing - we'll have to force the cast
                    return mapping.value as unknown as TEnum;
                }
            } else if (mapping) {
                // TypeScript's typing isn't strong enough for us to tell it what we're doing - we'll have to force the cast
                return mapping.value as unknown as TEnum;
            }
        }
        throw new InvalidParameterError('key', `Invalid key parameter (${key}) - must be one of: ${this.#keys.join(', ')}`);
    }

    /**
     * Gets the key for the specified enum value.
     * @param value The value to get the key for.
     * @param caseSensitive true if the comparison should be case sensitive.  Defaults to false.
     * @param enumType The type of the enum.
     * @throws [[InvalidParameterError]] If value is not a value for enumType.
     */
    public getKeyForValue(value: string, caseSensitive: boolean = false): string {
        if (value) {
            const mapping = this.#valueMap[value.toLowerCase()];
            if (caseSensitive) {
                if (mapping && value === mapping.value) {
                    return mapping.key;
                }
            } else if (mapping) {
                return mapping.key;
            }
        }
        throw new InvalidParameterError(
            'value',
            `Invalid value parameter (${value}) - must be one of: ${this.#values.join(', ')}`
        );
    }

    /**
     * Returns a JSON representation of the current object instance.
     * @returns A JSON representation of the current object instance.
     */
    public toJSON(): unknown {
        return this.#enumType;
    }
}

interface KeyMapping {
    key: string;
    value: string;
}
