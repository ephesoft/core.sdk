import { NestedError } from './NestedError';

/**
 * Indicates the item was not found.
 */
export class NotFoundError extends NestedError {
    /**
     * Initializes a new instance of the [[NotFoundError]] class.
     * @param message A description of the error.
     * @param innerError The error that caused the [[NotFoundError]].
     */
    public constructor(message?: string, innerError?: Error) {
        super(message ?? 'Not found', innerError);
        this.name = 'NotFoundError';
    }
}
