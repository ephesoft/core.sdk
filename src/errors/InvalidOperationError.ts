import { NestedError } from './NestedError';

/**
 * Indicates a function call was invalid for the object's current state.
 */
export class InvalidOperationError extends NestedError {
    /**
     * Initializes a new instance of the [[InvalidOperationError]] class.
     * @param message A description of the error.
     * @param innerError The error that caused the [[InvalidOperationError]].
     */
    public constructor(message: string, innerError?: Error) {
        super(message, innerError);
        this.name = 'InvalidOperationError';
    }
}
