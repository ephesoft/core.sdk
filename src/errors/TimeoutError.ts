import { NestedError } from './NestedError';

/**
 * Indicates an operation exceeded the timeout period.
 */
export class TimeoutError extends NestedError {
    /**
     * Initializes a new instance of the [[TimeoutError]] class.
     * @param message A description of the error.
     * @param innerError The error that caused the [[TimeoutError]].
     */
    public constructor(message: string, innerError?: Error) {
        super(message, innerError);
        this.name = 'TimeoutError';
    }
}
