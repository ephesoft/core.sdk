import { NestedError } from './NestedError';

export class DataTypeExtractionError extends NestedError {
    public constructor(message: string, innerError?: Error) {
        super(message, innerError);
        this.name = 'DataTypeExtractionError';
    }
}
