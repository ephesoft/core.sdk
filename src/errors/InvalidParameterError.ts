import { NestedError } from './NestedError';

/**
 * Indicates an invalid parameter was passed to a function.
 */
export class InvalidParameterError extends NestedError {
    /**
     * The name of the parameter that was invalid.
     */
    public readonly parameterName: string;

    /**
     * Initializes a new instance of the [[InvalidParameterError]] class.
     * @param parameterName The name of the parameter that was invalid.
     * @param message A description of the problem.
     * @param innerError The error that caused the [[InvalidParameterError]].
     */
    public constructor(parameterName: string, message?: string, innerError?: Error) {
        super(message ?? 'Invalid parameter', innerError);
        this.parameterName = parameterName;
        this.name = 'InvalidParameterError';
    }

    /**
     * Creates a string representation of this error.
     * @returns A string representation of this error.
     */
    public toString(): string {
        // This does not output nested errors as they may contain sensitive data.
        // Best to let the consumer decide what to expose.
        return `${this.constructor.name}: ${this.message} (parameter: ${this.parameterName})`;
    }

    /**
     * Returns data intended to be consumed by a logger.
     * @returns A data structure to be included in the log entry.
     */
    public getLogData(): unknown {
        return {
            parameterName: this.parameterName
        };
    }
}
