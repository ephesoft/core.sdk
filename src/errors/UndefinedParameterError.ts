import { InvalidParameterError } from './InvalidParameterError';

/**
 * Indicates that a required function parameter was null or undefined.
 */
export class UndefinedParameterError extends InvalidParameterError {
    /**
     * Initializes a new instance of the [[UndefinedParameterError]] class.
     * @param parameterName The name of the undefined parameter.
     * @param message A description of the problem.
     * @param innerError The error that caused the [[UndefinedParameterError]].
     */
    public constructor(parameterName: string, message?: string, innerError?: Error) {
        super(parameterName, message ?? 'Parameter was null or undefined', innerError);
        this.name = 'UndefinedParameterError';
    }
}
