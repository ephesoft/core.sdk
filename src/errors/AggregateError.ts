import { ErrorBase } from './ErrorBase';

/**
 * Indicates one or more errors occurred during the course of an operation.\
 * This class attempts to roughly emulate the Mozilla implementation:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/AggregateError
 */
export class AggregateError extends ErrorBase {
    /**
     * The errors that occurred.
     */
    readonly #errors: Error[];

    /**
     * Initializes a new instance of the [[AggregateError]] class.
     * @param errors The error(s) encountered.
     * @param message An overall description of the error.
     */
    public constructor(errors: Error[], message: string) {
        super(message);
        this.#errors = errors ?? [];
        this.name = 'AggregateError';
    }

    /**
     * Gets the error array specifying the errors encountered.
     */
    public get errors(): Error[] {
        // Return a shallow copy to emulate MDN implementation
        return [...this.#errors];
    }
}
