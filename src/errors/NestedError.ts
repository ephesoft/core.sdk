import { ErrorBase } from './ErrorBase';

/**
 * Custom implementation of Error for reporting a message and the causing Error.
 */
export class NestedError extends ErrorBase {
    /**
     * Gets the error that caused this error (or null if no inner error was specified).
     */
    public readonly innerError: Error | null;

    /**
     * Initializes a new instance of the [[NestedError]] class.
     * @param message A description of the error.
     * @param innerError The error that caused the [[NestedError]].
     */
    public constructor(message: string, innerError?: Error | null) {
        super(message);
        this.innerError = innerError === undefined ? null : innerError;
        // Hard-code the name if NestedError is not subclassed - otherwise try to infer the name
        if (Object.getPrototypeOf(this) === NestedError.prototype) {
            this.name = 'NestedError';
        } else {
            // This produces a useless name when code is minified
            this.name = this.constructor.name;
        }
    }

    /**
     * Creates a string representation of this error.
     * @returns A string representation of this error.
     */
    public toString(): string {
        // This does not output nested errors as they may contain sensitive data.
        // Best to let the consumer decide what to expose.
        return `${this.constructor.name}: ${this.message}`;
    }
}
