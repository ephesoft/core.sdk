import { NestedError } from './NestedError';

/**
 * Indicates a function has not been implemented.
 */
export class NotImplementedError extends NestedError {
    /**
     * Initializes a new instance of the [[NotImplementedError]] class.
     * @param message A description of the error.
     * @param innerError The error that caused the [[NotImplementedError]].
     */
    public constructor(message?: string, innerError?: Error) {
        super(message ?? 'Not implemented', innerError);
        this.name = 'NotImplementedError';
    }
}
