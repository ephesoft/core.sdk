/**
 * Base class for errors that adds a [[toJSON]] function.
 */
export abstract class ErrorBase extends Error {
    /**
     * Initializes a new instance of the [[ErrorBase]] class.
     * @param message A description of the error.
     */
    protected constructor(message: string) {
        super(message);
    }

    /**
     * Returns a JSON representation of the current object instance.
     * @returns A JSON representation of the current object instance.
     */
    public toJSON(): unknown {
        const extendedError = this as unknown as ExtendedError;
        const json: ErrorJson = {
            name: extendedError.name,
            message: extendedError.message,
            stack: this.getStackJson()
        };
        if (extendedError.innerError) {
            json.innerError = extendedError.innerError;
        }
        if (extendedError.errors) {
            json.errors = extendedError.errors;
        }
        if (extendedError.getLogData instanceof Function) {
            json.data = extendedError.getLogData();
        }
        return json;
    }

    private getStackJson(): string[] {
        const result: string[] = [];
        // Remove the error name and message
        const stackEntries = String(this.stack ?? '').split(/[\r\n]/g);
        for (const item of stackEntries) {
            let formattedItem = item.trim();
            // Remove noise from the individual stack item
            if (!formattedItem.startsWith('at')) {
                // Not a stack entry - ignore it
                continue;
            }
            // Remove the "at ""
            formattedItem = item.substr(2).trim();
            if (formattedItem) {
                result.push(formattedItem);
            }
        }
        return result;
    }
}

interface ErrorJson {
    name: string;
    message: string;
    stack: string[];
    innerError?: Error;
    errors?: Error[];
    data?: unknown;
}

interface ExtendedError extends Error {
    innerError?: Error;
    errors?: Error[];
    getLogData(): undefined;
}
