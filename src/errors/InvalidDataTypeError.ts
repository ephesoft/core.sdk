import { NestedError } from './NestedError';

/**
 * Indicates the data type was invalid.
 */
export class InvalidDataTypeError extends NestedError {
    /**
     * Initializes a new instance of the [[InvalidDataTypeError]] class.
     * @param message A description of the error.
     * @param innerError The error that caused the [[InvalidDataTypeError]].
     */
    public constructor(message?: string, innerError?: Error) {
        super(message ?? 'Invalid Data Type', innerError);
        this.name = 'InvalidDataTypeError';
    }
}
