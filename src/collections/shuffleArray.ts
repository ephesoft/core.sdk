import { InvalidParameterError } from '../errors/InvalidParameterError';

/**
 * Shuffles the specified array.\
 * This is Algorithm P (Shuffling) from the Donald E. Knuth book The Art of Computer Programming.
 * @param array The array to be shuffled.
 * @param numberOfPasses The number of times to shuffle the array.
 * @typeparam TItem The type of items stored in the array.
 * @returns A shuffled copy of array.
 * @throws [[InvalidParameterError]] If array is not an array.
 * @throws [[InvalidParameterError]] If numberOfPasses is less than 1.
 */
export function shuffleArray<TItem>(array: TItem[], numberOfPasses: number = 1): TItem[] {
    // JS can slip us anything - throw error if we get something unexpected
    if (!Array.isArray(array)) {
        throw new InvalidParameterError('array', `Invalid array parameter (${String(array)}) - must be an array`);
    }
    if (numberOfPasses < 1) {
        throw new InvalidParameterError(
            'numberOfPasses',
            `Invalid numberOfPasses parameter (${numberOfPasses}) - must be greater than zero`
        );
    }
    const shuffled = Array.isArray(array) ? [...array] : [];
    const lastItemIndex = shuffled.length - 1;
    for (let passNumber = 0; passNumber < numberOfPasses; passNumber++) {
        for (let currentItemIndex = lastItemIndex; currentItemIndex > 0; currentItemIndex--) {
            // always swap with an index below the current one
            const indexToSwapWith = Math.floor(Math.random() * (currentItemIndex + 1));
            [shuffled[currentItemIndex], shuffled[indexToSwapWith]] = [shuffled[indexToSwapWith], shuffled[currentItemIndex]];
        }
    }
    return shuffled;
}
