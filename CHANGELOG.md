# Changelog

## 5.2.0 (2022-11-04)
- Added new simplified `Semaphore` implementation
- Added `TimeoutError`
- Updated npm references

## 5.1.0 (2022-10-21)
- Removed internal package reference

## 5.0.0 (2022-10-20)
- BREAKING: Removed `Semaphore` and `SemaphoreSlim` classes
- Changed build to publish to NPM instead of MyGet
- Updated npm references

## 4.1.0 (2022-09-29)
- Updated npm references
- Updated to template version 6.0.2

## 4.0.0 (2022-08-10)
- BREAKING: Changed `StringEnum.values` to return an enum array instead of a string array - this should only be a breaking change at compile time as types aren't enforced at run time
- Added `maximum` and `zero` properties to `TimeSpan`
- Added new NumberEnum class
- Updated to template version 5.2.0
- Updated npm references

## 3.5.0 (2021-12-22)
- Added Version class
- Updated npm references

## 3.4.0 (2021-12-15)
- Removed TimeSpan constructor overloads - while this is technically a breaking change, there is currently nothing in source control that uses these overloads
- Added new getVariableAsTimeSpan function to environments.ts
- Updated npm references

## 3.3.0 (2021-12-08)
- Updated to template version 2.5.0
- Added TimeSpan class
- Added regexEscape helper method
- Updated npm references

## 3.2.1 (2021-10-01)
- Fixed bad import for DataTypeExtractionError in extractDate

## 3.2.0 (2021-09-24)
- Updated to version 2.2.0 of the typescript.sdk.template
- Updated extractNumber() to handle unsafe numbers

## 3.1.0 (2021-09-07)
- Deprecated `Semaphore`
- Deprecated `SemaphoreSlim`

## 3.0.3 (2021-05-07)
- Fixed misleading example and bad type parameter on MockBase
- Updated npm references

## 3.0.2 (2021-05-05)
- Fixed bad type on getCalls method for mocks
- Updated npm references

## 3.0.1 (2021-04-30)
- Fixed missing types in index.ts

## 3.0.0 (2021-04-30)
- BREAKING: Moved MockError to mocks folder
- BREAKING: Removed ObjectType type
- BREAKING: Removed Optional type
- BREAKING: Removed Keyed type
- Added new MockEngine and MockBase classes to make creating mocks easier
- Changed build to use scripts from integration.test.sdk instead of the local build scripts
- Updated npm references

## 2.18.0 (2021-04-26)
- Deprecated Optional type
- Deprecated Keyed type
- Added Require type for specifying which properties are required
- Added DeepReadonly type
- Added DeepRequired type
- Added index file
- Fixed documentation for deepFreeze
- Updated npm references

## 2.17.0 (2021-04-23)
- Added MockError error
- Added DeepPartial type
- Added DeepMutable type
- Added deepFreeze helper function
- Updated npm references
- Fixed audit issue

## 2.16.0 (2021-04-02)
- Added values getter to StringEnum
- Added changelog tests
- Updated npm references

## 2.15.0 (2020-12-17)
- Added function extractDate

## 2.14.0 (2020-12-14)
- Added function extractNumber
- Added errors: NumberNotFoundError, DataTypeExtractionError

## 2.13.1 (2020-12-14)
- Fixed regex in ErrorBase to be compatible with Safari browser (Safari is unique among our supported browsers in that it doesn't support look-behinds in regular expressions).
- Added docs:bitbucket script to package.json.
- Updated npm references.

## 2.13.0 (2020-12-04)
- Added functions: getMask, getMasks, applyMask, applyMasks, getMaskProperties
- Added types: Keyed, Optional, KeyedOptional
- Added errors: InvalidDataTypeError

## 2.12.0 (2020-11-25)
- Added StringEnum.toJSON function.
- Added NestedError.toJSON function to make our errors work better with JSON.stringify.

## 2.11.0 (2020-11-19)
- Added a new setPropertyIfValue utility function to handle the common use case of only wanting to add a property if we have a value.

## 2.10.0 (2020-11-17)
- Added stringifySafe function to the stringConversion file for converting values to JSON notation without throwing errors on circular references

## 2.9.0 (2020-11-15)
- Added StringEnum class for converting strings to enum keys/values and vice versa
- Updated npm references
- Updated build pipeline image

## 2.8.0 (2020-08-31)
- Updated npm references.

## 2.7.2 (2020-08-26)
- Changed direction and disabled the rule prohibiting the use of object as a type.  ObjectType has been marked as deprecated.

## 2.7.1 (2020-08-26)
- Changed ObjectType to an alias for object to allow it to be used without linter complaints. The linter's suggestion of using Record<string, undefined> as a replacement for object and {} is naive at best - they are two VERY different things.  This was noticed downstream in the http.client.sdk as attempting to assign body to an object caused compiler errors.

## 2.7.0 (2020-08-25)
- Added shuffleArray function
- Added new type to help make linter happy.
- Updated npm dependencies

## 2.6.0 (2020-08-19)
- Added explicit names to errors so WebPack doesn't mangle them
- Added Mutable type
- Added Map types (The existing Record type isn't quite what I was looking for)
- Added some missing npm run scripts.
- Updated npm references

## 2.5.0 (2020-08-01)
- Added mechanism to verify beta versions aren't published to GA.
- Updated npm references.

## 2.4.0 (2020-07-25)
- Added ESLint report data to SonarCloud scans
- Added getLogData function to several errors so we get helpful log messages when we start using the new logger

## 2.3.0 (2020-07-23)
- Updated to use SDK template changes for publishing beta packages

## 2.2.0 (2020-07-13)
- Updated tsconfig.json to match the template
- Updated npm dependencies

## 2.1.1 (2020-06-17)
- Removed redundant scripts from SonarCloud steps of build pipeline
- Updated npm dependencies

## 2.1.0 (2020-06-17)
- Added NotFoundError

## 2.0.0 (2020-06-05)
- BREAKING: Removed index file

## 1.2.1 (2020-06-01)
- Added AggregateError to exports in index.ts

## 1.2.0 (2020-05-31)
- Added AggregateError

## 1.1.1 (2020-05-27)
- Removed caching in Bitbucket pipeline
- Updated to capture coverage output as artifact in Bitbucket pipeline

## 1.1.0 (2020-05-27)
- Added new SemaphoreSlim class
- Updated npm dependencies
- Changed package.json so globs are enclosed in double quotes so they evaluate correctly
- Updated package.json to use absolute dependency versions

## 1.0.3 (2020-04-27)
- Fixed issue that made sleep function inaccessible
- Updated npm dependencies
- Updated package.json configuration to more closely match the template
- Fixed linting issues

## 1.0.2 (2020-04-17)
- Fixed workspace file name
- Added test for version validation
- Updated package dependencies

## 1.0.1 (2020-04-09)
- Updated to using new # syntax for private variables
- Added more error handling around assumptions in Semaphore class
- Added a SemaphoreFailureError for reporting critical failures in Semaphore

## 1.0.0 (2020-04-08)
- Added files for initial release

- - - - -
* Following [Semantic Versioning](https://semver.org/)
* [Changelog Formatting](https://ephesoft.atlassian.net/wiki/spaces/ZEUS1/pages/1189347481/Changelog+Formatting)
* [Major Version Migration Notes](MIGRATION.md)
