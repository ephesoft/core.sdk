module.exports = {
    timeout: 30000,
    require: ['ts-node/register'],
    reporter: '@ephesoft/mocha-reporter'
};